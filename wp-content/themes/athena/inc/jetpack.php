<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package ecotheme
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function ecotheme_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'ecotheme_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function ecotheme_jetpack_setup
add_action( 'after_setup_theme', 'ecotheme_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function ecotheme_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function ecotheme_infinite_scroll_render
