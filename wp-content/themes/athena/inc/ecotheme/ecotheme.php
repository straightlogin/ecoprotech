<?php

/**
 * 
 * ecotheme WordPress Theme
 * 
 * This file contains most of the work done by ecotheme
 * It's pretty straight forward, feel free to edit if you're comfortable with basic PHP
 * 
 * If you got here, thank you for using this theme ! Hack away at it as you see fit.
 * Please take a minute to leave us a review on WordPress.org
 * 
 * 
 */


function ecotheme_scripts() {

    wp_enqueue_style('ecotheme-style', get_stylesheet_uri());

    wp_enqueue_script('ecotheme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true);

    wp_enqueue_script('ecotheme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


    if ( 'Raleway, sans-serif' == get_theme_mod('header_font', 'Raleway, sans-serif') || 'Raleway, sans-serif' == get_theme_mod('theme_font', 'Raleway, sans-serif') )
        wp_enqueue_style('ecotheme-font-raleway', '//fonts.googleapis.com/css?family=Raleway:400,300,600', array(), ecotheme_VERSION);

    if ( 'Lato, sans-serif' == get_theme_mod('header_font', 'Raleway, sans-serif') || 'Lato, sans-serif' == get_theme_mod('theme_font', 'Raleway, sans-serif') )
        wp_enqueue_style('ecotheme-font-lato', '//fonts.googleapis.com/css?family=Lato:400,700,300', array(), ecotheme_VERSION);

    if ( 'Source Sans Pro, sans-serif' == get_theme_mod('header_font', 'Raleway, sans-serif') || 'Source Sans Pro, sans-serif' == get_theme_mod('theme_font', 'Raleway, sans-serif') )
        wp_enqueue_style('ecotheme-font-source', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700,300', array(), ecotheme_VERSION);

    if ( 'Open Sans, sans-serif' == get_theme_mod('header_font', 'Raleway, sans-serif') || 'Open Sans, sans-serif' == get_theme_mod('theme_font', 'Raleway, sans-serif') )
        wp_enqueue_style('ecotheme-font-opensans', '//fonts.googleapis.com/css?family=Open+Sans:400,300,600', array(), ecotheme_VERSION);

    if ( 'Lobster Two, cursive' == get_theme_mod('header_font', 'Raleway, sans-serif') || 'Lobster Two, cursive' == get_theme_mod('theme_font', 'Raleway, sans-serif') )
        wp_enqueue_style('ecotheme-font-lobster', '//fonts.googleapis.com/css?family=Lobster+Two:400,700', array(), ecotheme_VERSION);

    wp_enqueue_style('ecotheme-bootstrap', get_template_directory_uri() . '/inc/css/bootstrap.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-bootstrap-theme', get_template_directory_uri() . '/inc/css/bootstrap-theme.min.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-fontawesome', get_template_directory_uri() . '/inc/css/font-awesome.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-main-style', get_template_directory_uri() . '/inc/css/style.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-camera-style', get_template_directory_uri() . '/inc/css/camera.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-animations', get_template_directory_uri() . '/inc/css/animate.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-slicknav', get_template_directory_uri() . '/inc/css/slicknav.min.css', array(), ecotheme_VERSION);
    wp_enqueue_style('ecotheme-template', get_template_directory_uri() . '/inc/css/temps/' . esc_attr(get_theme_mod('theme_color', 'green')) . '.css', array(), ecotheme_VERSION);

    wp_enqueue_script('ecotheme-sticky', get_template_directory_uri() . '/inc/js/sticky.min.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-easing', get_template_directory_uri() . '/inc/js/easing.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-camera', get_template_directory_uri() . '/inc/js/camera.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-parallax', get_template_directory_uri() . '/inc/js/parallax.min.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-carousel', get_template_directory_uri() . '/inc/js/owl.carousel.min.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-slicknav', get_template_directory_uri() . '/inc/js/slicknav.min.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-wow', get_template_directory_uri() . '/inc/js/wow.js', array('jquery'), ecotheme_VERSION, true);
    wp_enqueue_script('ecotheme-script', get_template_directory_uri() . '/inc/js/script.js', array('jquery', 'jquery-ui-core', 'jquery-masonry'), ecotheme_VERSION);
}

add_action('wp_enqueue_scripts', 'ecotheme_scripts');

function ecotheme_widgets_init() {

    register_sidebar(array(
        'name' => esc_html__('Right Sidebar', 'ecotheme'),
        'id' => 'sidebar-right',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Left Sidebar', 'ecotheme'),
        'id' => 'sidebar-left',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Shop Sidebar ( WooCommerce )', 'ecotheme'),
        'id' => 'sidebar-shop',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));


    register_sidebar(array(
        'name' => esc_html__('Footer', 'ecotheme'),
        'id' => 'sidebar-footer',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s col-sm-4">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Slider Overlay', 'ecotheme'),
        'id' => 'sidebar-overlay',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s col-sm-6">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Homepage', 'ecotheme'),
        'id' => 'sidebar-homepage',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s col-sm-12">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'ecotheme_widgets_init');



function ecotheme_do_left_sidebar( $args ) {
    
    if( get_theme_mod( 'sidebar_location', 'right' ) == 'none' ) :
        return;
    endif;
    
    if( $args[0] == 'frontpage' && get_theme_mod('home_sidebar') == 'off' )
        return;
    
    if( $args[0] == 'page' && get_theme_mod('page_sidebar') == 'off' )
        return;
    
    if( $args[0] == 'single' && get_theme_mod('single_sidebar') == 'off' )
        return;
    
    
    
    if( get_theme_mod( 'sidebar_location', 'right' ) == 'left' ) :
        
        echo '<div class="col-sm-4" id="ecotheme-sidebar">' .
        get_sidebar() . '</div>';
        
    endif;
    
    
}
add_action('ecotheme-sidebar-left', 'ecotheme_do_left_sidebar');

function ecotheme_do_right_sidebar( $args ) {
    
    if( get_theme_mod( 'sidebar_location', 'right' ) == 'none' ) :
        return;
    endif;
    
    if( $args[0] == 'frontpage' && get_theme_mod('home_sidebar') == 'off' )
        return;
    
    if( $args[0] == 'page' && get_theme_mod('page_sidebar') == 'off' )
        return;
    
    if( $args[0] == 'single' && get_theme_mod('single_sidebar') == 'off' )
        return;
    
    
    
    if( get_theme_mod( 'sidebar_location', 'right' ) == 'right' ) :
        
        echo '<div class="col-sm-4" id="ecotheme-sidebar">';
    
        get_sidebar();
        
        echo '</div>';
        
    endif;
    
    
}
add_action('ecotheme-sidebar-right', 'ecotheme_do_right_sidebar');

function ecotheme_main_width(){
    
    $width = 12;
    
    if( is_active_sidebar('sidebar-left') && is_active_sidebar('sidebar-right') ) :
        
        $width = 6;
        
    elseif( is_active_sidebar('sidebar-left') || is_active_sidebar('sidebar-right') ) :
        $width = 9;
    else:
        $width = 12;
    endif;
    
    
    return $width;
}


function ecotheme_get_image() {

    echo wp_get_attachment_url($POST['id']);

    exit();
}

add_action('wp_ajax_ecotheme_get_image', 'ecotheme_get_image');

function ecotheme_customize_nav($items) {

    if( get_theme_mod( 'show_search', 'on' ) == 'on' ) :
    $items .= '<li class="menu-item"><a class="ecotheme-search" href="#search" role="button" data-toggle="modal"><span class="fa fa-search"></span></a></li>';
    endif;
    
    if( class_exists( 'WooCommerce' ) ) :
        $items .= '<li><a class="ecotheme-cart" href="' . ( function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : WC()->cart->get_cart_url() ) . '"><span class="fa fa-shopping-cart"></span> ' . WC()->cart->get_cart_total() . '</a></li>';
    endif;
    
    
    
    return $items;
}

add_filter('wp_nav_menu_items', 'ecotheme_customize_nav');


function ecotheme_custom_css() {
    ?>
    <style type="text/css">


        body{
            font-size: <?php echo esc_attr( get_theme_mod( 'theme_font_size', '14px') ); ?>;
            font-family: <?php echo esc_attr( get_theme_mod( 'theme_font', 'Raleway, sans-serif' ) ); ?>;

        }
        h1,h2,h3,h4,h5,h6,.slide2-header,.slide1-header,.ecotheme-title, .widget-title,.entry-title, .product_title{
            font-family: <?php echo esc_attr( get_theme_mod('header_font', 'Raleway, sans-serif' ) ); ?>;
        }
        
        ul.ecotheme-nav > li.menu-item a{
            font-size: <?php echo esc_attr( get_theme_mod('menu_font_size', '14px' ) ); ?>;
        }
        
    </style>
    <?php
}

add_action('wp_head', 'ecotheme_custom_css');


function ecotheme_custom_js() { 
    
    
    if( get_theme_mod( 'blog_style', 'tiles' ) === 'tiles' ) :
    
    ?>
    <script type="text/javascript">
    jQuery(document).ready( function($) {
        $('.ecotheme-blog-content').imagesLoaded(function () {
            $('.ecotheme-blog-content').masonry({
                itemSelector: '.ecotheme-blog-post',
                gutter: 0,
                transitionDuration: 0,
            }).masonry('reloadItems');
        });
    });
    </script>
    <?php else : ?>
    <style>.ecotheme-blog-post{ width: 100% !important }</style>
    <?php 
    endif;
}

add_action('wp_head', 'ecotheme_custom_js');


function ecotheme_render_homepage() { ?>

    <div>
        <div>
            <video width="100%" autoplay loop muted>
                  <source src="<?php echo get_template_directory_uri() . '/inc/videos/videobg.mp4';?>" type="video/mp4">
                    Your browser does not support the video tag.
            </video>        
        </div>
        <div style="position: absolute; top: 0; margin-top: 20%; width: 100%;">
            <h2 style="font-size: 4vw; color: #fff;"><?php echo do_shortcode('[countdown date="June 25" width="50%" format="dHMS" radius="5" event="Days Until Launch" link="false"]');?></h2>
        </div>
    </div>
    <div class="clear">        
    </div>

    <div>
        <img src="<?php echo get_template_directory_uri() . '/inc/images/header_title.jpg'; ?>" />
    </div>   
    
    <div class="clear">        
    </div>
        <!-- Featured -->
         <?php if( get_theme_mod('callout_bool', 'on' ) == 'on' ) : ?>

        <div id="ecotheme-featured">
            <div class="col-sm-4 featured-box featured-box1" data-target="<?php echo esc_url( get_theme_mod( 'callout1_href', '#' ) ); ?>">

                <div class="reveal animated fadeInUp reveal">
                    <div class="ecotheme-icon">
                        <span class="<?php echo esc_attr(get_theme_mod('callout1_icon', __('fa fa-laptop', 'ecotheme'))); ?>"></span>
                    </div>

                    <h3 class="ecotheme-title"><?php echo esc_attr(get_theme_mod('callout1_title', __('Responsive', 'ecotheme'))); ?></h3>

                    <p class="ecotheme-desc"><?php echo esc_attr(get_theme_mod('callout1_text', __('ecotheme looks amazing on desktop and mobile devices.', 'ecotheme'))); ?></p>
                </div>

            </div>

            <div class="col-sm-4 featured-box featured-box2" data-target="<?php echo esc_url( get_theme_mod( 'callout2_href', '#' ) ); ?>">

                <div class="reveal animated fadeInUp delay1">

                    <div class="ecotheme-icon">
                        <span class="<?php echo esc_attr(get_theme_mod('callout2_icon', __('fa fa-magic', 'ecotheme'))); ?>"></span>
                    </div>

                    <h3 class="ecotheme-title"><?php echo esc_attr(get_theme_mod('callout2_title', __('Customizable', 'ecotheme'))); ?></h3>

                    <p class="ecotheme-desc"><?php echo esc_attr(get_theme_mod('callout2_text', __('ecotheme is easy to use and customize without having to touch code', 'ecotheme'))); ?></p>

                </div>

            </div>

            <div class="col-sm-4 featured-box featured-box3" data-target="<?php echo esc_url( get_theme_mod( 'callout3_href', '#' ) ); ?>">

                <div class="reveal animated fadeInUp delay2">

                    <div class="ecotheme-icon">
                        <span class="<?php echo esc_attr(get_theme_mod('callout3_icon', __('fa fa-shopping-cart', 'ecotheme'))); ?>"></span>
                    </div>

                    <h3 class="ecotheme-title"><?php echo esc_attr(get_theme_mod('callout3_title', __('WooCommerce', 'ecotheme'))); ?></h3>

                    <p class="ecotheme-desc"><?php echo esc_attr(get_theme_mod('callout3_text', __('ecotheme supports WooCommerce to build an online shopping site', 'ecotheme'))); ?></p>

                </div>
            </div>
        </div>
      <?php endif; ?>

    <!-- =============== -->
    <div id="ecotheme-project">        
        <?php echo do_shortcode('[metaslider id="25"]'); ?>    
    </div>
    <div class="clear">    
    </div>
    
   
    <?php get_sidebar('homepage'); ?>

    
    <?php
}

add_action( 'ecotheme_homepage', 'ecotheme_render_homepage' );


function ecotheme_render_footer(){ ?>
    
    <div class="ecotheme-footer" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo esc_attr( get_theme_mod('footer_background_image', get_template_directory_uri() . '/inc/images/footer.jpg' ) ); ?>">
        <div>
            <div class="row">
                <?php get_sidebar('footer'); ?>
            </div>            
        </div>

        
    </div>
    
    <div class="clear"></div>
    
    <div class="site-info">
        
        <div class="row">
            
            <div class="ecotheme-copyright">
                <?php echo esc_attr( get_theme_mod( 'copyright_text', __( 'Copyright Company Name 2015', 'ecotheme' ) ) ); ?>
            </div>
            
            <div id="authica-social">
                
                <?php if( get_theme_mod( 'facebook_url', 'http://facebook.com' ) != '' ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'facebook_url', 'http://facebook.com' ) ); ?>" target="_BLANK" class="ecotheme-facebook">
                    <span class="fa fa-facebook"></span>
                </a>
                <?php endif; ?>
                
                
                <?php if( get_theme_mod( 'gplus_url', 'http://gplus.com' ) != '' ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'gplus_url', 'http://gplus.com' ) ); ?>" target="_BLANK" class="ecotheme-gplus">
                    <span class="fa fa-google-plus"></span>
                </a>
                <?php endif; ?>
                
                <?php if( get_theme_mod( 'instagram_url', 'http://instagram.com' ) != '' ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'instagram_url', 'http://instagram.com' ) ); ?>" target="_BLANK" class="ecotheme-instagram">
                    <span class="fa fa-instagram"></span>
                </a>
                <?php endif; ?>
                
                <?php if( get_theme_mod( 'linkedin_url', 'http://linkedin.com' ) != '' ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'linkedin_url', 'http://linkedin.com' ) ); ?>" target="_BLANK" class="ecotheme-linkedin">
                    <span class="fa fa-linkedin"></span>
                </a>
                <?php endif; ?>
                
                
                <?php if( get_theme_mod( 'pinterest_url', 'http://pinterest.com' ) != '' ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'pinterest_url', 'http://pinterest.com' ) ); ?>" target="_BLANK" class="ecotheme-pinterest">
                    <span class="fa fa-pinterest"></span>
                </a>
                <?php endif; ?>
                
                <?php if( get_theme_mod( 'twitter_url', 'http://twitter.com' ) ) : ?>
                <a href="<?php echo esc_url( get_theme_mod( 'twitter_url', 'http://twitter.com' ) ); ?>" target="_BLANK" class="ecotheme-twitter">
                    <span class="fa fa-twitter"></span>
                </a>
                <?php endif; ?>
                
            </div>

            <?php $menu = wp_nav_menu( array ( 
                'theme_location'    => 'footer', 
                'menu_id'           => 'footer-menu', 
                'menu_class'        => 'ecotheme-footer-nav' ,

                ) ); ?>
            <br>

            <a href="#" rel="designer" style="display: block !important" class="rel">
                <?php _e( 'Design by' , 'ecotheme' ); echo ' Ecoprotech'; ?>
                <img src="<?php echo get_template_directory_uri() . '/inc/images/logo.png'?>"/>
            </a>
            
            
        </div>
        
        <div class="scroll-top alignright">
            <span class="fa fa-chevron-up"></span>
        </div>
        

        
    </div><!-- .site-info -->
    
    
<?php }
add_action( 'ecotheme_footer', 'ecotheme_render_footer' );



class ecotheme_recent_posts_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'ecotheme_recent_posts_widget', __('ecotheme Recent Articles', 'ecotheme'), array('description' => __('Use this widget to display the ecotheme Recent Posts.', 'ecotheme'),)
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget($args, $instance) {
        
        if( isset( $instance['title'] ) ) :
            $title = apply_filters('widget_title', $instance['title'] );
        else : 
            $title = '';
        endif;
        

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        
        echo ecotheme_recent_posts();

    }

    // Widget Backend
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Recent Articles', 'ecotheme');
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'ecotheme'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />             
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

add_action('widgets_init', 'ecotheme_load_widget');
function ecotheme_load_widget() {
    register_widget('ecotheme_recent_posts_widget');
}

function ecotheme_recent_posts() {
    $args = array(
        'numberposts' => '6',
        'post_status' => 'publish',
    );
    ?>
    <div id="ecotheme_recent_posts">
        <?php $recent_posts = wp_get_recent_posts($args);
        foreach ($recent_posts as $post) { ?>
            <div class="col-sm-4 ecotheme-single-post">
                <div>
                    <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post['ID'])); ?>
                    <img src="<?php echo $url; ?> " title="<?php echo $post['post_title']; ?>"/>
                    <div class="overlay">
                        <a href="<?php echo get_permalink($post['ID']) ?>" class="title"><?php echo $post['post_title']; ?></a>
                        <br>
                        <br>
                        <div class="center">
                            <a href="<?php echo get_permalink($post['ID']) ?>" class=""><i class="fa fa-external-link"></i></a>
                        </div>
                    </div>
                </div>
            </div>
    <?php } ?>
        <?php wp_reset_postdata(); ?>
    </div>
<?php
}

