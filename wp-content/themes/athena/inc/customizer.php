<?php

/**
 * ecotheme Theme Customizer.
 *
 * @package ecotheme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function ecotheme_customize_register( $wp_customize ) {


    // reset some stuff
    $wp_customize->remove_section( 'header_image' );
    $wp_customize->remove_section( 'background_image' );
    $wp_customize->remove_section( 'colors' );
    $wp_customize->remove_section( 'static_front_page' );
    $wp_customize->remove_section( 'title_tagline' );

    // *********************************************
    // ****************** General ******************
    // *********************************************
    
    $wp_customize->add_panel( 'logo', array (
        'title' => __( 'Logo, Title & Favicon', 'ecotheme' ),
        'description' => __( 'set the logo image, site title, description and site icon favicon', 'ecotheme' ),
        'priority' => 10
    ) );
    
    $wp_customize->add_section( 'logo', array (
        'title'                 => __( 'Logo', 'ecotheme' ),
        'panel'                 => 'logo',
    ) );
    
    $wp_customize->add_panel( 'general', array (
        'title' => __( 'General', 'ecotheme' ),
        'description' => __( 'General settings for your site, such as title, favicon and more', 'ecotheme' ),
        'priority' => 10
    ) );


    // *********************************************
    // ****************** Slider *****************
    // *********************************************

    $wp_customize->add_panel( 'slider', array (
        'title'                 => __( 'Slider', 'ecotheme' ),
        'description'           => __( 'Customize the slider. ecotheme includes 2 slides, and the pro version supports up to 5', 'ecotheme' ),
        'priority'              => 10
    ) );
    
    $wp_customize->add_section( 'slide1', array (
        'title'                 => __( 'Slide #1', 'ecotheme' ),
        'description'           => __( 'Use the settings below to upload your images, set main callout text and button text & URLs', 'ecotheme' ),
        'panel'                 => 'slider',
    ) );
    
    $wp_customize->add_section( 'slide2', array (
        'title'                 => __( 'Slide #2', 'ecotheme' ),
        'description'           => __( 'Use the settings below to upload your images, set main callout text and button text & URLs', 'ecotheme' ),
        'panel'                 => 'slider',
    ) );

    // 1st slide
    $wp_customize->add_setting( 'featured_image1', array (
        'default'               => get_template_directory_uri() . '/inc/images/ecotheme.jpg',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control1', array (
        'label' =>              __( 'Background Image', 'ecotheme' ),
        'section'               => 'slide1',
        'mime_type'             => 'image',
        'settings'              => 'featured_image1',
        'description'           => __( 'Select the image file that you would like to use as the featured images', 'ecotheme' ),        
    ) ) );

    $wp_customize->add_setting( 'featured_image1_title', array (
        'default'               => __( 'Welcome to Ecoprotech', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'

    ) );
    
    $wp_customize->add_control( 'featured_image1_title', array(
        'type'                  => 'text',
        'section'               => 'slide1',
        'label'                 => __( 'Header Text', 'ecotheme' ),
        'description'           => __( 'The main heading text, leave blank to hide', 'ecotheme' ),
    ) );


    $wp_customize->add_setting( 'slide1_button1_text', array (
        'default'               => __( 'View Features', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'slide1_button1_text', array(
        'type'                  => 'text',
        'section'               => 'slide1',
        'label'                 => __( 'Button #1 Text', 'ecotheme' ),
        'description'           => __( 'The text for the button, leave blank to hide', 'ecotheme' ),
    ) );

    $wp_customize->add_setting( 'slide1_button1_url', array (
        'default'               => '',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'slide1_button1_url', array(
        'type'                  => 'text',
        'section'               => 'slide1',
        'label'                 => __( 'Button #1 URL', 'ecotheme' ),
    ) );
   

    $wp_customize->add_setting( 'slide1_button2_text', array (
        'default'               => __( 'Learn More', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'slide1_button2_text', array(
        'type'                  => 'text',
        'section'               => 'slide1',
        'label'                 => __( 'Button #2 Text', 'ecotheme' ),
        'description'           => __( 'The text for the button, leave blank to hide', 'ecotheme' ),
    ) );

    $wp_customize->add_setting( 'slide1_button2_url', array (
        'default'               => '',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'slide1_button2_url', array(
        'type'                  => 'text',
        'section'               => 'slide1',
        'label'                 => __( 'Button #2 URL', 'ecotheme' ),
    ) );
    
    
    // 2nd slide
    $wp_customize->add_setting( 'featured_image2', array (
        'default'               => get_template_directory_uri() . '/inc/images/ecotheme2.jpg',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control2', array (
        'label' =>              __( 'Background Image', 'ecotheme' ),
        'section'               => 'slide2',
        'mime_type'             => 'image',
        'settings'              => 'featured_image2',
        'description'           => __( 'Select the image file that you would like to use as the featured images', 'ecotheme' ),        
    ) ) );

    $wp_customize->add_setting( 'featured_image2_title', array (
        'default'               => __( 'Welcome to ecotheme', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'featured_image2_title', array(
        'type'                  => 'text',
        'section'               => 'slide2',
        'label'                 => __( 'Header Text', 'ecotheme' ),
        'description'           => __( 'The main heading text, leave blank to hide', 'ecotheme' ),
    ) );

    $wp_customize->add_setting( 'slide2_button1_text', array (
        'default'               => __( 'View Features', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'slide2_button1_text', array(
        'type'                  => 'text',
        'section'               => 'slide2',
        'label'                 => __( 'Button #1 Text', 'ecotheme' ),
        'description'           => __( 'The text for the button, leave blank to hide', 'ecotheme' ),
    ) );

    $wp_customize->add_setting( 'slide2_button1_url', array (
        'default'               => '',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'slide2_button1_url', array(
        'type'                  => 'text',
        'section'               => 'slide2',
        'label'                 => __( 'Button #1 URL', 'ecotheme' ),
    ) );
    

    $wp_customize->add_setting( 'slide2_button2_text', array (
        'default'               => __( 'Learn More', 'ecotheme' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'slide2_button2_text', array(
        'type'                  => 'text',
        'section'               => 'slide2',
        'label'                 => __( 'Button #2 Text', 'ecotheme' ),
        'description'           => __( 'The text for the button, leave blank to hide', 'ecotheme' ),
    ) );

    $wp_customize->add_setting( 'slide2_button2_url', array (
        'default'               => '',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'slide2_button2_url', array(
        'type'                  => 'text',
        'section'               => 'slide2',
        'label'                 => __( 'Button #2 URL', 'ecotheme' ),
    ) );
    
    
    // *********************************************
    // ****************** Homepage *****************
    // *********************************************
    $wp_customize->add_panel( 'homepage', array (
        'title'                 => __( 'Frontpage', 'ecotheme' ),
        'description'           => __( 'Customize the appearance of your homepage', 'ecotheme' ),
        'priority'              => 10
    ) );

    $wp_customize->add_section( 'homepage_callouts', array (
        'title'                 => __( 'Icon Callouts', 'ecotheme' ),
        'panel'                 => 'homepage',
    ) );

    $wp_customize->add_section( 'homepage_widget', array (
        'title'                 => __( 'Homepage Widget', 'ecotheme' ),
        'panel'                 => 'homepage',
    ) );

    $wp_customize->add_section( 'blog_layout', array (
        'title'                 => __( 'Blog Layout', 'ecotheme' ),
        'panel'                 => 'appearance',
    ) );

    $wp_customize->add_section( 'site_search', array (
        'title'                 => __( 'Site Search Icon', 'ecotheme' ),
        'panel'                 => 'appearance',
    ) );
    
    // Widget
    $wp_customize->add_setting( 'homepage_widget_background', array (
        'default'               => get_template_directory_uri() . '/inc/images/widget.jpg',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control5', array (
        'label' =>              __( 'Widget Background', 'ecotheme' ),
        'section'               => 'homepage_widget',
        'mime_type'             => 'image',
        'settings'              => 'homepage_widget_background',
        'description'           => __( 'Select the image file that you would like to use as the background image. You can change the contents of this Widget from <strong>Appearance - Widgets</strong>', 'ecotheme' ),        
    ) ) );
    

    $wp_customize->add_section( 'homepage_overlay', array (
        'title'                 => __( 'Overlay', 'ecotheme' ),
        'description'           => __( 'The overlay appears after the user clicks the icon on the bottom-right of the slider', 'ecotheme' ),
        'panel'                 => 'homepage',
    ) );

    $wp_customize->add_section( 'static_front_page', array (
        'title' => __( 'Static Front Page', 'ecotheme' ),
        'panel' => 'homepage',
    ) );
    
    $wp_customize->add_section( 'title_tagline', array (
        'title' => __( 'Site Title, Tagline & Favicon', 'ecotheme' ),
        'panel' => 'logo',
    ) );
    
    $wp_customize->add_setting( 'overlay_bool', array (
        'default'               => 'on',
        'transport'             => 'refresh',
        'sanitize_callback'     => 'ecotheme_on_off_sanitize'
    ) );
    
   $wp_customize->add_control( 'overlay_bool', array(
        'label'   => __( 'Enable Homepage Overlay Widget', 'ecotheme' ),
        'section' => 'homepage_overlay',
        'type'    => 'radio',
        'choices'    => array(
            'on'    => __( 'Show', 'ecotheme' ),
            'off'    => __( 'Hide', 'ecotheme' )
        )
    ));

    $wp_customize->add_setting( 'overlay_icon', array (
        'default'               => 'fa fa-question-circle',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_icon_sanitize'
    ) );
    
   $wp_customize->add_control( 'overlay_icon', array(
        'label'   => __( 'Overlay Trigger Icon', 'ecotheme' ),
        'section' => 'homepage_overlay',
        'type'    => 'select',
        'choices'    => ecotheme_icons()
    ));
   
    $wp_customize->add_setting( 'blog_style', array (
        'default'               => 'tiles',
        'transport'             => 'refresh',
        'sanitize_callback'     => 'ecotheme_blogstyle_sanitize'
    ) );
    
   $wp_customize->add_control( 'blog_style', array(
        'label'   => __( 'Select the blog layout you prefer', 'ecotheme' ),
        'section' => 'blog_layout',
        'type'    => 'radio',
        'choices'    => array(
            'stacked'    => __( 'Stacked', 'ecotheme' ),
            'tiles'    => __( 'Tiles', 'ecotheme' )
        )
    ));
   
    $wp_customize->add_setting( 'show_search', array (
        'default'               => 'on',
        'transport'             => 'refresh',
        'sanitize_callback'     => 'ecotheme_on_off_sanitize'
    ) );
    
   $wp_customize->add_control( 'show_search', array(
        'label'   => __( 'Toggle the search icon in the menu', 'ecotheme' ),
        'section' => 'site_search',
        'type'    => 'radio',
        'choices'    => array(
            'on'    => __( 'On', 'ecotheme' ),
            'off'    => __( 'Off', 'ecotheme' )
        )
    ));
    
    
   // **************************************************
   // ********************* Callouts *******************
   // **************************************************

    // callouts setting
    $wp_customize->add_setting('callout_bool', array(
        'default' => 'on',
        'transport' => 'refresh',
        'sanitize_callback' => 'ecotheme_on_off_sanitize'
    ));

    $wp_customize->add_control('callout_bool', array(
        'label' => __('Display Icon Callouts on Frontpage', 'ecotheme'),
        'section' => 'homepage_callouts',
        'type' => 'radio',
        'choices' => array(
            'on' => __('Show', 'ecotheme'),
            'off' => __('Hide', 'ecotheme')
        )
    ));
    // Callout #1
    $wp_customize->add_setting('callout1_icon', array(
        'default' => 'fa fa-laptop',
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_icon_sanitize'
    ));

    $wp_customize->add_control('callout1_icon', array(
        'label' => __('Callout #1: Select Icon', 'ecotheme'),
        'section' => 'homepage_callouts',
        'type' => 'select',
        'choices' => ecotheme_icons()
    ));

    $wp_customize->add_setting('callout1_title', array(
        'default' => 'Responsive',
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout1_title', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #1: Title', 'ecotheme'),
        'description' => __('Set the callout title text', 'ecotheme'),
    ));

    $wp_customize->add_setting('callout1_href', array(
        'default' => '#',
        'transport' => 'postMessage',
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_control('callout1_href', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #1: Link', 'ecotheme'),
        'description' => __('Set the callout link URL', 'ecotheme'),
    ));

    $wp_customize->add_setting('callout1_text', array(
        'default' => __('ecotheme is a carefully designed and developed theme that you can use to make your site stand out', 'ecotheme'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout1_text', array(
        'type' => 'textarea',
        'section' => 'homepage_callouts',
        'label' => __('Callout #1: Description', 'ecotheme'),
        'description' => __('Set the callout detail text', 'ecotheme'),
    ));

    // Callout #2
    $wp_customize->add_setting('callout2_icon', array(
        'default' => 'fa fa-magic',
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_icon_sanitize'
    ));

    $wp_customize->add_control('callout2_icon', array(
        'label' => __('Callout #2: Select Icon', 'ecotheme'),
        'section' => 'homepage_callouts',
        'type' => 'select',
        'choices' => ecotheme_icons()
    ));

    $wp_customize->add_setting('callout2_title', array(
        'default' => __('Customizable', 'ecotheme'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout2_title', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #2: Title', 'ecotheme'),
        'description' => __('Set the callout title text', 'ecotheme'),
    ));
    
    $wp_customize->add_setting('callout2_href', array(
        'default' => '#',
        'transport' => 'postMessage',
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_control('callout2_href', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #2: Link', 'ecotheme'),
        'description' => __('Set the callout link URL', 'ecotheme'),
    ));

    $wp_customize->add_setting('callout2_text', array(
        'default' => __('ecotheme is easy to use and customize without having to touch code', 'ecotheme'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout2_text', array(
        'type' => 'textarea',
        'section' => 'homepage_callouts',
        'label' => __('Callout #2: Description', 'ecotheme'),
        'description' => __('Set the callout detail text', 'ecotheme'),
    ));

    // Callout #3
    $wp_customize->add_setting('callout3_icon', array(
        'default' => 'fa fa-shopping-cart',
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_icon_sanitize'
    ));

    $wp_customize->add_control('callout3_icon', array(
        'label' => __('Callout #3: Select Icon', 'ecotheme'),
        'section' => 'homepage_callouts',
        'type' => 'select',
        'choices' => ecotheme_icons()
    ));

    $wp_customize->add_setting('callout3_title', array(
        'default' => __('WooCommerce', 'ecotheme'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout3_title', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #3: Title', 'ecotheme'),
        'description' => __('Set the callout title text', 'ecotheme'),
    ));
    
    $wp_customize->add_setting('callout3_href', array(
        'default' => '#',
        'transport' => 'postMessage',
        'sanitize_callback' => 'esc_url_raw'
    ));

    $wp_customize->add_control('callout3_href', array(
        'type' => 'text',
        'section' => 'homepage_callouts',
        'label' => __('Callout #3: Link', 'ecotheme'),
        'description' => __('Set the callout link URL', 'ecotheme'),
    ));

    $wp_customize->add_setting('callout3_text', array(
        'default' => __('ecotheme supports WooCommerce to build an online shopping site', 'ecotheme'),
        'transport' => 'postMessage',
        'sanitize_callback' => 'ecotheme_text_sanitize'
    ));

    $wp_customize->add_control('callout3_text', array(
        'type' => 'textarea',
        'section' => 'homepage_callouts',
        'label' => __('Callout #3: Description', 'ecotheme'),
        'description' => __('Set the callout detail text', 'ecotheme'),
    ));
    
    // *********************************************
    // ****************** Apperance *****************
    // *********************************************
    $wp_customize->add_panel( 'appearance', array (
        'title'                 => __( 'Appearance', 'ecotheme' ),
        'description'           => __( 'Customize your site colros, fonts and other appearance settings', 'ecotheme' ),
        'priority'              => 10
    ) );
    

    
    $wp_customize->add_section( 'color', array (
        'title'                 => __( 'Skin Color', 'ecotheme' ),
        'panel'                 => 'appearance',
    ) );
    
    $wp_customize->add_section( 'font', array (
        'title'                 => __( 'Fonts', 'ecotheme' ),
        'panel'                 => 'appearance',
    ) );
    
    // Logo Bool
    $wp_customize->add_setting( 'logo_bool', array (
        'default'               => 'on',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_on_off_sanitize'
    ) );

    $wp_customize->add_control( 'logo_bool', array(
        'type'                  => 'radio',
        'section'               => 'logo',
        'label'                 => __( 'Display Logo', 'ecotheme' ),
        'description'           => __( 'If you do not use a logo, the site title will be displayed', 'ecotheme' ),  
        'choices'               => array(
            'on'    => __( 'Show', 'ecotheme' ),
            'off'    => __( 'Hide', 'ecotheme' )
        )
    ) );
    
    // Logo Image
    $wp_customize->add_setting( 'logo', array (
        'default'               => get_template_directory_uri() . '/inc/images/logo.png',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control4', array (
        'label' =>              __( 'Logo', 'ecotheme' ),
        'section'               => 'logo',
        'mime_type'             => 'image',
        'settings'              => 'logo',
        'description'           => __( 'Image for your site', 'ecotheme' ),        
    ) ) );
    


    
    $wp_customize->add_setting( 'theme_color', array (
        'default'               => 'green',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_theme_color_sanitize'
    ) );
    
    $wp_customize->add_control( 'theme_color', array(
        'type'                  => 'radio',
        'section'               => 'color',
        'label'                 => __( 'Theme Skin Color', 'ecotheme' ),
        'description'           => __( 'Select the theme main color', 'ecotheme' ),
        'choices'               => array(
            'green'             => __( 'Green', 'ecotheme' ),
            'blue'              => __( 'Blue', 'ecotheme' ),
            'red'               => __( 'Red', 'ecotheme' ),
            'pink'              => __( 'Pink', 'ecotheme' ),
            'yellow'            => __( 'Yellow', 'ecotheme' ),
            'darkblue'          => __( 'Dark Blue', 'ecotheme' ),
        )
        
    ) );
    
    $wp_customize->add_setting( 'header_font', array (
        'default'               => 'Raleway, sans-serif',
        'transport'             => 'refresh',
        'sanitize_callback'     => 'ecotheme_font_sanitize'
    ) );
    
    $wp_customize->add_control( 'header_font', array(
        'type'                  => 'select',
        'section'               => 'font',
        'label'                 => __( 'Headers Font', 'ecotheme' ),
        'description'           => __( 'Applies to the slider header, callouts headers, post page & widget titles etc..', 'ecotheme' ),
        'choices'               => ecotheme_fonts()
        
    ) );
    
    $wp_customize->add_setting( 'theme_font', array (
        'default'               => 'Raleway, sans-serif',
        'transport'             => 'refresh',
        'sanitize_callback'     => 'ecotheme_font_sanitize'
    ) );
    
    $wp_customize->add_control( 'theme_font', array(
        'type'                  => 'select',
        'section'               => 'font',
        'label'                 => __( 'General font for the site body', 'ecotheme' ),
        'choices'               => ecotheme_fonts()
        
    ) );
    
    
    $wp_customize->add_setting( 'menu_font_size', array (
        'default'               => '14px',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_font_size_sanitize'
    ) );
    
    $wp_customize->add_control( 'menu_font_size', array(
        'type'                  => 'select',
        'section'               => 'font',
        'label'                 => __( 'Menu Font Size', 'ecotheme' ),
        'choices'               => ecotheme_font_sizes()
        
    ) );
    
    $wp_customize->add_setting( 'theme_font_size', array (
        'default'               => '14px',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_font_size_sanitize'
    ) );
    
    $wp_customize->add_control( 'theme_font_size', array(
        'type'                  => 'select',
        'section'               => 'font',
        'label'                 => __( 'Content Font Size', 'ecotheme' ),
        'choices'               => ecotheme_font_sizes()
        
    ) );
    
    
    // *********************************************
    // ****************** Footer *****************
    // *********************************************
    $wp_customize->add_panel( 'footer', array (
        'title'                 => __( 'Footer', 'ecotheme' ),
        'description'           => __( 'Customize the site footer', 'ecotheme' ),
        'priority'              => 10
    ) );
    
    $wp_customize->add_section( 'footer_background', array (
        'title'                 => __( 'Footer Background', 'ecotheme' ),
        'panel'                 => 'footer',
    ) );
    
    $wp_customize->add_setting( 'footer_background_image', array (
        'default'               => get_template_directory_uri() . '/inc/images/footer.jpg',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'image_control3', array (
        'label' =>              __( 'Footer Background Image ( Parallax )', 'ecotheme' ),
        'section'               => 'footer_background',
        'mime_type'             => 'image',
        'settings'              => 'footer_background_image',
        'description'           => __( 'Select the image file that you would like to use as the footer background. You can change the contents of this Widget from <strong>Appearance - Widgets</strong>', 'ecotheme' ),        
    ) ) );
    
    $wp_customize->add_section( 'footer_text', array (
        'title'                 => __( 'Copyright Text', 'ecotheme' ),
        'panel'                 => 'footer',
    ) );
    
    $wp_customize->add_setting( 'copyright_text', array (
        'default'               => __( 'Copyright Company Name', 'ecotheme' ) . date( 'Y' ),
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'ecotheme_text_sanitize'
    ) );
    
    $wp_customize->add_control( 'copyright_text', array(
        'type'                  => 'text',
        'section'               => 'footer_text',
        'label'                 => __( 'Copyright Text', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_section( 'social_links', array (
        'title'                 => __( 'Social Icons & Links', 'ecotheme' ),
        'panel'                 => 'footer',
    ) );
    
    $wp_customize->add_setting( 'facebook_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'facebook_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Facebook URL', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_setting( 'gplus_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'gplus_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Google Plus URL', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_setting( 'instagram_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'instagram_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Instagram URL', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_setting( 'linkedin_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'linkedin_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Linkedin URL', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_setting( 'pinterest_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'pinterest_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Pinterest URL', 'ecotheme' )
        
    ) );
    
    $wp_customize->add_setting( 'twitter_url', array (
        'default'               => '#',
        'transport'             => 'postMessage',
        'sanitize_callback'     => 'esc_url_raw'
    ) );
    
    $wp_customize->add_control( 'twitter_url', array(
        'type'                  => 'text',
        'section'               => 'social_links',
        'label'                 => __( 'Twitter URL', 'ecotheme' )
        
    ) );
    
    // *********************************************
    // ****************** Social Icons *****************
    // *********************************************
    $wp_customize->add_panel( 'social', array (
        'title'                 => __( 'Social', 'ecotheme' ),
        'description'           => __( 'Social Icons, Links & Location', 'ecotheme' ),
        'priority'              => 10
    ) );
   
    
    $wp_customize->get_setting( 'blogname' )->transport             = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport      = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport     = 'postMessage';
    $wp_customize->get_setting( 'featured_image1' )->transport      = 'postMessage';
    $wp_customize->get_setting( 'featured_image2' )->transport      = 'postMessage';
    $wp_customize->get_setting( 'callout1_icon' )->transport      = 'postMessage';
//    $wp_customize->get_setting( 'header_font' )->transport      = 'postMessage';
    
}

add_action( 'customize_register', 'ecotheme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */

function ecotheme_customize_enqueue() {
    
    wp_enqueue_script( 'ecotheme-customizer-js', get_template_directory_uri() . '/inc/js/customizer.js', array( 'jquery', 'customize-controls' ), false, true );
    wp_enqueue_style('ecotheme-customizer-css', get_template_directory_uri() . '/inc/css/customizer.css', array(), ecotheme_VERSION);
}
add_action( 'customize_controls_enqueue_scripts', 'ecotheme_customize_enqueue' );

function ecotheme_customize_preview_js() {
    wp_enqueue_script( 'ecotheme_customizer', get_template_directory_uri() . '/js/customizer.js', array ( 'customize-preview' ), ecotheme_VERSION, true );
}

add_action( 'customize_preview_init', 'ecotheme_customize_preview_js' );


function ecotheme_icons(){
    
    return array( 
        'fa fa-clock' => __( 'Select One', 'ecotheme'), 
        'fa fa-500px' => __( ' 500px', 'ecotheme'), 
        'fa fa-amazon' => __( ' amazon', 'ecotheme'), 
        'fa fa-balance-scale' => __( ' balance-scale', 'ecotheme'), 'fa fa-battery-0' => __( ' battery-0', 'ecotheme'), 'fa fa-battery-1' => __( ' battery-1', 'ecotheme'), 'fa fa-battery-2' => __( ' battery-2', 'ecotheme'), 'fa fa-battery-3' => __( ' battery-3', 'ecotheme'), 'fa fa-battery-4' => __( ' battery-4', 'ecotheme'), 'fa fa-battery-empty' => __( ' battery-empty', 'ecotheme'), 'fa fa-battery-full' => __( ' battery-full', 'ecotheme'), 'fa fa-battery-half' => __( ' battery-half', 'ecotheme'), 'fa fa-battery-quarter' => __( ' battery-quarter', 'ecotheme'), 'fa fa-battery-three-quarters' => __( ' battery-three-quarters', 'ecotheme'), 'fa fa-black-tie' => __( ' black-tie', 'ecotheme'), 'fa fa-calendar-check-o' => __( ' calendar-check-o', 'ecotheme'), 'fa fa-calendar-minus-o' => __( ' calendar-minus-o', 'ecotheme'), 'fa fa-calendar-plus-o' => __( ' calendar-plus-o', 'ecotheme'), 'fa fa-calendar-times-o' => __( ' calendar-times-o', 'ecotheme'), 'fa fa-cc-diners-club' => __( ' cc-diners-club', 'ecotheme'), 'fa fa-cc-jcb' => __( ' cc-jcb', 'ecotheme'), 'fa fa-chrome' => __( ' chrome', 'ecotheme'), 'fa fa-clone' => __( ' clone', 'ecotheme'), 'fa fa-commenting' => __( ' commenting', 'ecotheme'), 'fa fa-commenting-o' => __( ' commenting-o', 'ecotheme'), 'fa fa-contao' => __( ' contao', 'ecotheme'), 'fa fa-creative-commons' => __( ' creative-commons', 'ecotheme'), 'fa fa-expeditedssl' => __( ' expeditedssl', 'ecotheme'), 'fa fa-firefox' => __( ' firefox', 'ecotheme'), 'fa fa-fonticons' => __( ' fonticons', 'ecotheme'), 'fa fa-genderless' => __( ' genderless', 'ecotheme'), 'fa fa-get-pocket' => __( ' get-pocket', 'ecotheme'), 'fa fa-gg' => __( ' gg', 'ecotheme'), 'fa fa-gg-circle' => __( ' gg-circle', 'ecotheme'), 'fa fa-hand-grab-o' => __( ' hand-grab-o', 'ecotheme'), 'fa fa-hand-lizard-o' => __( ' hand-lizard-o', 'ecotheme'), 'fa fa-hand-paper-o' => __( ' hand-paper-o', 'ecotheme'), 'fa fa-hand-peace-o' => __( ' hand-peace-o', 'ecotheme'), 'fa fa-hand-pointer-o' => __( ' hand-pointer-o', 'ecotheme'), 'fa fa-hand-rock-o' => __( ' hand-rock-o', 'ecotheme'), 'fa fa-hand-scissors-o' => __( ' hand-scissors-o', 'ecotheme'), 'fa fa-hand-spock-o' => __( ' hand-spock-o', 'ecotheme'), 'fa fa-hand-stop-o' => __( ' hand-stop-o', 'ecotheme'), 'fa fa-hourglass' => __( ' hourglass', 'ecotheme'), 'fa fa-hourglass-1' => __( ' hourglass-1', 'ecotheme'), 'fa fa-hourglass-2' => __( ' hourglass-2', 'ecotheme'), 'fa fa-hourglass-3' => __( ' hourglass-3', 'ecotheme'), 'fa fa-hourglass-end' => __( ' hourglass-end', 'ecotheme'), 'fa fa-hourglass-half' => __( ' hourglass-half', 'ecotheme'), 'fa fa-hourglass-o' => __( ' hourglass-o', 'ecotheme'), 'fa fa-hourglass-start' => __( ' hourglass-start', 'ecotheme'), 'fa fa-houzz' => __( ' houzz', 'ecotheme'), 'fa fa-i-cursor' => __( ' i-cursor', 'ecotheme'), 'fa fa-industry' => __( ' industry', 'ecotheme'), 'fa fa-internet-explorer' => __( ' internet-explorer', 'ecotheme'), 'fa fa-map' => __( ' map', 'ecotheme'), 'fa fa-map-o' => __( ' map-o', 'ecotheme'), 'fa fa-map-pin' => __( ' map-pin', 'ecotheme'), 'fa fa-map-signs' => __( ' map-signs', 'ecotheme'), 'fa fa-mouse-pointer' => __( ' mouse-pointer', 'ecotheme'), 'fa fa-object-group' => __( ' object-group', 'ecotheme'), 'fa fa-object-ungroup' => __( ' object-ungroup', 'ecotheme'), 'fa fa-odnoklassniki' => __( ' odnoklassniki', 'ecotheme'), 'fa fa-odnoklassniki-square' => __( ' odnoklassniki-square', 'ecotheme'), 'fa fa-opencart' => __( ' opencart', 'ecotheme'), 'fa fa-opera' => __( ' opera', 'ecotheme'), 'fa fa-optin-monster' => __( ' optin-monster', 'ecotheme'), 'fa fa-registered' => __( ' registered', 'ecotheme'), 'fa fa-safari' => __( ' safari', 'ecotheme'), 'fa fa-sticky-note' => __( ' sticky-note', 'ecotheme'), 'fa fa-sticky-note-o' => __( ' sticky-note-o', 'ecotheme'), 'fa fa-television' => __( ' television', 'ecotheme'), 'fa fa-trademark' => __( ' trademark', 'ecotheme'), 'fa fa-tripadvisor' => __( ' tripadvisor', 'ecotheme'), 'fa fa-tv' => __( ' tv', 'ecotheme'), 'fa fa-vimeo' => __( ' vimeo', 'ecotheme'), 'fa fa-wikipedia-w' => __( ' wikipedia-w', 'ecotheme'), 'fa fa-y-combinator' => __( ' y-combinator', 'ecotheme'), 'fa fa-yc' => __( ' yc', 'ecotheme'), 'fa fa-adjust' => __( ' adjust', 'ecotheme'), 'fa fa-anchor' => __( ' anchor', 'ecotheme'), 'fa fa-archive' => __( ' archive', 'ecotheme'), 'fa fa-area-chart' => __( ' area-chart', 'ecotheme'), 'fa fa-arrows' => __( ' arrows', 'ecotheme'), 'fa fa-arrows-h' => __( ' arrows-h', 'ecotheme'), 'fa fa-arrows-v' => __( ' arrows-v', 'ecotheme'), 'fa fa-asterisk' => __( ' asterisk', 'ecotheme'), 'fa fa-at' => __( ' at', 'ecotheme'), 'fa fa-automobile' => __( ' automobile', 'ecotheme'), 'fa fa-balance-scale' => __( ' balance-scale', 'ecotheme'), 'fa fa-ban' => __( ' ban', 'ecotheme'), 'fa fa-bank' => __( ' bank', 'ecotheme'), 'fa fa-bar-chart' => __( ' bar-chart', 'ecotheme'), 'fa fa-bar-chart-o' => __( ' bar-chart-o', 'ecotheme'), 'fa fa-barcode' => __( ' barcode', 'ecotheme'), 'fa fa-bars' => __( ' bars', 'ecotheme'), 'fa fa-battery-0' => __( ' battery-0', 'ecotheme'), 'fa fa-battery-1' => __( ' battery-1', 'ecotheme'), 'fa fa-battery-2' => __( ' battery-2', 'ecotheme'), 'fa fa-battery-3' => __( ' battery-3', 'ecotheme'), 'fa fa-battery-4' => __( ' battery-4', 'ecotheme'), 'fa fa-battery-empty' => __( ' battery-empty', 'ecotheme'), 'fa fa-battery-full' => __( ' battery-full', 'ecotheme'), 'fa fa-battery-half' => __( ' battery-half', 'ecotheme'), 'fa fa-battery-quarter' => __( ' battery-quarter', 'ecotheme'), 'fa fa-battery-three-quarters' => __( ' battery-three-quarters', 'ecotheme'), 'fa fa-bed' => __( ' bed', 'ecotheme'), 'fa fa-beer' => __( ' beer', 'ecotheme'), 'fa fa-bell' => __( ' bell', 'ecotheme'), 'fa fa-bell-o' => __( ' bell-o', 'ecotheme'), 'fa fa-bell-slash' => __( ' bell-slash', 'ecotheme'), 'fa fa-bell-slash-o' => __( ' bell-slash-o', 'ecotheme'), 'fa fa-bicycle' => __( ' bicycle', 'ecotheme'), 'fa fa-binoculars' => __( ' binoculars', 'ecotheme'), 'fa fa-birthday-cake' => __( ' birthday-cake', 'ecotheme'), 'fa fa-bolt' => __( ' bolt', 'ecotheme'), 'fa fa-bomb' => __( ' bomb', 'ecotheme'), 'fa fa-book' => __( ' book', 'ecotheme'), 'fa fa-bookmark' => __( ' bookmark', 'ecotheme'), 'fa fa-bookmark-o' => __( ' bookmark-o', 'ecotheme'), 'fa fa-briefcase' => __( ' briefcase', 'ecotheme'), 'fa fa-bug' => __( ' bug', 'ecotheme'), 'fa fa-building' => __( ' building', 'ecotheme'), 'fa fa-building-o' => __( ' building-o', 'ecotheme'), 'fa fa-bullhorn' => __( ' bullhorn', 'ecotheme'), 'fa fa-bullseye' => __( ' bullseye', 'ecotheme'), 'fa fa-bus' => __( ' bus', 'ecotheme'), 'fa fa-cab' => __( ' cab', 'ecotheme'), 'fa fa-calculator' => __( ' calculator', 'ecotheme'), 'fa fa-calendar' => __( ' calendar', 'ecotheme'), 'fa fa-calendar-check-o' => __( ' calendar-check-o', 'ecotheme'), 'fa fa-calendar-minus-o' => __( ' calendar-minus-o', 'ecotheme'), 'fa fa-calendar-o' => __( ' calendar-o', 'ecotheme'), 'fa fa-calendar-plus-o' => __( ' calendar-plus-o', 'ecotheme'), 'fa fa-calendar-times-o' => __( ' calendar-times-o', 'ecotheme'), 'fa fa-camera' => __( ' camera', 'ecotheme'), 'fa fa-camera-retro' => __( ' camera-retro', 'ecotheme'), 'fa fa-car' => __( ' car', 'ecotheme'), 'fa fa-caret-square-o-down' => __( ' caret-square-o-down', 'ecotheme'), 'fa fa-caret-square-o-left' => __( ' caret-square-o-left', 'ecotheme'), 'fa fa-caret-square-o-right' => __( ' caret-square-o-right', 'ecotheme'), 'fa fa-caret-square-o-up' => __( ' caret-square-o-up', 'ecotheme'), 'fa fa-cart-arrow-down' => __( ' cart-arrow-down', 'ecotheme'), 'fa fa-cart-plus' => __( ' cart-plus', 'ecotheme'), 'fa fa-cc' => __( ' cc', 'ecotheme'), 'fa fa-certificate' => __( ' certificate', 'ecotheme'), 'fa fa-check' => __( ' check', 'ecotheme'), 'fa fa-check-circle' => __( ' check-circle', 'ecotheme'), 'fa fa-check-circle-o' => __( ' check-circle-o', 'ecotheme'), 'fa fa-check-square' => __( ' check-square', 'ecotheme'), 'fa fa-check-square-o' => __( ' check-square-o', 'ecotheme'), 'fa fa-child' => __( ' child', 'ecotheme'), 'fa fa-circle' => __( ' circle', 'ecotheme'), 'fa fa-circle-o' => __( ' circle-o', 'ecotheme'), 'fa fa-circle-o-notch' => __( ' circle-o-notch', 'ecotheme'), 'fa fa-circle-thin' => __( ' circle-thin', 'ecotheme'), 'fa fa-clock-o' => __( ' clock-o', 'ecotheme'), 'fa fa-clone' => __( ' clone', 'ecotheme'), 'fa fa-close' => __( ' close', 'ecotheme'), 'fa fa-cloud' => __( ' cloud', 'ecotheme'), 'fa fa-cloud-download' => __( ' cloud-download', 'ecotheme'), 'fa fa-cloud-upload' => __( ' cloud-upload', 'ecotheme'), 'fa fa-code' => __( ' code', 'ecotheme'), 'fa fa-code-fork' => __( ' code-fork', 'ecotheme'), 'fa fa-coffee' => __( ' coffee', 'ecotheme'), 'fa fa-cog' => __( ' cog', 'ecotheme'), 'fa fa-cogs' => __( ' cogs', 'ecotheme'), 'fa fa-comment' => __( ' comment', 'ecotheme'), 'fa fa-comment-o' => __( ' comment-o', 'ecotheme'), 'fa fa-commenting' => __( ' commenting', 'ecotheme'), 'fa fa-commenting-o' => __( ' commenting-o', 'ecotheme'), 'fa fa-comments' => __( ' comments', 'ecotheme'), 'fa fa-comments-o' => __( ' comments-o', 'ecotheme'), 'fa fa-compass' => __( ' compass', 'ecotheme'), 'fa fa-copyright' => __( ' copyright', 'ecotheme'), 'fa fa-creative-commons' => __( ' creative-commons', 'ecotheme'), 'fa fa-credit-card' => __( ' credit-card', 'ecotheme'), 'fa fa-crop' => __( ' crop', 'ecotheme'), 'fa fa-crosshairs' => __( ' crosshairs', 'ecotheme'), 'fa fa-cube' => __( ' cube', 'ecotheme'), 'fa fa-cubes' => __( ' cubes', 'ecotheme'), 'fa fa-cutlery' => __( ' cutlery', 'ecotheme'), 'fa fa-dashboard' => __( ' dashboard', 'ecotheme'), 'fa fa-database' => __( ' database', 'ecotheme'), 'fa fa-desktop' => __( ' desktop', 'ecotheme'), 'fa fa-diamond' => __( ' diamond', 'ecotheme'), 'fa fa-dot-circle-o' => __( ' dot-circle-o', 'ecotheme'), 'fa fa-download' => __( ' download', 'ecotheme'), 'fa fa-edit' => __( ' edit', 'ecotheme'), 'fa fa-ellipsis-h' => __( ' ellipsis-h', 'ecotheme'), 'fa fa-ellipsis-v' => __( ' ellipsis-v', 'ecotheme'), 'fa fa-envelope' => __( ' envelope', 'ecotheme'), 'fa fa-envelope-o' => __( ' envelope-o', 'ecotheme'), 'fa fa-envelope-square' => __( ' envelope-square', 'ecotheme'), 'fa fa-eraser' => __( ' eraser', 'ecotheme'), 'fa fa-exchange' => __( ' exchange', 'ecotheme'), 'fa fa-exclamation' => __( ' exclamation', 'ecotheme'), 'fa fa-exclamation-circle' => __( ' exclamation-circle', 'ecotheme'), 'fa fa-exclamation-triangle' => __( ' exclamation-triangle', 'ecotheme'), 'fa fa-external-link' => __( ' external-link', 'ecotheme'), 'fa fa-external-link-square' => __( ' external-link-square', 'ecotheme'), 'fa fa-eye' => __( ' eye', 'ecotheme'), 'fa fa-eye-slash' => __( ' eye-slash', 'ecotheme'), 'fa fa-eyedropper' => __( ' eyedropper', 'ecotheme'), 'fa fa-fax' => __( ' fax', 'ecotheme'), 'fa fa-feed' => __( ' feed', 'ecotheme'), 'fa fa-female' => __( ' female', 'ecotheme'), 'fa fa-fighter-jet' => __( ' fighter-jet', 'ecotheme'), 'fa fa-file-archive-o' => __( ' file-archive-o', 'ecotheme'), 'fa fa-file-audio-o' => __( ' file-audio-o', 'ecotheme'), 'fa fa-file-code-o' => __( ' file-code-o', 'ecotheme'), 'fa fa-file-excel-o' => __( ' file-excel-o', 'ecotheme'), 'fa fa-file-image-o' => __( ' file-image-o', 'ecotheme'), 'fa fa-file-movie-o' => __( ' file-movie-o', 'ecotheme'), 'fa fa-file-pdf-o' => __( ' file-pdf-o', 'ecotheme'), 'fa fa-file-photo-o' => __( ' file-photo-o', 'ecotheme'), 'fa fa-file-picture-o' => __( ' file-picture-o', 'ecotheme'), 'fa fa-file-powerpoint-o' => __( ' file-powerpoint-o', 'ecotheme'), 'fa fa-file-sound-o' => __( ' file-sound-o', 'ecotheme'), 'fa fa-file-video-o' => __( ' file-video-o', 'ecotheme'), 'fa fa-file-word-o' => __( ' file-word-o', 'ecotheme'), 'fa fa-file-zip-o' => __( ' file-zip-o', 'ecotheme'), 'fa fa-film' => __( ' film', 'ecotheme'), 'fa fa-filter' => __( ' filter', 'ecotheme'), 'fa fa-fire' => __( ' fire', 'ecotheme'), 'fa fa-fire-extinguisher' => __( ' fire-extinguisher', 'ecotheme'), 'fa fa-flag' => __( ' flag', 'ecotheme'), 'fa fa-flag-checkered' => __( ' flag-checkered', 'ecotheme'), 'fa fa-flag-o' => __( ' flag-o', 'ecotheme'), 'fa fa-flash' => __( ' flash', 'ecotheme'), 'fa fa-flask' => __( ' flask', 'ecotheme'), 'fa fa-folder' => __( ' folder', 'ecotheme'), 'fa fa-folder-o' => __( ' folder-o', 'ecotheme'), 'fa fa-folder-open' => __( ' folder-open', 'ecotheme'), 'fa fa-folder-open-o' => __( ' folder-open-o', 'ecotheme'), 'fa fa-frown-o' => __( ' frown-o', 'ecotheme'), 'fa fa-futbol-o' => __( ' futbol-o', 'ecotheme'), 'fa fa-gamepad' => __( ' gamepad', 'ecotheme'), 'fa fa-gavel' => __( ' gavel', 'ecotheme'), 'fa fa-gear' => __( ' gear', 'ecotheme'), 'fa fa-gears' => __( ' gears', 'ecotheme'), 'fa fa-gift' => __( ' gift', 'ecotheme'), 'fa fa-glass' => __( ' glass', 'ecotheme'), 'fa fa-globe' => __( ' globe', 'ecotheme'), 'fa fa-graduation-cap' => __( ' graduation-cap', 'ecotheme'), 'fa fa-group' => __( ' group', 'ecotheme'), 'fa fa-hand-grab-o' => __( ' hand-grab-o', 'ecotheme'), 'fa fa-hand-lizard-o' => __( ' hand-lizard-o', 'ecotheme'), 'fa fa-hand-paper-o' => __( ' hand-paper-o', 'ecotheme'), 'fa fa-hand-peace-o' => __( ' hand-peace-o', 'ecotheme'), 'fa fa-hand-pointer-o' => __( ' hand-pointer-o', 'ecotheme'), 'fa fa-hand-rock-o' => __( ' hand-rock-o', 'ecotheme'), 'fa fa-hand-scissors-o' => __( ' hand-scissors-o', 'ecotheme'), 'fa fa-hand-spock-o' => __( ' hand-spock-o', 'ecotheme'), 'fa fa-hand-stop-o' => __( ' hand-stop-o', 'ecotheme'), 'fa fa-hdd-o' => __( ' hdd-o', 'ecotheme'), 'fa fa-headphones' => __( ' headphones', 'ecotheme'), 'fa fa-heart' => __( ' heart', 'ecotheme'), 'fa fa-heart-o' => __( ' heart-o', 'ecotheme'), 'fa fa-heartbeat' => __( ' heartbeat', 'ecotheme'), 'fa fa-history' => __( ' history', 'ecotheme'), 'fa fa-home' => __( ' home', 'ecotheme'), 'fa fa-hotel' => __( ' hotel', 'ecotheme'), 'fa fa-hourglass' => __( ' hourglass', 'ecotheme'), 'fa fa-hourglass-1' => __( ' hourglass-1', 'ecotheme'), 'fa fa-hourglass-2' => __( ' hourglass-2', 'ecotheme'), 'fa fa-hourglass-3' => __( ' hourglass-3', 'ecotheme'), 'fa fa-hourglass-end' => __( ' hourglass-end', 'ecotheme'), 'fa fa-hourglass-half' => __( ' hourglass-half', 'ecotheme'), 'fa fa-hourglass-o' => __( ' hourglass-o', 'ecotheme'), 'fa fa-hourglass-start' => __( ' hourglass-start', 'ecotheme'), 'fa fa-i-cursor' => __( ' i-cursor', 'ecotheme'), 'fa fa-image' => __( ' image', 'ecotheme'), 'fa fa-inbox' => __( ' inbox', 'ecotheme'), 'fa fa-industry' => __( ' industry', 'ecotheme'), 'fa fa-info' => __( ' info', 'ecotheme'), 'fa fa-info-circle' => __( ' info-circle', 'ecotheme'), 'fa fa-institution' => __( ' institution', 'ecotheme'), 'fa fa-key' => __( ' key', 'ecotheme'), 'fa fa-keyboard-o' => __( ' keyboard-o', 'ecotheme'), 'fa fa-language' => __( ' language', 'ecotheme'), 'fa fa-laptop' => __( ' laptop', 'ecotheme'), 'fa fa-leaf' => __( ' leaf', 'ecotheme'), 'fa fa-legal' => __( ' legal', 'ecotheme'), 'fa fa-lemon-o' => __( ' lemon-o', 'ecotheme'), 'fa fa-level-down' => __( ' level-down', 'ecotheme'), 'fa fa-level-up' => __( ' level-up', 'ecotheme'), 'fa fa-life-bouy' => __( ' life-bouy', 'ecotheme'), 'fa fa-life-buoy' => __( ' life-buoy', 'ecotheme'), 'fa fa-life-ring' => __( ' life-ring', 'ecotheme'), 'fa fa-life-saver' => __( ' life-saver', 'ecotheme'), 'fa fa-lightbulb-o' => __( ' lightbulb-o', 'ecotheme'), 'fa fa-line-chart' => __( ' line-chart', 'ecotheme'), 'fa fa-location-arrow' => __( ' location-arrow', 'ecotheme'), 'fa fa-lock' => __( ' lock', 'ecotheme'), 'fa fa-magic' => __( ' magic', 'ecotheme'), 'fa fa-magnet' => __( ' magnet', 'ecotheme'), 'fa fa-mail-forward' => __( ' mail-forward', 'ecotheme'), 'fa fa-mail-reply' => __( ' mail-reply', 'ecotheme'), 'fa fa-mail-reply-all' => __( ' mail-reply-all', 'ecotheme'), 'fa fa-male' => __( ' male', 'ecotheme'), 'fa fa-map' => __( ' map', 'ecotheme'), 'fa fa-map-marker' => __( ' map-marker', 'ecotheme'), 'fa fa-map-o' => __( ' map-o', 'ecotheme'), 'fa fa-map-pin' => __( ' map-pin', 'ecotheme'), 'fa fa-map-signs' => __( ' map-signs', 'ecotheme'), 'fa fa-meh-o' => __( ' meh-o', 'ecotheme'), 'fa fa-microphone' => __( ' microphone', 'ecotheme'), 'fa fa-microphone-slash' => __( ' microphone-slash', 'ecotheme'), 'fa fa-minus' => __( ' minus', 'ecotheme'), 'fa fa-minus-circle' => __( ' minus-circle', 'ecotheme'), 'fa fa-minus-square' => __( ' minus-square', 'ecotheme'), 'fa fa-minus-square-o' => __( ' minus-square-o', 'ecotheme'), 'fa fa-mobile' => __( ' mobile', 'ecotheme'), 'fa fa-mobile-phone' => __( ' mobile-phone', 'ecotheme'), 'fa fa-money' => __( ' money', 'ecotheme'), 'fa fa-moon-o' => __( ' moon-o', 'ecotheme'), 'fa fa-mortar-board' => __( ' mortar-board', 'ecotheme'), 'fa fa-motorcycle' => __( ' motorcycle', 'ecotheme'), 'fa fa-mouse-pointer' => __( ' mouse-pointer', 'ecotheme'), 'fa fa-music' => __( ' music', 'ecotheme'), 'fa fa-navicon' => __( ' navicon', 'ecotheme'), 'fa fa-newspaper-o' => __( ' newspaper-o', 'ecotheme'), 'fa fa-object-group' => __( ' object-group', 'ecotheme'), 'fa fa-object-ungroup' => __( ' object-ungroup', 'ecotheme'), 'fa fa-paint-brush' => __( ' paint-brush', 'ecotheme'), 'fa fa-paper-plane' => __( ' paper-plane', 'ecotheme'), 'fa fa-paper-plane-o' => __( ' paper-plane-o', 'ecotheme'), 'fa fa-paw' => __( ' paw', 'ecotheme'), 'fa fa-pencil' => __( ' pencil', 'ecotheme'), 'fa fa-pencil-square' => __( ' pencil-square', 'ecotheme'), 'fa fa-pencil-square-o' => __( ' pencil-square-o', 'ecotheme'), 'fa fa-phone' => __( ' phone', 'ecotheme'), 'fa fa-phone-square' => __( ' phone-square', 'ecotheme'), 'fa fa-photo' => __( ' photo', 'ecotheme'), 'fa fa-picture-o' => __( ' picture-o', 'ecotheme'), 'fa fa-pie-chart' => __( ' pie-chart', 'ecotheme'), 'fa fa-plane' => __( ' plane', 'ecotheme'), 'fa fa-plug' => __( ' plug', 'ecotheme'), 'fa fa-plus' => __( ' plus', 'ecotheme'), 'fa fa-plus-circle' => __( ' plus-circle', 'ecotheme'), 'fa fa-plus-square' => __( ' plus-square', 'ecotheme'), 'fa fa-plus-square-o' => __( ' plus-square-o', 'ecotheme'), 'fa fa-power-off' => __( ' power-off', 'ecotheme'), 'fa fa-print' => __( ' print', 'ecotheme'), 'fa fa-puzzle-piece' => __( ' puzzle-piece', 'ecotheme'), 'fa fa-qrcode' => __( ' qrcode', 'ecotheme'), 'fa fa-question' => __( ' question', 'ecotheme'), 'fa fa-question-circle' => __( ' question-circle', 'ecotheme'), 'fa fa-quote-left' => __( ' quote-left', 'ecotheme'), 'fa fa-quote-right' => __( ' quote-right', 'ecotheme'), 'fa fa-random' => __( ' random', 'ecotheme'), 'fa fa-recycle' => __( ' recycle', 'ecotheme'), 'fa fa-refresh' => __( ' refresh', 'ecotheme'), 'fa fa-registered' => __( ' registered', 'ecotheme'), 'fa fa-remove' => __( ' remove', 'ecotheme'), 'fa fa-reorder' => __( ' reorder', 'ecotheme'), 'fa fa-reply' => __( ' reply', 'ecotheme'), 'fa fa-reply-all' => __( ' reply-all', 'ecotheme'), 'fa fa-retweet' => __( ' retweet', 'ecotheme'), 'fa fa-road' => __( ' road', 'ecotheme'), 'fa fa-rocket' => __( ' rocket', 'ecotheme'), 'fa fa-rss' => __( ' rss', 'ecotheme'), 'fa fa-rss-square' => __( ' rss-square', 'ecotheme'), 'fa fa-search' => __( ' search', 'ecotheme'), 'fa fa-search-minus' => __( ' search-minus', 'ecotheme'), 'fa fa-search-plus' => __( ' search-plus', 'ecotheme'), 'fa fa-send' => __( ' send', 'ecotheme'), 'fa fa-send-o' => __( ' send-o', 'ecotheme'), 'fa fa-server' => __( ' server', 'ecotheme'), 'fa fa-share' => __( ' share', 'ecotheme'), 'fa fa-share-alt' => __( ' share-alt', 'ecotheme'), 'fa fa-share-alt-square' => __( ' share-alt-square', 'ecotheme'), 'fa fa-share-square' => __( ' share-square', 'ecotheme'), 'fa fa-share-square-o' => __( ' share-square-o', 'ecotheme'), 'fa fa-shield' => __( ' shield', 'ecotheme'), 'fa fa-ship' => __( ' ship', 'ecotheme'), 'fa fa-shopping-cart' => __( ' shopping-cart', 'ecotheme'), 'fa fa-sign-in' => __( ' sign-in', 'ecotheme'), 'fa fa-sign-out' => __( ' sign-out', 'ecotheme'), 'fa fa-signal' => __( ' signal', 'ecotheme'), 'fa fa-sitemap' => __( ' sitemap', 'ecotheme'), 'fa fa-sliders' => __( ' sliders', 'ecotheme'), 'fa fa-smile-o' => __( ' smile-o', 'ecotheme'), 'fa fa-soccer-ball-o' => __( ' soccer-ball-o', 'ecotheme'), 'fa fa-sort' => __( ' sort', 'ecotheme'), 'fa fa-sort-alpha-asc' => __( ' sort-alpha-asc', 'ecotheme'), 'fa fa-sort-alpha-desc' => __( ' sort-alpha-desc', 'ecotheme'), 'fa fa-sort-amount-asc' => __( ' sort-amount-asc', 'ecotheme'), 'fa fa-sort-amount-desc' => __( ' sort-amount-desc', 'ecotheme'), 'fa fa-sort-asc' => __( ' sort-asc', 'ecotheme'), 'fa fa-sort-desc' => __( ' sort-desc', 'ecotheme'), 'fa fa-sort-down' => __( ' sort-down', 'ecotheme'), 'fa fa-sort-numeric-asc' => __( ' sort-numeric-asc', 'ecotheme'), 'fa fa-sort-numeric-desc' => __( ' sort-numeric-desc', 'ecotheme'), 'fa fa-sort-up' => __( ' sort-up', 'ecotheme'), 'fa fa-space-shuttle' => __( ' space-shuttle', 'ecotheme'), 'fa fa-spinner' => __( ' spinner', 'ecotheme'), 'fa fa-spoon' => __( ' spoon', 'ecotheme'), 'fa fa-square' => __( ' square', 'ecotheme'), 'fa fa-square-o' => __( ' square-o', 'ecotheme'), 'fa fa-star' => __( ' star', 'ecotheme'), 'fa fa-star-half' => __( ' star-half', 'ecotheme'), 'fa fa-star-half-empty' => __( ' star-half-empty', 'ecotheme'), 'fa fa-star-half-full' => __( ' star-half-full', 'ecotheme'), 'fa fa-star-half-o' => __( ' star-half-o', 'ecotheme'), 'fa fa-star-o' => __( ' star-o', 'ecotheme'), 'fa fa-sticky-note' => __( ' sticky-note', 'ecotheme'), 'fa fa-sticky-note-o' => __( ' sticky-note-o', 'ecotheme'), 'fa fa-street-view' => __( ' street-view', 'ecotheme'), 'fa fa-suitcase' => __( ' suitcase', 'ecotheme'), 'fa fa-sun-o' => __( ' sun-o', 'ecotheme'), 'fa fa-support' => __( ' support', 'ecotheme'), 'fa fa-tablet' => __( ' tablet', 'ecotheme'), 'fa fa-tachometer' => __( ' tachometer', 'ecotheme'), 'fa fa-tag' => __( ' tag', 'ecotheme'), 'fa fa-tags' => __( ' tags', 'ecotheme'), 'fa fa-tasks' => __( ' tasks', 'ecotheme'), 'fa fa-taxi' => __( ' taxi', 'ecotheme'), 'fa fa-television' => __( ' television', 'ecotheme'), 'fa fa-terminal' => __( ' terminal', 'ecotheme'), 'fa fa-thumb-tack' => __( ' thumb-tack', 'ecotheme'), 'fa fa-thumbs-down' => __( ' thumbs-down', 'ecotheme'), 'fa fa-thumbs-o-down' => __( ' thumbs-o-down', 'ecotheme'), 'fa fa-thumbs-o-up' => __( ' thumbs-o-up', 'ecotheme'), 'fa fa-thumbs-up' => __( ' thumbs-up', 'ecotheme'), 'fa fa-ticket' => __( ' ticket', 'ecotheme'), 'fa fa-times' => __( ' times', 'ecotheme'), 'fa fa-times-circle' => __( ' times-circle', 'ecotheme'), 'fa fa-times-circle-o' => __( ' times-circle-o', 'ecotheme'), 'fa fa-tint' => __( ' tint', 'ecotheme'), 'fa fa-toggle-down' => __( ' toggle-down', 'ecotheme'), 'fa fa-toggle-left' => __( ' toggle-left', 'ecotheme'), 'fa fa-toggle-off' => __( ' toggle-off', 'ecotheme'), 'fa fa-toggle-on' => __( ' toggle-on', 'ecotheme'), 'fa fa-toggle-right' => __( ' toggle-right', 'ecotheme'), 'fa fa-toggle-up' => __( ' toggle-up', 'ecotheme'), 'fa fa-trademark' => __( ' trademark', 'ecotheme'), 'fa fa-trash' => __( ' trash', 'ecotheme'), 'fa fa-trash-o' => __( ' trash-o', 'ecotheme'), 'fa fa-tree' => __( ' tree', 'ecotheme'), 'fa fa-trophy' => __( ' trophy', 'ecotheme'), 'fa fa-truck' => __( ' truck', 'ecotheme'), 'fa fa-tty' => __( ' tty', 'ecotheme'), 'fa fa-tv' => __( ' tv', 'ecotheme'), 'fa fa-umbrella' => __( ' umbrella', 'ecotheme'), 'fa fa-university' => __( ' university', 'ecotheme'), 'fa fa-unlock' => __( ' unlock', 'ecotheme'), 'fa fa-unlock-alt' => __( ' unlock-alt', 'ecotheme'), 'fa fa-unsorted' => __( ' unsorted', 'ecotheme'), 'fa fa-upload' => __( ' upload', 'ecotheme'), 'fa fa-user' => __( ' user', 'ecotheme'), 'fa fa-user-plus' => __( ' user-plus', 'ecotheme'), 'fa fa-user-secret' => __( ' user-secret', 'ecotheme'), 'fa fa-user-times' => __( ' user-times', 'ecotheme'), 'fa fa-users' => __( ' users', 'ecotheme'), 'fa fa-video-camera' => __( ' video-camera', 'ecotheme'), 'fa fa-volume-down' => __( ' volume-down', 'ecotheme'), 'fa fa-volume-off' => __( ' volume-off', 'ecotheme'), 'fa fa-volume-up' => __( ' volume-up', 'ecotheme'), 'fa fa-warning' => __( ' warning', 'ecotheme'), 'fa fa-wheelchair' => __( ' wheelchair', 'ecotheme'), 'fa fa-wifi' => __( ' wifi', 'ecotheme'), 'fa fa-wrench' => __( ' wrench', 'ecotheme'), 'fa fa-hand-grab-o' => __( ' hand-grab-o', 'ecotheme'), 'fa fa-hand-lizard-o' => __( ' hand-lizard-o', 'ecotheme'), 'fa fa-hand-o-down' => __( ' hand-o-down', 'ecotheme'), 'fa fa-hand-o-left' => __( ' hand-o-left', 'ecotheme'), 'fa fa-hand-o-right' => __( ' hand-o-right', 'ecotheme'), 'fa fa-hand-o-up' => __( ' hand-o-up', 'ecotheme'), 'fa fa-hand-paper-o' => __( ' hand-paper-o', 'ecotheme'), 'fa fa-hand-peace-o' => __( ' hand-peace-o', 'ecotheme'), 'fa fa-hand-pointer-o' => __( ' hand-pointer-o', 'ecotheme'), 'fa fa-hand-rock-o' => __( ' hand-rock-o', 'ecotheme'), 'fa fa-hand-scissors-o' => __( ' hand-scissors-o', 'ecotheme'), 'fa fa-hand-spock-o' => __( ' hand-spock-o', 'ecotheme'), 'fa fa-hand-stop-o' => __( ' hand-stop-o', 'ecotheme'), 'fa fa-thumbs-down' => __( ' thumbs-down', 'ecotheme'), 'fa fa-thumbs-o-down' => __( ' thumbs-o-down', 'ecotheme'), 'fa fa-thumbs-o-up' => __( ' thumbs-o-up', 'ecotheme'), 'fa fa-thumbs-up' => __( ' thumbs-up', 'ecotheme'), 'fa fa-ambulance' => __( ' ambulance', 'ecotheme'), 'fa fa-automobile' => __( ' automobile', 'ecotheme'), 'fa fa-bicycle' => __( ' bicycle', 'ecotheme'), 'fa fa-bus' => __( ' bus', 'ecotheme'), 'fa fa-cab' => __( ' cab', 'ecotheme'), 'fa fa-car' => __( ' car', 'ecotheme'), 'fa fa-fighter-jet' => __( ' fighter-jet', 'ecotheme'), 'fa fa-motorcycle' => __( ' motorcycle', 'ecotheme'), 'fa fa-plane' => __( ' plane', 'ecotheme'), 'fa fa-rocket' => __( ' rocket', 'ecotheme'), 'fa fa-ship' => __( ' ship', 'ecotheme'), 'fa fa-space-shuttle' => __( ' space-shuttle', 'ecotheme'), 'fa fa-subway' => __( ' subway', 'ecotheme'), 'fa fa-taxi' => __( ' taxi', 'ecotheme'), 'fa fa-train' => __( ' train', 'ecotheme'), 'fa fa-truck' => __( ' truck', 'ecotheme'), 'fa fa-wheelchair' => __( ' wheelchair', 'ecotheme'), 'fa fa-genderless' => __( ' genderless', 'ecotheme'), 'fa fa-intersex' => __( ' intersex', 'ecotheme'), 'fa fa-mars' => __( ' mars', 'ecotheme'), 'fa fa-mars-double' => __( ' mars-double', 'ecotheme'), 'fa fa-mars-stroke' => __( ' mars-stroke', 'ecotheme'), 'fa fa-mars-stroke-h' => __( ' mars-stroke-h', 'ecotheme'), 'fa fa-mars-stroke-v' => __( ' mars-stroke-v', 'ecotheme'), 'fa fa-mercury' => __( ' mercury', 'ecotheme'), 'fa fa-neuter' => __( ' neuter', 'ecotheme'), 'fa fa-transgender' => __( ' transgender', 'ecotheme'), 'fa fa-transgender-alt' => __( ' transgender-alt', 'ecotheme'), 'fa fa-venus' => __( ' venus', 'ecotheme'), 'fa fa-venus-double' => __( ' venus-double', 'ecotheme'), 'fa fa-venus-mars' => __( ' venus-mars', 'ecotheme'), 'fa fa-file' => __( ' file', 'ecotheme'), 'fa fa-file-archive-o' => __( ' file-archive-o', 'ecotheme'), 'fa fa-file-audio-o' => __( ' file-audio-o', 'ecotheme'), 'fa fa-file-code-o' => __( ' file-code-o', 'ecotheme'), 'fa fa-file-excel-o' => __( ' file-excel-o', 'ecotheme'), 'fa fa-file-image-o' => __( ' file-image-o', 'ecotheme'), 'fa fa-file-movie-o' => __( ' file-movie-o', 'ecotheme'), 'fa fa-file-o' => __( ' file-o', 'ecotheme'), 'fa fa-file-pdf-o' => __( ' file-pdf-o', 'ecotheme'), 'fa fa-file-photo-o' => __( ' file-photo-o', 'ecotheme'), 'fa fa-file-picture-o' => __( ' file-picture-o', 'ecotheme'), 'fa fa-file-powerpoint-o' => __( ' file-powerpoint-o', 'ecotheme'), 'fa fa-file-sound-o' => __( ' file-sound-o', 'ecotheme'), 'fa fa-file-text' => __( ' file-text', 'ecotheme'), 'fa fa-file-text-o' => __( ' file-text-o', 'ecotheme'), 'fa fa-file-video-o' => __( ' file-video-o', 'ecotheme'), 'fa fa-file-word-o' => __( ' file-word-o', 'ecotheme'), 'fa fa-file-zip-o' => __( ' file-zip-o', 'ecotheme'), 'fa fa-circle-o-notch' => __( ' circle-o-notch', 'ecotheme'), 'fa fa-cog' => __( ' cog', 'ecotheme'), 'fa fa-gear' => __( ' gear', 'ecotheme'), 'fa fa-refresh' => __( ' refresh', 'ecotheme'), 'fa fa-spinner' => __( ' spinner', 'ecotheme'), 'fa fa-check-square' => __( ' check-square', 'ecotheme'), 'fa fa-check-square-o' => __( ' check-square-o', 'ecotheme'), 'fa fa-circle' => __( ' circle', 'ecotheme'), 'fa fa-circle-o' => __( ' circle-o', 'ecotheme'), 'fa fa-dot-circle-o' => __( ' dot-circle-o', 'ecotheme'), 'fa fa-minus-square' => __( ' minus-square', 'ecotheme'), 'fa fa-minus-square-o' => __( ' minus-square-o', 'ecotheme'), 'fa fa-plus-square' => __( ' plus-square', 'ecotheme'), 'fa fa-plus-square-o' => __( ' plus-square-o', 'ecotheme'), 'fa fa-square' => __( ' square', 'ecotheme'), 'fa fa-square-o' => __( ' square-o', 'ecotheme'), 'fa fa-cc-amex' => __( ' cc-amex', 'ecotheme'), 'fa fa-cc-diners-club' => __( ' cc-diners-club', 'ecotheme'), 'fa fa-cc-discover' => __( ' cc-discover', 'ecotheme'), 'fa fa-cc-jcb' => __( ' cc-jcb', 'ecotheme'), 'fa fa-cc-mastercard' => __( ' cc-mastercard', 'ecotheme'), 'fa fa-cc-paypal' => __( ' cc-paypal', 'ecotheme'), 'fa fa-cc-stripe' => __( ' cc-stripe', 'ecotheme'), 'fa fa-cc-visa' => __( ' cc-visa', 'ecotheme'), 'fa fa-credit-card' => __( ' credit-card', 'ecotheme'), 'fa fa-google-wallet' => __( ' google-wallet', 'ecotheme'), 'fa fa-paypal' => __( ' paypal', 'ecotheme'), 'fa fa-area-chart' => __( ' area-chart', 'ecotheme'), 'fa fa-bar-chart' => __( ' bar-chart', 'ecotheme'), 'fa fa-bar-chart-o' => __( ' bar-chart-o', 'ecotheme'), 'fa fa-line-chart' => __( ' line-chart', 'ecotheme'), 'fa fa-pie-chart' => __( ' pie-chart', 'ecotheme'), 'fa fa-bitcoin' => __( ' bitcoin', 'ecotheme'), 'fa fa-btc' => __( ' btc', 'ecotheme'), 'fa fa-cny' => __( ' cny', 'ecotheme'), 'fa fa-dollar' => __( ' dollar', 'ecotheme'), 'fa fa-eur' => __( ' eur', 'ecotheme'), 'fa fa-euro' => __( ' euro', 'ecotheme'), 'fa fa-gbp' => __( ' gbp', 'ecotheme'), 'fa fa-gg' => __( ' gg', 'ecotheme'), 'fa fa-gg-circle' => __( ' gg-circle', 'ecotheme'), 'fa fa-ils' => __( ' ils', 'ecotheme'), 'fa fa-inr' => __( ' inr', 'ecotheme'), 'fa fa-jpy' => __( ' jpy', 'ecotheme'), 'fa fa-krw' => __( ' krw', 'ecotheme'), 'fa fa-money' => __( ' money', 'ecotheme'), 'fa fa-rmb' => __( ' rmb', 'ecotheme'), 'fa fa-rouble' => __( ' rouble', 'ecotheme'), 'fa fa-rub' => __( ' rub', 'ecotheme'), 'fa fa-ruble' => __( ' ruble', 'ecotheme'), 'fa fa-rupee' => __( ' rupee', 'ecotheme'), 'fa fa-shekel' => __( ' shekel', 'ecotheme'), 'fa fa-sheqel' => __( ' sheqel', 'ecotheme'), 'fa fa-try' => __( ' try', 'ecotheme'), 'fa fa-turkish-lira' => __( ' turkish-lira', 'ecotheme'), 'fa fa-usd' => __( ' usd', 'ecotheme'), 'fa fa-won' => __( ' won', 'ecotheme'), 'fa fa-yen' => __( ' yen', 'ecotheme'), 'fa fa-align-center' => __( ' align-center', 'ecotheme'), 'fa fa-align-justify' => __( ' align-justify', 'ecotheme'), 'fa fa-align-left' => __( ' align-left', 'ecotheme'), 'fa fa-align-right' => __( ' align-right', 'ecotheme'), 'fa fa-bold' => __( ' bold', 'ecotheme'), 'fa fa-chain' => __( ' chain', 'ecotheme'), 'fa fa-chain-broken' => __( ' chain-broken', 'ecotheme'), 'fa fa-clipboard' => __( ' clipboard', 'ecotheme'), 'fa fa-columns' => __( ' columns', 'ecotheme'), 'fa fa-copy' => __( ' copy', 'ecotheme'), 'fa fa-cut' => __( ' cut', 'ecotheme'), 'fa fa-dedent' => __( ' dedent', 'ecotheme'), 'fa fa-eraser' => __( ' eraser', 'ecotheme'), 'fa fa-file' => __( ' file', 'ecotheme'), 'fa fa-file-o' => __( ' file-o', 'ecotheme'), 'fa fa-file-text' => __( ' file-text', 'ecotheme'), 'fa fa-file-text-o' => __( ' file-text-o', 'ecotheme'), 'fa fa-files-o' => __( ' files-o', 'ecotheme'), 'fa fa-floppy-o' => __( ' floppy-o', 'ecotheme'), 'fa fa-font' => __( ' font', 'ecotheme'), 'fa fa-header' => __( ' header', 'ecotheme'), 'fa fa-indent' => __( ' indent', 'ecotheme'), 'fa fa-italic' => __( ' italic', 'ecotheme'), 'fa fa-link' => __( ' link', 'ecotheme'), 'fa fa-list' => __( ' list', 'ecotheme'), 'fa fa-list-alt' => __( ' list-alt', 'ecotheme'), 'fa fa-list-ol' => __( ' list-ol', 'ecotheme'), 'fa fa-list-ul' => __( ' list-ul', 'ecotheme'), 'fa fa-outdent' => __( ' outdent', 'ecotheme'), 'fa fa-paperclip' => __( ' paperclip', 'ecotheme'), 'fa fa-paragraph' => __( ' paragraph', 'ecotheme'), 'fa fa-paste' => __( ' paste', 'ecotheme'), 'fa fa-repeat' => __( ' repeat', 'ecotheme'), 'fa fa-rotate-left' => __( ' rotate-left', 'ecotheme'), 'fa fa-rotate-right' => __( ' rotate-right', 'ecotheme'), 'fa fa-save' => __( ' save', 'ecotheme'), 'fa fa-scissors' => __( ' scissors', 'ecotheme'), 'fa fa-strikethrough' => __( ' strikethrough', 'ecotheme'), 'fa fa-subscript' => __( ' subscript', 'ecotheme'), 'fa fa-superscript' => __( ' superscript', 'ecotheme'), 'fa fa-table' => __( ' table', 'ecotheme'), 'fa fa-text-height' => __( ' text-height', 'ecotheme'), 'fa fa-text-width' => __( ' text-width', 'ecotheme'), 'fa fa-th' => __( ' th', 'ecotheme'), 'fa fa-th-large' => __( ' th-large', 'ecotheme'), 'fa fa-th-list' => __( ' th-list', 'ecotheme'), 'fa fa-underline' => __( ' underline', 'ecotheme'), 'fa fa-undo' => __( ' undo', 'ecotheme'), 'fa fa-unlink' => __( ' unlink', 'ecotheme'), 'fa fa-angle-double-down' => __( ' angle-double-down', 'ecotheme'), 'fa fa-angle-double-left' => __( ' angle-double-left', 'ecotheme'), 'fa fa-angle-double-right' => __( ' angle-double-right', 'ecotheme'), 'fa fa-angle-double-up' => __( ' angle-double-up', 'ecotheme'), 'fa fa-angle-down' => __( ' angle-down', 'ecotheme'), 'fa fa-angle-left' => __( ' angle-left', 'ecotheme'), 'fa fa-angle-right' => __( ' angle-right', 'ecotheme'), 'fa fa-angle-up' => __( ' angle-up', 'ecotheme'), 'fa fa-arrow-circle-down' => __( ' arrow-circle-down', 'ecotheme'), 'fa fa-arrow-circle-left' => __( ' arrow-circle-left', 'ecotheme'), 'fa fa-arrow-circle-o-down' => __( ' arrow-circle-o-down', 'ecotheme'), 'fa fa-arrow-circle-o-left' => __( ' arrow-circle-o-left', 'ecotheme'), 'fa fa-arrow-circle-o-right' => __( ' arrow-circle-o-right', 'ecotheme'), 'fa fa-arrow-circle-o-up' => __( ' arrow-circle-o-up', 'ecotheme'), 'fa fa-arrow-circle-right' => __( ' arrow-circle-right', 'ecotheme'), 'fa fa-arrow-circle-up' => __( ' arrow-circle-up', 'ecotheme'), 'fa fa-arrow-down' => __( ' arrow-down', 'ecotheme'), 'fa fa-arrow-left' => __( ' arrow-left', 'ecotheme'), 'fa fa-arrow-right' => __( ' arrow-right', 'ecotheme'), 'fa fa-arrow-up' => __( ' arrow-up', 'ecotheme'), 'fa fa-arrows' => __( ' arrows', 'ecotheme'), 'fa fa-arrows-alt' => __( ' arrows-alt', 'ecotheme'), 'fa fa-arrows-h' => __( ' arrows-h', 'ecotheme'), 'fa fa-arrows-v' => __( ' arrows-v', 'ecotheme'), 'fa fa-caret-down' => __( ' caret-down', 'ecotheme'), 'fa fa-caret-left' => __( ' caret-left', 'ecotheme'), 'fa fa-caret-right' => __( ' caret-right', 'ecotheme'), 'fa fa-caret-square-o-down' => __( ' caret-square-o-down', 'ecotheme'), 'fa fa-caret-square-o-left' => __( ' caret-square-o-left', 'ecotheme'), 'fa fa-caret-square-o-right' => __( ' caret-square-o-right', 'ecotheme'), 'fa fa-caret-square-o-up' => __( ' caret-square-o-up', 'ecotheme'), 'fa fa-caret-up' => __( ' caret-up', 'ecotheme'), 'fa fa-chevron-circle-down' => __( ' chevron-circle-down', 'ecotheme'), 'fa fa-chevron-circle-left' => __( ' chevron-circle-left', 'ecotheme'), 'fa fa-chevron-circle-right' => __( ' chevron-circle-right', 'ecotheme'), 'fa fa-chevron-circle-up' => __( ' chevron-circle-up', 'ecotheme'), 'fa fa-chevron-down' => __( ' chevron-down', 'ecotheme'), 'fa fa-chevron-left' => __( ' chevron-left', 'ecotheme'), 'fa fa-chevron-right' => __( ' chevron-right', 'ecotheme'), 'fa fa-chevron-up' => __( ' chevron-up', 'ecotheme'), 'fa fa-exchange' => __( ' exchange', 'ecotheme'), 'fa fa-hand-o-down' => __( ' hand-o-down', 'ecotheme'), 'fa fa-hand-o-left' => __( ' hand-o-left', 'ecotheme'), 'fa fa-hand-o-right' => __( ' hand-o-right', 'ecotheme'), 'fa fa-hand-o-up' => __( ' hand-o-up', 'ecotheme'), 'fa fa-long-arrow-down' => __( ' long-arrow-down', 'ecotheme'), 'fa fa-long-arrow-left' => __( ' long-arrow-left', 'ecotheme'), 'fa fa-long-arrow-right' => __( ' long-arrow-right', 'ecotheme'), 'fa fa-long-arrow-up' => __( ' long-arrow-up', 'ecotheme'), 'fa fa-toggle-down' => __( ' toggle-down', 'ecotheme'), 'fa fa-toggle-left' => __( ' toggle-left', 'ecotheme'), 'fa fa-toggle-right' => __( ' toggle-right', 'ecotheme'), 'fa fa-toggle-up' => __( ' toggle-up', 'ecotheme'), 'fa fa-arrows-alt' => __( ' arrows-alt', 'ecotheme'), 'fa fa-backward' => __( ' backward', 'ecotheme'), 'fa fa-compress' => __( ' compress', 'ecotheme'), 'fa fa-eject' => __( ' eject', 'ecotheme'), 'fa fa-expand' => __( ' expand', 'ecotheme'), 'fa fa-fast-backward' => __( ' fast-backward', 'ecotheme'), 'fa fa-fast-forward' => __( ' fast-forward', 'ecotheme'), 'fa fa-forward' => __( ' forward', 'ecotheme'), 'fa fa-pause' => __( ' pause', 'ecotheme'), 'fa fa-play' => __( ' play', 'ecotheme'), 'fa fa-play-circle' => __( ' play-circle', 'ecotheme'), 'fa fa-play-circle-o' => __( ' play-circle-o', 'ecotheme'), 'fa fa-random' => __( ' random', 'ecotheme'), 'fa fa-step-backward' => __( ' step-backward', 'ecotheme'), 'fa fa-step-forward' => __( ' step-forward', 'ecotheme'), 'fa fa-stop' => __( ' stop', 'ecotheme'), 'fa fa-youtube-play' => __( ' youtube-play', 'ecotheme'), 'fa fa-500px' => __( ' 500px', 'ecotheme'), 'fa fa-adn' => __( ' adn', 'ecotheme'), 'fa fa-amazon' => __( ' amazon', 'ecotheme'), 'fa fa-android' => __( ' android', 'ecotheme'), 'fa fa-angellist' => __( ' angellist', 'ecotheme'), 'fa fa-apple' => __( ' apple', 'ecotheme'), 'fa fa-behance' => __( ' behance', 'ecotheme'), 'fa fa-behance-square' => __( ' behance-square', 'ecotheme'), 'fa fa-bitbucket' => __( ' bitbucket', 'ecotheme'), 'fa fa-bitbucket-square' => __( ' bitbucket-square', 'ecotheme'), 'fa fa-bitcoin' => __( ' bitcoin', 'ecotheme'), 'fa fa-black-tie' => __( ' black-tie', 'ecotheme'), 'fa fa-btc' => __( ' btc', 'ecotheme'), 'fa fa-buysellads' => __( ' buysellads', 'ecotheme'), 'fa fa-cc-amex' => __( ' cc-amex', 'ecotheme'), 'fa fa-cc-diners-club' => __( ' cc-diners-club', 'ecotheme'), 'fa fa-cc-discover' => __( ' cc-discover', 'ecotheme'), 'fa fa-cc-jcb' => __( ' cc-jcb', 'ecotheme'), 'fa fa-cc-mastercard' => __( ' cc-mastercard', 'ecotheme'), 'fa fa-cc-paypal' => __( ' cc-paypal', 'ecotheme'), 'fa fa-cc-stripe' => __( ' cc-stripe', 'ecotheme'), 'fa fa-cc-visa' => __( ' cc-visa', 'ecotheme'), 'fa fa-chrome' => __( ' chrome', 'ecotheme'), 'fa fa-codepen' => __( ' codepen', 'ecotheme'), 'fa fa-connectdevelop' => __( ' connectdevelop', 'ecotheme'), 'fa fa-contao' => __( ' contao', 'ecotheme'), 'fa fa-css3' => __( ' css3', 'ecotheme'), 'fa fa-dashcube' => __( ' dashcube', 'ecotheme'), 'fa fa-delicious' => __( ' delicious', 'ecotheme'), 'fa fa-deviantart' => __( ' deviantart', 'ecotheme'), 'fa fa-digg' => __( ' digg', 'ecotheme'), 'fa fa-dribbble' => __( ' dribbble', 'ecotheme'), 'fa fa-dropbox' => __( ' dropbox', 'ecotheme'), 'fa fa-drupal' => __( ' drupal', 'ecotheme'), 'fa fa-empire' => __( ' empire', 'ecotheme'), 'fa fa-expeditedssl' => __( ' expeditedssl', 'ecotheme'), 'fa fa-facebook' => __( ' facebook', 'ecotheme'), 'fa fa-facebook-f' => __( ' facebook-f', 'ecotheme'), 'fa fa-facebook-official' => __( ' facebook-official', 'ecotheme'), 'fa fa-facebook-square' => __( ' facebook-square', 'ecotheme'), 'fa fa-firefox' => __( ' firefox', 'ecotheme'), 'fa fa-flickr' => __( ' flickr', 'ecotheme'), 'fa fa-fonticons' => __( ' fonticons', 'ecotheme'), 'fa fa-forumbee' => __( ' forumbee', 'ecotheme'), 'fa fa-foursquare' => __( ' foursquare', 'ecotheme'), 'fa fa-ge' => __( ' ge', 'ecotheme'), 'fa fa-get-pocket' => __( ' get-pocket', 'ecotheme'), 'fa fa-gg' => __( ' gg', 'ecotheme'), 'fa fa-gg-circle' => __( ' gg-circle', 'ecotheme'), 'fa fa-git' => __( ' git', 'ecotheme'), 'fa fa-git-square' => __( ' git-square', 'ecotheme'), 'fa fa-github' => __( ' github', 'ecotheme'), 'fa fa-github-alt' => __( ' github-alt', 'ecotheme'), 'fa fa-github-square' => __( ' github-square', 'ecotheme'), 'fa fa-gittip' => __( ' gittip', 'ecotheme'), 'fa fa-google' => __( ' google', 'ecotheme'), 'fa fa-google-plus' => __( ' google-plus', 'ecotheme'), 'fa fa-google-plus-square' => __( ' google-plus-square', 'ecotheme'), 'fa fa-google-wallet' => __( ' google-wallet', 'ecotheme'), 'fa fa-gratipay' => __( ' gratipay', 'ecotheme'), 'fa fa-hacker-news' => __( ' hacker-news', 'ecotheme'), 'fa fa-houzz' => __( ' houzz', 'ecotheme'), 'fa fa-html5' => __( ' html5', 'ecotheme'), 'fa fa-instagram' => __( ' instagram', 'ecotheme'), 'fa fa-internet-explorer' => __( ' internet-explorer', 'ecotheme'), 'fa fa-ioxhost' => __( ' ioxhost', 'ecotheme'), 'fa fa-joomla' => __( ' joomla', 'ecotheme'), 'fa fa-jsfiddle' => __( ' jsfiddle', 'ecotheme'), 'fa fa-lastfm' => __( ' lastfm', 'ecotheme'), 'fa fa-lastfm-square' => __( ' lastfm-square', 'ecotheme'), 'fa fa-leanpub' => __( ' leanpub', 'ecotheme'), 'fa fa-linkedin' => __( ' linkedin', 'ecotheme'), 'fa fa-linkedin-square' => __( ' linkedin-square', 'ecotheme'), 'fa fa-linux' => __( ' linux', 'ecotheme'), 'fa fa-maxcdn' => __( ' maxcdn', 'ecotheme'), 'fa fa-meanpath' => __( ' meanpath', 'ecotheme'), 'fa fa-medium' => __( ' medium', 'ecotheme'), 'fa fa-odnoklassniki' => __( ' odnoklassniki', 'ecotheme'), 'fa fa-odnoklassniki-square' => __( ' odnoklassniki-square', 'ecotheme'), 'fa fa-opencart' => __( ' opencart', 'ecotheme'), 'fa fa-openid' => __( ' openid', 'ecotheme'), 'fa fa-opera' => __( ' opera', 'ecotheme'), 'fa fa-optin-monster' => __( ' optin-monster', 'ecotheme'), 'fa fa-pagelines' => __( ' pagelines', 'ecotheme'), 'fa fa-paypal' => __( ' paypal', 'ecotheme'), 'fa fa-pied-piper' => __( ' pied-piper', 'ecotheme'), 'fa fa-pied-piper-alt' => __( ' pied-piper-alt', 'ecotheme'), 'fa fa-pinterest' => __( ' pinterest', 'ecotheme'), 'fa fa-pinterest-p' => __( ' pinterest-p', 'ecotheme'), 'fa fa-pinterest-square' => __( ' pinterest-square', 'ecotheme'), 'fa fa-qq' => __( ' qq', 'ecotheme'), 'fa fa-ra' => __( ' ra', 'ecotheme'), 'fa fa-rebel' => __( ' rebel', 'ecotheme'), 'fa fa-reddit' => __( ' reddit', 'ecotheme'), 'fa fa-reddit-square' => __( ' reddit-square', 'ecotheme'), 'fa fa-renren' => __( ' renren', 'ecotheme'), 'fa fa-safari' => __( ' safari', 'ecotheme'), 'fa fa-sellsy' => __( ' sellsy', 'ecotheme'), 'fa fa-share-alt' => __( ' share-alt', 'ecotheme'), 'fa fa-share-alt-square' => __( ' share-alt-square', 'ecotheme'), 'fa fa-shirtsinbulk' => __( ' shirtsinbulk', 'ecotheme'), 'fa fa-simplybuilt' => __( ' simplybuilt', 'ecotheme'), 'fa fa-skyatlas' => __( ' skyatlas', 'ecotheme'), 'fa fa-skype' => __( ' skype', 'ecotheme'), 'fa fa-slack' => __( ' slack', 'ecotheme'), 'fa fa-slideshare' => __( ' slideshare', 'ecotheme'), 'fa fa-soundcloud' => __( ' soundcloud', 'ecotheme'), 'fa fa-spotify' => __( ' spotify', 'ecotheme'), 'fa fa-stack-exchange' => __( ' stack-exchange', 'ecotheme'), 'fa fa-stack-overflow' => __( ' stack-overflow', 'ecotheme'), 'fa fa-steam' => __( ' steam', 'ecotheme'), 'fa fa-steam-square' => __( ' steam-square', 'ecotheme'), 'fa fa-stumbleupon' => __( ' stumbleupon', 'ecotheme'), 'fa fa-stumbleupon-circle' => __( ' stumbleupon-circle', 'ecotheme'), 'fa fa-tencent-weibo' => __( ' tencent-weibo', 'ecotheme'), 'fa fa-trello' => __( ' trello', 'ecotheme'), 'fa fa-tripadvisor' => __( ' tripadvisor', 'ecotheme'), 'fa fa-tumblr' => __( ' tumblr', 'ecotheme'), 'fa fa-tumblr-square' => __( ' tumblr-square', 'ecotheme'), 'fa fa-twitch' => __( ' twitch', 'ecotheme'), 'fa fa-twitter' => __( ' twitter', 'ecotheme'), 'fa fa-twitter-square' => __( ' twitter-square', 'ecotheme'), 'fa fa-viacoin' => __( ' viacoin', 'ecotheme'), 'fa fa-vimeo' => __( ' vimeo', 'ecotheme'), 'fa fa-vimeo-square' => __( ' vimeo-square', 'ecotheme'), 'fa fa-vine' => __( ' vine', 'ecotheme'), 'fa fa-vk' => __( ' vk', 'ecotheme'), 'fa fa-wechat' => __( ' wechat', 'ecotheme'), 'fa fa-weibo' => __( ' weibo', 'ecotheme'), 'fa fa-weixin' => __( ' weixin', 'ecotheme'), 'fa fa-whatsapp' => __( ' whatsapp', 'ecotheme'), 'fa fa-wikipedia-w' => __( ' wikipedia-w', 'ecotheme'), 'fa fa-windows' => __( ' windows', 'ecotheme'), 'fa fa-wordpress' => __( ' wordpress', 'ecotheme'), 'fa fa-xing' => __( ' xing', 'ecotheme'), 'fa fa-xing-square' => __( ' xing-square', 'ecotheme'), 'fa fa-y-combinator' => __( ' y-combinator', 'ecotheme'), 'fa fa-y-combinator-square' => __( ' y-combinator-square', 'ecotheme'), 'fa fa-yahoo' => __( ' yahoo', 'ecotheme'), 'fa fa-yc' => __( ' yc', 'ecotheme'), 'fa fa-yc-square' => __( ' yc-square', 'ecotheme'), 'fa fa-yelp' => __( ' yelp', 'ecotheme'), 'fa fa-youtube' => __( ' youtube', 'ecotheme'), 'fa fa-youtube-play' => __( ' youtube-play', 'ecotheme'), 'fa fa-youtube-square' => __( ' youtube-square', 'ecotheme'), 'fa fa-ambulance' => __( ' ambulance', 'ecotheme'), 'fa fa-h-square' => __( ' h-square', 'ecotheme'), 'fa fa-heart' => __( ' heart', 'ecotheme'), 'fa fa-heart-o' => __( ' heart-o', 'ecotheme'), 'fa fa-heartbeat' => __( ' heartbeat', 'ecotheme'), 'fa fa-hospital-o' => __( ' hospital-o', 'ecotheme'), 'fa fa-medkit' => __( ' medkit', 'ecotheme'), 'fa fa-plus-square' => __( ' plus-square', 'ecotheme'), 'fa fa-stethoscope' => __( ' stethoscope', 'ecotheme'), 'fa fa-user-md' => __( ' user-md', 'ecotheme'), 'fa fa-wheelchair' => __( ' wheelchair', 'ecotheme') );
    
    
    
}

function ecotheme_fonts(){
    
    $font_family_array = array(
        'Arial, Helvetica, sans-serif'          => 'Arial',
        'Arial Black, Gadget, sans-serif'       => 'Arial Black',
        'Courier New, monospace'                => 'Courier New',
        'Lobster Two, cursive'                  => 'Lobster - Cursive',
        'Georgia, serif'                        => 'Georgia',
        'Impact, Charcoal, sans-serif'          => 'Impact',
        'Lucida Console, Monaco, monospace'     => 'Lucida Console',
        'Lucida Sans Unicode, Lucida Grande, sans-serif' => 'Lucida Sans Unicode',
        'MS Sans Serif, Geneva, sans-serif'     => 'MS Sans Serif',
        'MS Serif, New York, serif'             => 'MS Serif',
        'Open Sans, sans-serif'                 => 'Open Sans',
        'Source Sans Pro, sans-serif'           => 'Source Sans Pro',
        'Lato, sans-serif'                      => 'Lato',
        'Tahoma, Geneva, sans-serif'            => 'Tahoma',
        'Times New Roman, Times, serif'         => 'Times New Roman',
        'Trebuchet MS, sans-serif'              => 'Trebuchet MS',
        'Verdana, Geneva, sans-serif'           => 'Verdana',
        'Raleway, sans-serif'                   => 'Raleway',
    );
    
    
    return $font_family_array;
}

function ecotheme_font_sizes(){
    
    $font_size_array = array(
        '10px' => '10 px',
        '12px' => '12 px',
        '14px' => '14 px',
        '16px' => '16 px',
        '18px' => '18 px',
        '20px' => '20 px',
    );
    
    return $font_size_array;
    
}



function ecotheme_font_sanitize( $input ) {
    
    $valid_keys = ecotheme_fonts();
    
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
    
}

function ecotheme_font_size_sanitize( $input ) {
    
    $valid_keys = ecotheme_font_sizes();
    
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
    
}

function ecotheme_icon_sanitize( $input ) {
    
    $valid_keys = ecotheme_icons();
    
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
    
}

function ecotheme_text_sanitize( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function ecotheme_slider_transition_sanitize($input) {
    $valid_keys = array(
        'true' => __('Fade', 'ecotheme'),
        'false' => __('Slide', 'ecotheme'),
    );
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
}

function ecotheme_radio_sanitize_enabledisable($input) {
    $valid_keys = array(
        'enable' => __('Enable', 'ecotheme'),
        'disable' => __('Disable', 'ecotheme')
    );
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
}

function ecotheme_radio_sanitize_yesno($input) {
    $valid_keys = array(
        'yes' => __('Yes', 'ecotheme'),
        'no' => __('No', 'ecotheme')
    );
    if (array_key_exists($input, $valid_keys)) {
        return $input;
    } else {
        return '';
    }
}

// checkbox sanitization
function ecotheme_checkbox_sanitize($input) {
    if ($input == 1) {
        return 1;
    } else {
        return '';
    }
}

//integer sanitize
function ecotheme_integer_sanitize($input) {
    return intval($input);
}


function ecotheme_sidebar_sanitize($input) {
    
    $valid = array(
        'none'              => __( 'No Sidebar', 'ecotheme'),
        'right'             => __( 'Right', 'ecotheme'),
        'left'              => __( 'Left', 'ecotheme'),
    );
    
    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return '';
    }
    
    
}

function ecotheme_on_off_sanitize($input) {
    $valid = array(
        'on'    => __( 'Show', 'ecotheme' ),
        'off'    => __( 'Hide', 'ecotheme' )
    );

    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return '';
    }
}

function ecotheme_blogstyle_sanitize($input) {
    $valid = array(
        'stacked'    => __( 'Stacked', 'ecotheme' ),
        'tiles'    => __( 'Tiles', 'ecotheme' )
    );

    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return '';
    }
}

function ecotheme_theme_color_sanitize($input) {
    $valid = array(
        'green'             => __( 'Green', 'ecotheme' ),
        'blue'              => __( 'Blue', 'ecotheme' ),
        'red'               => __( 'Red', 'ecotheme' ),
        'pink'              => __( 'Pink', 'ecotheme' ),
        'yellow'            => __( 'Yellow', 'ecotheme' ),
        'darkblue'          => __( 'Dark Blue', 'ecotheme' ),
    );

    if (array_key_exists($input, $valid)) {
        return $input;
    } else {
        return '';
    }
}

