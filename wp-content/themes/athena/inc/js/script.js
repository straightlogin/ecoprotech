jQuery(document).ready(function ($) {

    function get_height() {

        if (jQuery(window).width() < 601) {

            return jQuery(window).height();
        } else {
            return jQuery(window).height();
        }


    }

    ecotheme_slider();

    function ecotheme_slider() {

        var height = get_height();

        jQuery('#ecotheme-slider').camera({
            height: height + 'px',
            loader: 'bar',
            overlay: false,
            fx: 'simpleFade',
            pagination: false,
            thumbnails: false,
            transPeriod: 1000,
            overlayer: false,
            playPause: false,
            hover: false,
        });
    }
});

jQuery(document).ready(function ($) {

    $('#ecotheme-featured .featured-box').click(function () {

        if( $(this).attr('data-target') && $(this).attr('data-target') != '#' ) {
            window.location.href = $(this).attr('data-target');
        }

    });


    $('.featured-box').hover(function () {

        $('.ecotheme-icon', this).stop(true, false).animate({
            top: '-7px'

        }, 300);
        $('.ecotheme-desc', this).stop(true, false).animate({
            top: '7px'

        }, 300);

        $('.ecotheme-title', this).stop(true, false).animate({
            'letter-spacing': '1.5px'

        }, 300);

    }, function () {
        $('.ecotheme-icon', this).stop(true, false).animate({
            top: '0px'

        }, 300);
        $('.ecotheme-desc', this).stop(true, false).animate({
            top: '0px'

        }, 300);
        $('.ecotheme-title', this).stop(true, false).animate({
            'letter-spacing': '1px'

        }, 300);
    });




    $('#primary-menu').slicknav({
        prependTo: $('.ecotheme-header-menu'),
        label: '',
        allowParentLinks: true
    });

    $('.ecotheme-search, #ecotheme-search .fa.fa-close').click(function () {

        $('#ecotheme-search').fadeToggle(449)

    });

    // Homepage Overlay
    $('#ecotheme-overlay-trigger .fa').click(function () {

        var selector = $('#ecotheme-overlay-trigger');

        if (selector.hasClass('open')) {

            $('.overlay-widget').hide();
            selector.removeClass('open animated slideInUp');

        } else {

            selector.addClass('open animated slideInUp');
            $('.overlay-widget').fadeIn(330);
        }

    });

    // scroll to top trigger
    $('.scroll-top').click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
        return false;
    });

    // scroll to top trigger
    $('.scroll-down').click(function () {

        $("html, body").animate({
            scrollTop: ($(window).height() - 85)
        }, 1000);

        return false;

    });



    // Parallax
    $(window).scroll(function () {

        var s = $(window).scrollTop();

        $('.parallax').css({top: (s / 3.)});

        if (s > $(window).height()) {

            $('#ecotheme-header.frontpage').addClass('sticky animated slideInDown');

        } else {
            $('#ecotheme-header.frontpage').removeClass('sticky animated slideInDown');
        }

    })

    // ------------
    var ecothemeWow = new WOW({
        boxClass: 'reveal',
        animateClass: 'animated',
        offset: 150

    });

    ecothemeWow.init();
    
    
});