<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class WOE_Formatter_Zapier extends WOE_Formatter {
	var $prev_added_zapier = false;
	private $export_type;
	private $start_tag = '[';
	private $end_tag = ']';

	public function __construct( $mode, $filename, $settings, $format, $labels, $field_formats, $date_format ) {
		parent::__construct( $mode, $filename, $settings, $format, $labels, $field_formats, $date_format );
	}

	public function start( $data = '' ) {
		parent::start( $data );

		$this->export_type = $this->settings['global_job_settings']['destination']['zapier_export_type'];
		$this->settings    = array();
		$start_text        = $this->convert_literals( $this->start_tag );

		fwrite( $this->handle, apply_filters( "woe_zapier_start_text", $start_text ) );
	}

	public function output( $rec ) {
		$rec = parent::output( $rec );
		if ( $this->prev_added_zapier ) {
			fwrite( $this->handle, "," );
		}

		//rename fields in array
		$rec_out = array();
		$labels  = $this->labels['order'];

		foreach ( $rec as $field => $value ) {
			if ( explode( "_", $field )[0] === "products" ) {
				$child_labels = $this->labels['products'];
			} elseif ( explode( "_", $field )[0] === "coupons" ) {
				$child_labels = $this->labels['coupons'];
			} else {
				$rec_out[ $labels[ $field ] ] = $value;
				continue;
			}

			$fields = explode( "_", $field );
			if ( 'order' == $this->export_type ) {
				$child_field = join( "_", array_slice( $fields, 1, - 1 ) );
				$child_index = end( $fields );

				$rec_out[ 'Product ' . $child_index . " " . $child_labels[ $child_field ] ] = $value;
			} elseif ( 'order_items' == $this->export_type ) {
				$child_field = join( "_", array_slice( $fields, 1 ) );

				$rec_out[ 'Product ' . $child_labels[ $child_field ] ] = $value;
			}

		}

		$json = json_encode( $rec_out );

		if ( $this->has_output_filter ) {
			$json = apply_filters( "woe_zapier_output_filter", $json, $rec );
		}
		fwrite( $this->handle, $json );

		// first record added!
		if ( ! $this->prev_added_zapier ) {
			$this->prev_added_zapier = true;
		}
	}

	public function finish( $data = '' ) {
		$end_text = $this->convert_literals( $this->end_tag );
		fwrite( $this->handle, apply_filters( "woe_zapier_end_text", $end_text ) );
		parent::finish();
	}
}