<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [affiliate_rank] shortcode
 *
 * @since  1.0.2
 */
function affwp_affiliate_rank_shortcode( $atts, $content = null ) {

	extract( shortcode_atts( array(
		'affiliate_id' 	=> affwp_get_affiliate_id(),
		'state' 		=> 'current',
		'show' 			=> 'rank'
		
	), $atts ) );

	ob_start();
	
	show_affiliate_rank( $affiliate_id, $state, $show );
	
	$content = ob_get_clean();
	return $content;
}
add_shortcode( 'affiliate_rank', 'affwp_affiliate_rank_shortcode' );

/**
 * [affiliate_rank_content] shortcode
 * Renders the content if the current user is an affiliate and has the set rank.
 *
 * @since  1.0.2
 */
function affwp_affiliate_rank_content_shortcode( $atts, $content = null ) {

	extract( shortcode_atts( array(
		'rank_id' => 0,
		
	), $atts ) );
	
	if ( ! affwp_ranks_has_rank( 0, $rank_id ) ) {
		
		ob_start();
		
		show_affiliate_rank_content_msg( 0, $rank_id );
		
		$content = ob_get_clean();
		return $content;

	} else {

		return do_shortcode( $content );
	}
}
add_shortcode( 'affiliate_rank_content', 'affwp_affiliate_rank_content_shortcode' );