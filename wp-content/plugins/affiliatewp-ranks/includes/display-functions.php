<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Display an affiliate's rank
 *
 * @since  1.0.2
 */
function show_affiliate_rank( $affiliate_id = 0, $state = '', $show = '' ) {

	if ( empty( $affiliate_id ) ) return;

	// Show the rank
	if ( $show == 'rank' ) {
		show_affiliate_rank_name( $affiliate_id, $state );
	}
	
	// Show the rank table
	if ( $show == 'table' ) {
		show_affiliate_rank_table( $affiliate_id );
	}
	
}

/**
 * Display an affiliate's rank (Name)
 *
 * @since  1.0.2
 */
function show_affiliate_rank_name( $affiliate_id = 0, $state = '' ) {

	if ( empty( $affiliate_id ) ) $affiliate_id = affwp_get_affiliate_id();
	
	if ( empty( $affiliate_id ) ) return;

	if ( empty( $state ) ) $state = 'current';

	// Show current rank
	if ( $state == 'current' )
		$rank_id = affwp_ranks_get_affiliate_rank( $affiliate_id );
	
	// Show last rank
	if ( $state == 'last' )
		$rank_id = affwp_ranks_get_affiliate_last_rank( $affiliate_id );
	
	// Show next rank
	if ( $state == 'next' )
		$rank_id = affwp_ranks_get_affiliate_next_rank( $affiliate_id );
	
	$rank_data = get_rank_by_id( $rank_id );
	$rank_name = $rank_data[0]['name'];
	
	if ( empty( $rank_id ) || empty( $rank_name ) ) $rank_name = 'None';
	
	?>
	
	<span class="affwp-ranks-aff-rank"><?php echo $rank_name; ?></span>
	
	<?php
	
}

/**
 * Display an affiliate's rank Table (Last, Current, & Next)
 *
 * @since  1.0.2
 */
function show_affiliate_rank_table( $affiliate_id = 0 ) {
	
	if ( empty( $affiliate_id ) ) $affiliate_id = affwp_get_affiliate_id();	
	
	if ( empty( $affiliate_id ) ) return;
	
	// Get affiliate's rank data
	$rank_id = affwp_ranks_get_affiliate_rank( $affiliate_id );
	$rank_data = get_rank_by_id( $rank_id );
	$rank_order = isset( $rank_data[0]['order'] ) ? $rank_data[0]['order'] : 0;
	$rank_name = isset( $rank_data[0]['name'] ) ? $rank_data[0]['name'] : '';
	$rank_mode = isset( $rank_data[0]['mode'] ) ? $rank_data[0]['mode'] : '';
	$rank_type = isset( $rank_data[0]['type'] ) ? $rank_data[0]['type'] : '';
	$rank_requirement = isset( $rank_data[0]['requirement'] ) ? $rank_data[0]['requirement'] : 0;
	$rank_rate = isset( $rank_data[0]['rate'] ) ? $rank_data[0]['rate'] : 0;
	$rank_rate_type = isset( $rank_data[0]['rate_type'] ) ? $rank_data[0]['rate_type'] : '';
	
	// Get last rank
	$last_rank_id = affwp_ranks_get_affiliate_last_rank( $affiliate_id );
	$last_rank_data = get_rank_by_id( $last_rank_id );
	$last_rank_order = isset( $last_rank_data[0]['order'] ) ? $last_rank_data[0]['order'] : 0;
	$last_rank_name = isset( $last_rank_data[0]['name'] ) ? $last_rank_data[0]['name'] : '';
	$last_rank_mode = isset( $last_rank_data[0]['mode'] ) ? $last_rank_data[0]['mode'] : '';
	$last_rank_type = isset( $last_rank_data[0]['type'] ) ? $last_rank_data[0]['type'] : '';
	$last_rank_requirement = isset( $last_rank_data[0]['requirement'] ) ? $last_rank_data[0]['requirement'] : 0;
	$last_rank_rate = isset( $last_rank_data[0]['rate'] ) ? $last_rank_data[0]['rate'] : 0;
	$last_rank_rate_type = isset( $last_rank_data[0]['rate_type'] ) ? $last_rank_data[0]['rate_type'] : '';
	
	// Get next rank
	$next_rank_id = affwp_ranks_get_affiliate_next_rank( $affiliate_id );
	$next_rank_data = get_rank_by_id( $next_rank_id );
	$next_rank_order = isset( $next_rank_data[0]['order'] ) ? $next_rank_data[0]['order'] : 0;
	$next_rank_name = isset( $next_rank_data[0]['name'] ) ? $next_rank_data[0]['name'] : '';
	$next_rank_mode = isset( $next_rank_data[0]['mode'] ) ? $next_rank_data[0]['mode'] : '';
	$next_rank_type = isset( $next_rank_data[0]['type'] ) ? $next_rank_data[0]['type'] : '';
	$next_rank_requirement = isset( $next_rank_data[0]['requirement'] ) ? $next_rank_data[0]['requirement'] : 0;
	$next_rank_rate = isset( $next_rank_data[0]['rate'] ) ? $next_rank_data[0]['rate'] : 0;
	$next_rank_rate_type = isset( $next_rank_data[0]['rate_type'] ) ? $next_rank_data[0]['rate_type'] : '';
	 
	?>
    <h4><?php _e( 'Affiliate Rank', 'affiliatewp-ranks' ); ?></h4>

    <table class="affwp-table table">
        <thead>
            <tr>
                <th><?php _e( 'Last Rank', 'affiliatewp-ranks' ); ?></th>
                <th><?php _e( 'Current Rank', 'affiliatewp-ranks' ); ?></th>
                <th><?php _e( 'Next Rank', 'affiliatewp-ranks' ); ?></th>
            </tr>
        </thead>

        <tbody>
            
            <?php 
                if ( empty( $last_rank_name ) ) $last_rank_name = 'None'; 
                if ( empty( $rank_name ) ) $rank_name = 'None'; 
                if ( empty( $next_rank_name ) ) $next_rank_name = 'None'; 
            ?>
            
            <tr>
                <td><?php echo $last_rank_name; ?></td>
                <td><?php echo $rank_name; ?></td>
                <td><?php echo $next_rank_name; ?></td>
            </tr>

        </tbody>
    </table>
	<?php
}

/**
 * Display a message to affiliates that don't have the required rank
 *
 * @since  1.0.3
 */
function show_affiliate_rank_content_msg( $affiliate_id = 0, $rank_id = 0 ) {
	
	if ( empty( $affiliate_id ) ) $affiliate_id = affwp_get_affiliate_id();
	
	if ( empty( $affiliate_id ) ) return;
	
	if ( empty( $rank_id ) ) return;
	
	// Rank required message goes here

	$rank_data = get_rank_by_id( $rank_id );
	$rank_name = !empty( $rank_data[0]['name'] ) ? $rank_data[0]['name'] : 'Untitled';
	$rank_type = $rank_data[0]['type'];
	$rank_order = $rank_data[0]['order']; // ADD CHECK FOR HIGHER RANK AND ALLOW ACCESS
	$rank_requirement = $rank_data[0]['requirement'];
	$rank_msg_title = $rank_name . ' Rank Only';
	$rank_msg = 'Only affiliates with the <strong>' . $rank_name . ' Rank</strong> can view this content';

	if ( $rank_type == 'referral' )
		$rank_qualification = 'To earn the ' . $rank_name . ' Rank, you must <strong>Refer ' . $rank_requirement . ' Sales</strong>';
		
	if ( ( $rank_type == 'earnings' ) || ( $rank_type == 'earnings_percentage' ) )
		$rank_qualification = 'To earn the ' . $rank_name . ' Rank, you must <strong>Earn ' . affwp_currency_filter( $rank_requirement ) . ' in Commissions</strong>';	
		
	if ( $rank_type == 'sub_affiliate' )
		$rank_qualification = 'To earn the ' . $rank_name . ' Rank, you must <strong>Directly Refer ' . $rank_requirement . ' Sub Affiliates</strong>';	

	// Allow customization
	$rank_msg_title = apply_filters( 'affwp_ranks_rank_content_msg_title', $rank_msg_title, $rank_id, $rank_name, $rank_type, $rank_order, $rank_requirement, $rank_qualification );
	$rank_msg = apply_filters( 'affwp_ranks_rank_content_msg', $rank_msg, $rank_id, $rank_name, $rank_type, $rank_order, $rank_requirement, $rank_qualification );

	?>
	
    <h2 class="affwp-ranks-aff-rank-msg-title"><?php echo $rank_msg_title; ?></h2>
    <p class="affwp-ranks-aff-rank-msg"><?php echo $rank_msg; ?></p>
    
    <?php if ( isset( $rank_type ) && isset( $rank_requirement ) ) { ?>
    	<p class="affwp-ranks-aff-rank-qualification"><?php echo $rank_qualification; ?></p>
    <?php } ?>
	
	<?php

}