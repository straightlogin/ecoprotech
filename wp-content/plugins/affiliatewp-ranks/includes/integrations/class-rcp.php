<?php

class AffiliateWP_Ranks_RCP extends AffiliateWP_Ranks_Base {

	/**
	 * Get things started
	 *
	 * @access public
	 * @since  1.0.3
	*/
	public function init() {

		$this->context = 'rcp';
		
		/* Check for Restrict Content Pro */
		$integrations = affiliate_wp()->settings->get( 'affwp_ranks_integrations' );
				
		if ( ! isset( $integrations['rcp'] ) ) return; // Ranks integration for Restrict Content Pro is disabled 

		// Per-Subscription settings
		add_action( 'rcp_add_subscription_form', array( $this, 'subscription_new' ) );
		add_action( 'rcp_edit_subscription_form', array( $this, 'subscription_edit' ) );
		add_action( 'rcp_add_subscription', array( $this, 'store_subscription_meta' ), 10, 2 );
		add_action( 'rcp_edit_subscription_level', array( $this, 'store_subscription_meta' ), 10, 2 );
		
		// Require Rank
		add_action( 'rcp_form_errors', array( $this, 'require_rank_on_register' ), 10 );
		
		// Assign Rank
		add_action( 'rcp_form_processing', array( $this, 'assign_rank_on_register' ), 10, 2 );
		
	}

	/**
	 * Display Rank fields on add subscription screen
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function subscription_new() {
		$rank_options = $this->ranks_list();
		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="rcp-require-rank"><?php _e( 'Rank Requirement', 'affiliatewp-ranks' ); ?></label>
			</th>
			<td>
				<select name="affwp_ranks_rcp_rank_requirement" id="rcp-require-rank">
					<?php foreach( $rank_options as $r_key => $rank_option ) : ?>
						<option value="<?php echo esc_attr( $r_key ); ?>"><?php echo esc_html( $rank_option ); ?></option>
					<?php endforeach; ?>
				</select>
				<p class="description"><?php _e( 'Choose a rank to require before an affiliate can subscribe to this level.', 'affiliatewp-ranks' ); ?></p>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="rcp-assign-rank"><?php _e( 'Rank Assignment', 'affiliatewp-ranks' ); ?></label>
			</th>
			<td>
				<select name="affwp_ranks_rcp_rank_assignment" id="rcp-assign-rank">
					<?php foreach( $rank_options as $a_key => $rank_option ) : ?>
						<option value="<?php echo esc_attr( $a_key ); ?>"><?php echo esc_html( $rank_option ); ?></option>
					<?php endforeach; ?>
				</select>
				<p class="description"><?php _e( 'Choose a rank to assign automatically when an affiliate subscribes to this level.', 'affiliatewp-ranks' ); ?></p>
			</td>
		</tr>
		<?php
	}

	/**
	 * Display Rank fields on subscription edit screen
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function subscription_edit( $level ) {

		global $rcp_levels_db;

		$rank_requirement = get_option( 'affwp_ranks_rcp_rank_requirement_' . $level->id );
		$rank_assignment = get_option( 'affwp_ranks_rcp_rank_assignment_' . $level->id );
		$rank_options = $this->ranks_list();

		?>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="rcp-require-rank"><?php _e( 'Rank Requirement', 'affiliatewp-ranks' ); ?></label>
			</th>
			<td>
				<select name="affwp_ranks_rcp_rank_requirement" id="rcp-require-rank">
					<?php foreach( $rank_options as $r_key => $rank_option ) : ?>
						<option value="<?php echo esc_attr( $r_key ); ?>"<?php selected( $r_key, $rank_requirement ); ?>><?php echo esc_html( $rank_option ); ?></option>
					<?php endforeach; ?>
				</select>
				<p class="description"><?php _e( 'Choose a rank to require before an affiliate can subscribe to this level.', 'affiliatewp-ranks' ); ?></p>
			</td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="rcp-assign-rank"><?php _e( 'Rank Assignment', 'affiliatewp-ranks' ); ?></label>
			</th>
			<td>
				<select name="affwp_ranks_rcp_rank_assignment" id="rcp-assign-rank">
					<?php foreach( $rank_options as $a_key => $rank_option ) : ?>
						<option value="<?php echo esc_attr( $a_key ); ?>"<?php selected( $a_key, $rank_assignment ); ?>><?php echo esc_html( $rank_option ); ?></option>
					<?php endforeach; ?>
				</select>
				<p class="description"><?php _e( 'Choose a rank to assign automatically when an affiliate subscribes to this level.', 'affiliatewp-ranks' ); ?></p>
			</td>
		</tr>
		<?php
	}

	/**
	 * Store the Rank fields for the subscription level
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function store_subscription_meta( $level_id = 0, $args ) {

		global $rcp_levels_db;
		
		// Store the Rank Requirement field
		if( ! empty( $_POST['affwp_ranks_rcp_rank_requirement'] ) ) {

			update_option( 'affwp_ranks_rcp_rank_requirement_' . $level_id, $_POST['affwp_ranks_rcp_rank_requirement'] );

		} else {

			delete_option( 'affwp_ranks_rcp_rank_requirement_' . $level_id );

		}

		// Store the Rank Assignment field
		if( ! empty( $_POST['affwp_ranks_rcp_rank_assignment'] ) ) {

			update_option( 'affwp_ranks_rcp_rank_assignment_' . $level_id, sanitize_text_field( $_POST['affwp_ranks_rcp_rank_assignment'] ) );

		} else {

			delete_option( 'affwp_ranks_rcp_rank_assignment_' . $level_id );

		}		

		// Make sure RCP version is compatible
		if ( ! is_a( $rcp_levels_db, 'RCP_Levels' ) ) {
			return;
		}

	}
	
	/**
	 * Require a specific rank to register for a subscription level
	 *
	 */
	public function require_rank_on_register( $posted ) {
		
		// Don't require rank for members with existing subscriptions
		//if ( rcp_get_subscription_id() ) return;
		
		$level_id = $posted['rcp_level'];
		$rank_id = get_option( 'affwp_ranks_rcp_rank_requirement_' . $level_id );

		// Check for a required rank
		if ( $rank_id ) {

			$user_id = get_current_user_id();

			if ( ! affwp_is_affiliate( $user_id ) ) return false;

			$user = get_userdata( $user_id ); 
			$affiliate_id = affiliate_wp()->tracking->get_affiliate_id_from_login( $user->user_login );

			if ( ! affwp_ranks_has_rank( $affiliate_id, $rank_id ) ) {
				
				$rank_name = affwp_ranks_get_rank_name( $rank_id );
				
				// Display error if the affiliate doesn't have a higher rank
				if ( ! affwp_ranks_has_higher_rank( $affiliate_id, $rank_id ) ) {
					rcp_errors()->add( 'rank_required', sprintf( __( 'You must have the %s Rank to subscribe to this level.', 'affiliatewp-ranks' ), $rank_name ), 'register' );

				}
			}
		}
	}

	/**
	 * Assigns the per-subscription Rank during registration
	 *
	 */
	public function assign_rank_on_register( $posted, $user_id ) {

		$level_id = $posted['rcp_level'];
		$rank_id = get_option( 'affwp_ranks_rcp_rank_assignment_' . $level_id );
		
		if ( !empty( $rank_id ) ) {
			
			$user_id = get_current_user_id();

			if ( ! affwp_is_affiliate( $user_id ) ) return;
			
			$user = get_userdata( $user_id ); 
			$affiliate_id = affiliate_wp()->tracking->get_affiliate_id_from_login( $user->user_login ); 

			if ( affwp_ranks_has_rank( $affiliate_id, $rank_id ) ) return;

			affwp_ranks_set_affiliate_rank( $affiliate_id, $rank_id );
		}
	}

}
new AffiliateWP_Ranks_RCP;