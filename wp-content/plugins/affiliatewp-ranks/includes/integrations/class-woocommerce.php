<?php

class AffiliateWP_Ranks_WooCommerce extends AffiliateWP_Ranks_Base {

	/**
	 * The order object
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public $order;

	/**
	 * Get things started
	 *
	 * @access public
	 * @since  1.0.3
	*/
	public function init() {

		$this->context = 'woocommerce';

		/* Check for WooCommerce */
		$integrations = affiliate_wp()->settings->get( 'affwp_ranks_integrations' );
				
		if ( ! isset( $integrations['woocommerce'] ) ) return; // Ranks integration for WooCommerce is disabled 	
		
		// Per-Product Rank Settings
		add_filter( 'woocommerce_product_data_tabs', array( $this, 'product_tab' ) );
		add_action( 'woocommerce_product_data_panels', array( $this, 'product_settings' ), 100 );
		add_action( 'save_post', array( $this, 'save_meta' ) );
		
		add_filter( 'woocommerce_is_purchasable', array( $this, 'can_purchase' ), 99999, 2 );
		add_action( 'woocommerce_order_status_completed', array( $this, 'assign_product_rank' ), 10 );
		
	}

	/**
	 * Register the product settings tab
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function product_tab( $tabs ) {
		$tabs['affiliatewp_ranks'] = array(
			'label'  => __( 'AffiliateWP Ranks', 'affiliatewp-ranks' ),
			'target' => 'affwp_product_rank_settings',
			'class'  => array( ),
		);
		return $tabs;
	}	
	
	/**
	 * Adds per-product rank settings input fields
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function product_settings() {
		global $post;
?>
		<div id="affwp_product_rank_settings" class="panel woocommerce_options_panel">

			<div class="options_group">
				<p><?php _e( 'Setup affiliate rank settings for this product', 'affiliatewp-ranks' ); ?></p>
<?php

				woocommerce_wp_select( array(
					'id'          => '_affwp_ranks_woocommerce_rank_requirement',
					'label'       => __( 'Rank Requirement', 'affiliatewp-ranks' ),
					'options' 	  => $this->ranks_list(),
					'desc_tip'    => true,
					'description' => __( 'Choose a rank to require before an affiliate can buy this product.', 'affiliatewp-ranks' )
				) );		
		
				woocommerce_wp_select( array(
					'id'          => '_affwp_ranks_woocommerce_rank_assignment',
					'label'       => __( 'Rank Assignment', 'affiliatewp-ranks' ),
					'options' 	  => $this->ranks_list(),
					'desc_tip'    => true,
					'description' => __( 'Choose a rank to assign automatically when an affiliate buys this product.', 'affiliatewp-ranks' )
				) );
			
			/*
				woocommerce_wp_checkbox( array(
					'id'          => '_affwp_ranks_woocommerce_ignore_requirements',
					'label'       => __( 'Ignore Rank Assignment Requirements', 'affiliatewp-ranks' ),
					'description' => __( 'This will assign the rank after purchasing this product, without checking the requirements first.', 'affiliatewp-ranks' ),
					'cbvalue'     => 1
				) );

			*/
				wp_nonce_field( 'affwp_ranks_woo_product_nonce', 'affwp_ranks_woo_product_nonce' );
?>
			</div>
		</div>
<?php
	}
	
	/**
	 * Saves per-product rank settings input fields
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function save_meta( $post_id = 0 ) {
		
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
		
		// Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
		
		if ( empty( $_POST['affwp_ranks_woo_product_nonce'] ) || ! wp_verify_nonce( $_POST['affwp_ranks_woo_product_nonce'], 'affwp_ranks_woo_product_nonce' ) ) {
			return $post_id;
		}
		
		$post = get_post( $post_id );
		
		if ( ! $post ) {
			return $post_id;
		}
		
		// Check post type is product
		if ( 'product' != $post->post_type ) {
			return $post_id;
		}
		
		// Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		// Save Rank Requirement Field
		if ( ! empty( $_POST['_affwp_ranks_' . $this->context . '_rank_requirement'] ) ) {
			$rank_req = $_POST['_affwp_ranks_' . $this->context . '_rank_requirement'];
			update_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_rank_requirement', $rank_req );
		} else {
			delete_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_rank_requirement' );
		}
		
		// Save Rank Assignment Field
		if ( ! empty( $_POST['_affwp_ranks_' . $this->context . '_rank_assignment'] ) ) {
			$rank_assign = $_POST['_affwp_ranks_' . $this->context . '_rank_assignment'];
			update_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_rank_assignment', $rank_assign );
		} else {
			delete_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_rank_assignment' );
		}

		/* Save Ignore Rank Requirements Field
		if ( isset( $_POST['_affwp_ranks_' . $this->context . '_ignore_requirements'] ) ) {
			update_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_ignore_requirements', 1 );
		} else {
			delete_post_meta( $post_id, '_affwp_ranks_' . $this->context . '_ignore_requirements' );
		}
		*/
	}	

	/**
	 * Require a specific rank to purchase a product
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function can_purchase( $ret, $product ) {
		
		if ( $ret ) {
			$can_purchase = true;
			$rank_id = get_post_meta( $product->id, '_affwp_ranks_' . $this->context . '_rank_requirement', true );
			
			// Check for a required rank
			if ( $rank_id ) {
				
				$user_id = get_current_user_id();

				if ( ! affwp_is_affiliate( $user_id ) ) return false;

				$user = get_userdata( $user_id ); 
				$affiliate_id = affiliate_wp()->tracking->get_affiliate_id_from_login( $user->user_login );

				if ( ! affwp_ranks_has_rank( $affiliate_id, $rank_id ) ) {
					
					// Allow if the affiliate has a higher rank
					if ( affwp_ranks_has_higher_rank( $affiliate_id, $rank_id ) ) {
						$can_purchase = true;
					} else{
						$can_purchase = false;
					}

				}
			}

			$ret = $can_purchase;
		}
		return $ret;		
		
	}	
	
	/**
	 * Assign the per-product rank after purchase
	 *
	 * @access  public
	 * @since   1.0.3
	*/
	public function assign_product_rank( $order_id = 0 ) {
		
		$this->order = apply_filters( 'affwp_get_woocommerce_order', new WC_Order( $order_id ) );
		$items = $this->order->get_items();
		$user_id = $this->order->customer_user;
		
		// Assign after affiliate account is created
		if ( ! affwp_is_affiliate( $user_id ) ) {
			$this->assign_later = true;
			
			return;
		} 
		
		$user = get_userdata( $user_id ); 
		$affiliate_id = affiliate_wp()->tracking->get_affiliate_id_from_login( $user->user_login ); 		

		foreach ( $items as $product ) {
			
			$rank_id 			 = get_post_meta( $product['product_id'], '_affwp_ranks_' . $this->context . '_rank_assignment', true );
			// $ignore_requirements = get_post_meta( $product['product_id'], '_affwp_ranks_' . $this->context . '_ignore_requirements', true );
			
			if ( !empty( $rank_id ) ) {
				
				if ( affwp_ranks_has_rank( $affiliate_id, $rank_id ) ) continue;
				
				affwp_ranks_set_affiliate_rank( $affiliate_id, $rank_id );
				
				/* See if they qualify for the rank
				if ( ! $ignore_requirements ) {
					
					if ( affwp_ranks_is_qualified( $affiliate_id, $rank_id ) )
						affwp_ranks_set_affiliate_rank( $affiliate_id, $rank_id );
						
				} else{
					affwp_ranks_set_affiliate_rank( $affiliate_id, $rank_id );
				}
				*/
				
			} else{
				continue; // No rank to assign for this product
			}
			
		}
		
	}
	
}
new AffiliateWP_Ranks_WooCommerce;