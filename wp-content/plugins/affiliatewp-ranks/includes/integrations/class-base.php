<?php

class AffiliateWP_Ranks_Base {

	/**
	 * The context for referrals. This refers to the integration that is being used.
	 *
	 * @access  public
	 * @since   1.0
	 */
	public $context;

	/**
	 * The ID of the referring affiliate
	 *
	 * @access  public
	 * @since   1.0
	 */
	public $affiliate_id;
	
	public function __construct() {
	
		$this->affiliate_id = affiliate_wp()->tracking->get_affiliate_id();
		$this->init();
	}

	/**
	 * Gets things started
	 *
	 * @access  public
	 * @since   1.0
	 * @return  void
	 */
	public function init() {
		
	}

	/**
	 * Get Formatted Ranks List for Rank Select Fields
	 *
	 * @since 1.0.3
	 * @return void
	 */
	public function ranks_list() {

		// Get all ranks
		$ranks = get_ranks();
		$ranks_list = array( '' => __( 'None', 'affiliatewp-ranks' ) );
		
		if ( $ranks && ! empty( $ranks ) ) {

			foreach( $ranks as $rank_key => $rank_option ) {

				$ranks_list[ $rank_option['id'] ] = $rank_option['name'];

			}	

		}
		
		return $ranks_list;
		
	}

	
}