<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Handle license checks, activations and deactivations
 *
 * @author flowdee
 * @version 1.0.0
 */
class KryptoniteWP_License_Handler {

    // Defaults
    private $license_server = '';
    private $item_id = 0;
    private $item_name = '';
    private $url = '';

    /**
     * Class constructor.
     */
    function __construct( $data ) {

        $this->license_server = ( ! empty ( $data['license_server'] ) ) ? $data['license_server'] : 'https://kryptonitewp.com';
        $this->item_id = ( isset( $data['item_id'] ) ) ? intval( $data['item_id'] ) : 0;
        $this->item_name = ( isset( $data['item_name'] ) ) ? $data['item_name'] : '';
        $this->url = $data['url'];
    }

    /**
     * Check license key update
     *
     * @return mixed
     */
    public function handle_update( $new_license_key, $old_license_key ) {
        
        $deactivate = false;
        $activate = false;

        // Key deleted
        if ( empty( $new_license_key ) && !empty( $old_license_key ) ) {
            $deactivate = true;
        }
    
        // Key changed
        if ( !empty( $new_license_key ) && !empty( $old_license_key ) && $new_license_key != $old_license_key ) {
            $deactivate = true;
            $activate = true;
        }
    
        // Key submitted
        if ( !empty( $new_license_key ) && empty( $old_license_key ) ) {
            $activate = true;
        }

        //var_dump($deactivate);
    
        // Execute deactivation of current license key
        if ( $deactivate ) {
            $response = $this->deactivate( $old_license_key );
        }

        //var_dump($activate);
    
        // Execute activation of new license key
        if ( $activate ) {
            $response = $this->activate( $new_license_key );
        }

        if ( !empty ( $response ) )
            return $response;

        return false;
    }

    /**
     * Activate license key
     *
     * @return string
     */
    public function activate( $license_key ) {

        // retrieve the license from the database
        $license_key = trim( $license_key );

        // data to send in our API request
        $api_params = array(
            'edd_action'=> 'activate_license',
            'license' 	=> $license_key,
            'item_id' => $this->item_id,
            'url' => $this->url
        );

        // Call the custom API.
        $response = wp_remote_post( $this->license_server, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        // make sure the response came back okay
        if ( is_wp_error( $response ) )
            return false;

        // decode the license data
        $license_data = json_decode( wp_remote_retrieve_body( $response ) );

        // $license_data->license will be either "valid" or "invalid"

        return $license_data->license;
    }

    /**
     * Deactivate license key
     *
     * @return string
     */
    public function deactivate( $license_key ) {

        // retrieve the license from the database
        $license_key = trim( $license_key );

        // data to send in our API request
        $api_params = array(
            'edd_action'=> 'deactivate_license',
            'license' 	=> $license_key,
            'item_id' => $this->item_id,
            'url'       => $this->url
        );

        // Call the custom API.
        $response = wp_remote_post( $this->license_server, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

        // make sure the response came back okay
        if ( is_wp_error( $response ) )
            return false;

        // decode the license data
        $license_data = json_decode( wp_remote_retrieve_body( $response ) );

        // $license_data->license will be either "deactivated" or "failed"
        return $license_data->license;
    }

}