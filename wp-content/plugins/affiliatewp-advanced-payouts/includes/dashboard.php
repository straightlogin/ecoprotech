<?php
/**
 * Handle notices in affiliate's dashboard
 *
 * @param $affiliate_id
 * @param $active_tab
 */
function affwp_advpay_dashboard_notices( $affiliate_id, $active_tab ) {

    $affwp_settings = get_option( 'affwp_settings' );

    $affiliate = affwp_get_affiliate( $affiliate_id );
    $user_id = $affiliate->user_id;

    $request_payout_url = affwp_advpay_get_payout_url();
    $settings_tab_url = affwp_get_affiliate_area_page_url('settings');

    $affiliate_payouts_available = affwp_advpay_is_affiliate_payout_available( $affiliate_id );
    $affiliate_payout_status = affwp_advpay_get_affiliate_payout_status( $user_id );

    // Handle billing data validation
    $affiliate_billing_data_is_valid = affwp_advpay_check_affiliate_billing_data( $user_id );

    // Support for "Payout Statement"'s billing data
    $affiliate_ps_billing_data_is_valid = ( function_exists( 'affwp_ps_profile_settings_fields_validated' ) && ! empty( $affwp_settings['affwp_advpay_require_ps_billing_data'] ) && ! empty( $affwp_settings['affwp_ps_billing_data_required'] ) ) ? affwp_ps_profile_settings_fields_validated( $affiliate_id ) : true;

    // Maybe output notices
    if ( 'pending' === $affiliate_payout_status ) { ?>
        <p class="affwp-notice"><?php echo affwp_advpay_get_payout_pending_notice(); ?></p>

    <?php } elseif ( $affiliate_payouts_available && ! $affiliate_billing_data_is_valid ) { ?>
        <p class="affwp-notice"><?php printf(wp_kses(__('Please enter your billing data on the <a href="%s">settings tab</a> in order to request payouts.', 'affiliatewp-advanced-payouts'), array('a' => array('href' => array()))), esc_url( $settings_tab_url ) ); ?></p>

    <?php } elseif ( $affiliate_payouts_available && ! $affiliate_ps_billing_data_is_valid ) { ?>
        <?php // Do nothing, because Payout Statements already shows a notice ?>

    <?php } elseif ( $affiliate_payouts_available && $affiliate_billing_data_is_valid ) { ?>
        <p class="affwp-notice"><?php printf( wp_kses( __( 'You have unpaid earnings available to be paid out: <a href="%s">Request a payout</a>.', 'affiliatewp-advanced-payouts' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $request_payout_url ) ); ?></p>

    <?php }
}
add_action( 'affwp_affiliate_dashboard_top', 'affwp_advpay_dashboard_notices', 10, 2 );

/**
 * Extending "statistics" tab in affiliate's dashboard
 *
 * @param $affiliate_id
 */
function affwp_advpay_dashboard_extend_statistics( $affiliate_id ) {

    $affiliate    = affwp_get_affiliate( $affiliate_id );
    $user_id      = $affiliate->user_id;

    $min_amount = affwp_advpay_get_min_amount();

    $affiliate_payouts_available = affwp_advpay_is_affiliate_payout_available( $affiliate_id );
    $affiliate_payout_status = affwp_advpay_get_affiliate_payout_status( $user_id );
    ?>

    <table class="affwp-table">
        <thead>
        <tr>
            <th><?php _e( 'Payouts: Minimum Amount', 'affiliatewp-advanced-payouts' ); ?></th>
            <th><?php _e( 'Payouts: Current Status', 'affiliatewp-advanced-payouts' ); ?></th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td><?php echo affwp_currency_filter( ( ! empty( $min_amount ) ) ? $min_amount : 0 ); ?></td>
            <td>
                <?php if ( $affiliate_payout_status === 'pending' ) { ?>
                    <span style="color: cornflowerblue;"><?php _e('Pending', 'affiliatewp-advanced-payouts'); ?></span>
                <?php } elseif ( $affiliate_payouts_available ) { ?>
                    <span style="color: green;"><?php _e('Your earnings are available to be paid out.', 'affiliatewp-advanced-payouts'); ?></span
                <?php } else { ?>
                    <span style="color: red;"><?php _e('Minimum amount for a payout request not reached yet.', 'affiliatewp-advanced-payouts'); ?></span>
                <?php } ?>
            </td>
        </tr>
        </tbody>
    </table>

    <?php
}
add_action( 'affwp_affiliate_dashboard_after_earnings', 'affwp_advpay_dashboard_extend_statistics' );