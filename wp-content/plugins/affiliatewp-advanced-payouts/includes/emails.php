<?php
/**
 * E-Mails
 *
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * E-Mail notifications
 */
function affwp_advpay_notifications( $affiliate_id ) {

    $affwp_settings = get_option( 'affwp_settings' );

    if ( ! empty( $affwp_settings['affwp_advpay_admin_notification'] ) ) {
        affwp_advpay_admin_notification( $affiliate_id );
    }

    if ( ! empty( $affwp_settings['affwp_advpay_affiliate_notification'] ) ) {
        affwp_advpay_affiliate_notification( $affiliate_id );
    }
}
add_action( 'affwp_advpay_create_payout_request', 'affwp_advpay_notifications' );

/*
 * Send admin email notification
 */
function affwp_advpay_admin_notification( $affiliate_id ) {

    //affwp_advpay_debug_log( 'affwp_advpay_admin_notification' );

    $affwp_settings = get_option( 'affwp_settings' );

    $emails = new Affiliate_WP_Emails;
    $emails->__set( 'affiliate_id', $affiliate_id );

    $email = ( !empty( $affwp_settings['affwp_advpay_admin_notification_email'] ) ) ? $affwp_settings['affwp_advpay_admin_notification_email'] : get_bloginfo( 'admin_email' );
    $subject = ( !empty( $affwp_settings['affwp_advpay_admin_notification_subject'] ) ) ? $affwp_settings['affwp_advpay_admin_notification_subject'] : __( 'New affiliate payout requested!', 'affiliatewp-advanced-payouts' );
    $message = ( !empty( $affwp_settings['affwp_advpay_admin_notification_message'] ) ) ? $affwp_settings['affwp_advpay_admin_notification_message'] : sprintf( __( 'A new affiliate payout was requested on your site: %s', 'affiliatewp-advanced-payouts' ), home_url() ) . "\n\n" . __( 'Name: ', 'affiliatewp-advanced-payouts' ) . "{name}\n\n" . __( 'Unpaid Earnings: ', 'affiliatewp-advanced-payouts' ) . "{advpay_unpaid_earnings}";

    $email = apply_filters( 'affwp_advpay_admin_notification_email', $email, $affiliate_id );
    $subject = apply_filters( 'affwp_advpay_admin_notification_subject', $subject, $affiliate_id );
    $message = apply_filters( 'affwp_advpay_admin_notification_message', $message, $affiliate_id );

    //affwp_advpay_debug_log( $email );
    //affwp_advpay_debug_log( $subject );
    //affwp_advpay_debug_log( $message );

    $sent = $emails->send( $email, $subject, $message );

    //affwp_advpay_debug_log( '$sent >>> ' . $sent );
}

/*
 * Send admin email notification
 */
function affwp_advpay_affiliate_notification( $affiliate_id ) {

    //affwp_advpay_debug_log( 'affwp_advpay_affiliate_notification' );

    $affwp_settings = get_option( 'affwp_settings' );

    $emails = new Affiliate_WP_Emails;
    $emails->__set( 'affiliate_id', $affiliate_id );

    $affiliate = affwp_get_affiliate( $affiliate_id );
    $user_info = get_userdata($affiliate->user_id);

    $email = $user_info->user_email;
    $subject = ( !empty( $affwp_settings['affwp_advpay_affiliate_notification_subject'] ) ) ? $affwp_settings['affwp_advpay_affiliate_notification_subject'] : __( 'New affiliate payout requested!', 'affiliatewp-advanced-payouts' );
    $message = ( !empty( $affwp_settings['affwp_advpay_affiliate_notification_message'] ) ) ? $affwp_settings['affwp_advpay_affiliate_notification_message'] : sprintf( __( 'Thanks for requesting an affiliate payout on %s.', 'affiliatewp-advanced-payouts' ), home_url() ) . "\n\n" . __( 'Unpaid Earnings: ', 'affiliatewp-advanced-payouts' ) . "{advpay_unpaid_earnings}\n\n" . __( 'Payment Email: ', 'affiliatewp-advanced-payouts' ) . "{advpay_payment_email}\n\nWe will review your request as soon as possible.";

    $email = apply_filters( 'affwp_advpay_affiliate_notification_email', $email, $affiliate_id );
    $subject = apply_filters( 'affwp_advpay_affiliate_notification_subject', $subject, $affiliate_id );
    $message = apply_filters( 'affwp_advpay_affiliate_notification_message', $message, $affiliate_id );

    //affwp_advpay_debug_log( $email );
    //affwp_advpay_debug_log( $subject );
    //affwp_advpay_debug_log( $message );

    $sent = $emails->send( $email, $subject, $message );

    //affwp_advpay_debug_log( '$sent >>> ' . $sent );
}

/*
 * Search and replace notification custom placeholders
 */
function affwp_advpay_search_replace_email_placeholders( $message, $affiliate_id ) {

    $affiliate = affwp_get_affiliate( $affiliate_id );
    $user_id = $affiliate->user_id;

    if (strpos($message, '{advpay_unpaid_earnings}') !== false) {
        $message = str_replace("{advpay_unpaid_earnings}", affwp_currency_filter( affwp_get_affiliate_unpaid_earnings( $affiliate_id ) ), $message);
    }

    if (strpos($message, '{advpay_unpaid_referrals}') !== false) {
        $message = str_replace("{advpay_unpaid_referrals}", affwp_count_referrals( affwp_get_affiliate_id(), 'unpaid' ), $message);
    }

    if (strpos($message, '{advpay_payment_email}') !== false) {
        $message = str_replace("{advpay_payment_email}", $affiliate->payment_email, $message);
    }

    if (strpos($message, '{advpay_name}') !== false) {
        $message = str_replace("{advpay_name}", get_user_meta($user_id,  'affwp_advpay_name', true ), $message);
    }

    if (strpos($message, '{advpay_address}') !== false) {
        $message = str_replace("{advpay_address}", get_user_meta($user_id,  'affwp_advpay_address', true ), $message);
    }

    if (strpos($message, '{advpay_tax_number}') !== false) {
        $message = str_replace("{advpay_tax_number}", get_user_meta($user_id,  'affwp_advpay_tax_number', true ), $message);
    }

    return $message;
}
add_action( 'affwp_advpay_admin_notification_message', 'affwp_advpay_search_replace_email_placeholders', 100, 2 );
add_action( 'affwp_advpay_affiliate_notification_message', 'affwp_advpay_search_replace_email_placeholders', 100, 2 );