<?php
/**
 * Check if affiliates should be able to request payouts
 *
 * @return bool
 */
function affwp_advpay_get_status() {

    $affwp_settings = get_option( 'affwp_settings' );

    return ( !empty( $affwp_settings['affwp_advpay_enabled'] ) ) ? true : false;
}

/**
 * Get minimum amount needed for payouts
 *
 * @param bool $format
 * @return array|int
 */
function affwp_advpay_get_min_amount( $format = false ) {

    $affwp_settings = get_option( 'affwp_settings' );

    $min_amount = ( !empty( $affwp_settings['affwp_advpay_min_amount'] ) ) ? $affwp_settings['affwp_advpay_min_amount'] : 0;

    if ( $format ) {
        $min_amount = affwp_currency_filter( $min_amount );
    }

    return $min_amount;
}

/**
 * Get request payout page id
 *
 * @return null
 */
function affwp_advpay_get_custom_page_id() {

    $affwp_settings = get_option( 'affwp_settings' );

    return ( !empty( $affwp_settings['affwp_advpay_custom_page'] ) ) ? $affwp_settings['affwp_advpay_custom_page'] : null;
}

/**
 * Get request payout page url
 *
 * @return false|string
 */
function affwp_advpay_get_payout_url() {

    $custom_page_id = affwp_advpay_get_custom_page_id();

    if ( !empty( $custom_page_id ) ) {
        return get_permalink( $custom_page_id );
    } else {

        $tab = 'stats';
        $tab = apply_filters( 'affwp_advpay_request_payout_process_tab', $tab );

        $nonce_url = wp_nonce_url( add_query_arg( array( 'tab' => $tab, 'affwp_action' => 'advpay_process' ) ), 'advpay_process', 'affwp_advpay_nonce');

        return esc_url( $nonce_url );
    }
}

/*
 * Check if billing data fields will be shown
 */
function affwp_advpay_show_billing_data() {

    $affwp_settings = get_option( 'affwp_settings' );

    return ( !empty( $affwp_settings['affwp_advpay_show_billing_data'] ) ) ? true : false;
}

/**
 * Check if billing data fields are required
 *
 * @return bool
 */
function affwp_advpay_require_billing_data() {

    $affwp_settings = get_option( 'affwp_settings' );

    return ( !empty( $affwp_settings['affwp_advpay_require_billing_data'] ) ) ? true : false;
}

/**
 * Check if billing data fields are filled out
 *
 * @param $user_id
 * @return bool
 */
function affwp_advpay_check_affiliate_billing_data( $user_id ) {

    if ( !affwp_advpay_require_billing_data() )
        return true;

    // Variables
    $name  =  get_user_meta($user_id,  'affwp_advpay_name', true );
    $address  = get_user_meta($user_id,  'affwp_advpay_address', true );

    return ( !empty ( $name ) && !empty( $address) ) ? true : false;
}

/**
 * Get custom email tags
 *
 * @return array
 */
function affwp_advpay_get_custom_email_tags() {

    $custom_tags = array(
        array(
            'tag' => 'advpay_unpaid_earnings',
            'description' => __('Unpaid Earnings', 'affiliatewp-advanced-payouts')
        ),
        array(
            'tag' => 'advpay_unpaid_referrals',
            'description' => __('Unpaid Referrals', 'affiliatewp-advanced-payouts')
        ),
        array(
            'tag' => 'advpay_name',
            'description' => __('Billing data: Name', 'affiliatewp-advanced-payouts')
        ),
        array(
            'tag' => 'advpay_address',
            'description' => __('Billing data: Address', 'affiliatewp-advanced-payouts')
        ),
        array(
            'tag' => 'advpay_tax_number',
            'description' => __('Billing data: Tax number', 'affiliatewp-advanced-payouts')
        )
    );

    return $custom_tags;
}

/**
 * Get extended email tags list
 *
 * @return string
 */
function affwp_advpay_get_extend_email_tags_list() {

    $tags_list = '';

    // Whitelisted basic tags
    $whitelisted_basic_tags = array('name', 'user_name', 'user_email', 'website', 'login_url', 'site_name', 'referral_url', 'affiliate_id');

    // Get basic tags
    $basic_tags = affiliate_wp()->emails->get_tags();

    // Remove non-whitelisted basic tags
    foreach ( $basic_tags as $key => $email_tag) {
        if ( !in_array( $email_tag['tag'], $whitelisted_basic_tags ) )
            unset($basic_tags[$key]);
    }

    // Add custom tags
    $custom_tags = affwp_advpay_get_custom_email_tags();

    // Merge basic and custom tags
    $email_tags = array_merge($basic_tags, $custom_tags);

    $email_tags = apply_filters( 'affwp_advpay_email_tags', $email_tags );

    if( count( $email_tags ) > 0 ) {
        foreach( $email_tags as $email_tag ) {
            $tags_list .= '{' . $email_tag['tag'] . '} - ' . $email_tag['description'] . '<br />';
        }
    }

    // Return
    return $tags_list;
}

/**
 * Update affiliate's payout status
 *
 * @param $user_id
 * @param $status
 */
function affwp_advpay_update_affiliate_payout_status( $user_id, $status ) {

    if ( $status === 'delete' ) {
        delete_user_meta( $user_id, 'affwp_advpay_affiliate_payout_status' );
    } else {
        update_user_meta( $user_id, 'affwp_advpay_affiliate_payout_status', $status );
    }
}

/**
 * Get affiliate's payout status
 *
 * @param $user_id
 * @return mixed
 */
function affwp_advpay_get_affiliate_payout_status( $user_id ) {

    $status = get_user_meta($user_id, 'affwp_advpay_affiliate_payout_status', true );

    return $status;
}

/**
 * Check if affiliate's payouts are available
 *
 * @param $affiliate_id
 * @return bool
 */
function affwp_advpay_is_affiliate_payout_available( $affiliate_id ) {

    $unpaid_earnings = affwp_get_affiliate_unpaid_earnings( $affiliate_id );
    $min_amount = affwp_advpay_get_min_amount();

    return ( intval( $min_amount ) <= intval( $unpaid_earnings ) ) ? true : false;
}

/**
 * Total amount of pending payouts
 *
 * @return int|mixed
 */
function affwp_advpay_get_pending_payouts_amount() {

    $cache = get_transient( 'affwp_advpay_pending_payouts_amount' );

    if ( !empty( $cache ) )
        return $cache;

    $pending = 0;

    $affiliates = affiliate_wp()->affiliates->get_affiliates( array( 'number' => -1 ) );

    if ( $affiliates ) {
        foreach( $affiliates as $affiliate ) {
            if ( affwp_advpay_get_affiliate_payout_status( $affiliate->user_id ) === 'pending' )
                $pending++;
        }
    }

    set_transient( 'affwp_advpay_pending_payouts_amount', $pending, 60 * 60 * 1 ); // 1 hour

    return $pending;
}

/**
 * Clear total amount of pending payouts
 */
function affwp_advpay_clear_pending_payouts_amount() {
    delete_transient( 'affwp_advpay_pending_payouts_amount' );
}

/**
 * Get "payount pending" dashboard notice
 *
 * @return string
 */
function affwp_advpay_get_payout_pending_notice() {

    $affwp_settings = get_option( 'affwp_settings' );

    return ( ! empty( $affwp_settings['affwp_advpay_payout_pending_notice'] ) ) ? $affwp_settings['affwp_advpay_payout_pending_notice'] : __( 'Your payout request was received and will be processed soon.', 'affiliatewp-advanced-payouts' );
}
