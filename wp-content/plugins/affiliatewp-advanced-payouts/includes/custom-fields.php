<?php
/*
 * Affiliate WP
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/*
 * Add custom settings to template
 */
function affwp_advpay_add_custom_settings( $affiliate_id, $affiliate_user_id ) {

    if ( !affwp_advpay_show_billing_data() )
        return;

    $billing_data_req = affwp_advpay_require_billing_data();

    $affiliate_id  = affwp_get_affiliate_id();
    $user_id      = affwp_get_affiliate_user_id( $affiliate_id );

    // Variables
    $name  =  get_user_meta($user_id,  'affwp_advpay_name', true );
    $address  = get_user_meta($user_id,  'affwp_advpay_address', true );
    $tax_number  = get_user_meta($user_id,  'affwp_advpay_tax_number', true );
    ?>

    <?php if ( $billing_data_req ) { ?>
        <style type="text/css">
            .affwp-req { color: darkred; }
        </style>
    <?php } ?>

    <hr />

    <h4><?php _e('Payout billing data', 'affiliatewp-advanced-payouts'); ?></h4>

    <?php if ( $billing_data_req ) { ?>
        <p><?php //_e('In order to payout your earnings and creating a receipt, we need you to submit your address data.', 'affiliatewp-advanced-payouts'); ?></p>
    <?php } ?>

    <div class="affwp-wrap affwp-name-wrap">
        <label for="affwp-name"><?php _e( 'Name', 'affiliatewp-advanced-payouts' ); ?> <?php if ( $billing_data_req ) { ?><span class="affwp-req">*</span><?php } ?></label>
        <input id="affwp-name" type="text" name="affwp_advpay_name" value="<?php echo esc_attr( $name ); ?>" <?php if ( $billing_data_req ) { ?>required="required"<?php } ?> />
    </div>

    <div class="affwp-wrap affwp-address-wrap">
        <label for="affwp-address"><?php _e( 'BTC Wallet', 'affiliatewp-advanced-payouts' ); ?> <?php if ( $billing_data_req ) { ?><span class="affwp-req">*</span><?php } ?></label>
        <input id="affwp-address" type="text" name="affwp_advpay_address" value="<?php echo esc_attr( $address ); ?>" /></input>
    </div>

    <div class="affwp-wrap affwp-tax-number-wrap">
        <label for="affwp-tax-number"><?php _e( 'ETH Wallet', 'affiliatewp-advanced-payouts' ); ?></label>
        <input id="affwp-tax-number" type="text" name="affwp_advpay_tax_number" value="<?php echo esc_attr( $tax_number ); ?>" />
    </div>
    <?php

}
add_action( 'affwp_affiliate_dashboard_before_submit', 'affwp_advpay_add_custom_settings', 10, 2 );

/*
 * Save custom settings to user meta
 */
function affwp_advpay_save_custom_settings( $data ) {

    if ( !affwp_advpay_show_billing_data() )
        return;

    $affiliate_id = absint( $data['affiliate_id'] );
    $user_id      = affwp_get_affiliate_user_id( $affiliate_id );

    // Name
    if ( ! empty( $data['affwp_advpay_name'] ) ) {
        update_user_meta( $user_id, 'affwp_advpay_name', $data['affwp_advpay_name'] );
    } else {
        delete_user_meta( $user_id, 'affwp_advpay_name' );
    }

    // Address
    if ( ! empty( $data['affwp_advpay_address'] ) ) {
        update_user_meta( $user_id, 'affwp_advpay_address', $data['affwp_advpay_address'] );
    } else {
        delete_user_meta( $user_id, 'affwp_advpay_address' );
    }

    // Tax number
    if ( ! empty( $data['affwp_advpay_tax_number'] ) ) {
        update_user_meta( $user_id, 'affwp_advpay_tax_number', $data['affwp_advpay_tax_number'] );
    } else {
        delete_user_meta( $user_id, 'affwp_advpay_tax_number' );
    }

}
add_action( 'affwp_update_affiliate_profile_settings', 'affwp_advpay_save_custom_settings', 10, 1 );

/*
 * Save custom settings to profile
 */
function affwp_advpay_edit_affiliate_fields( $affiliate ) {

    if ( !affwp_advpay_show_billing_data() )
        return;

    //$affiliate_id = absint( $data['affiliate_id'] );
    //$user_id      = affwp_get_affiliate_user_id( $affiliate_id );

    $affiliate    = affwp_get_affiliate( absint( $_GET['affiliate_id'] ) );
    $user_id      = $affiliate->user_id;

    $name  =  get_user_meta($user_id,  'affwp_advpay_name', true );
    $address  = get_user_meta($user_id,  'affwp_advpay_address', true );
    $tax_number  = get_user_meta($user_id,  'affwp_advpay_tax_number', true );
    $payout_status  = get_user_meta($user_id,  'affwp_advpay_affiliate_payout_status', true );

    ?>
    <!-- Payout Status -->
    <tr class="form-row">
        <th scope="row">
            <label for="affwp_advpay_payout_status"><?php _e( 'Payout Status', 'affiliatewp-advanced-payouts' ); ?></label>
        </th>

        <td>
            <strong id="affwp_advpay_payout_status">
                <?php if ( $payout_status === 'pending' ) { ?>
                    <span style="color: cornflowerblue;"><?php _e('Pending', 'affiliatewp-advanced-payouts'); ?></span>
                <?php } else { ?>
                    <span style="color: green;"><?php _e('Open', 'affiliatewp-advanced-payouts'); ?></span>
                <?php } ?>
            </strong>
        </td>
    </tr>
    
    <!-- Name -->
    <tr class="form-row form-required">
        <th scope="row">
            <label for="affwp_advpay_name"><?php _e( 'Name', 'affiliatewp-advanced-payouts' ); ?></label>
        </th>

        <td>
            <input class="regular-text" type="text" name="affwp_advpay_name" id="affwp_advpay_name" value="<?php echo esc_attr( $name ); ?>"/>
            <p class="description"><?php _e( 'Affiliate\'s name needed for billing purpose.', 'affiliatewp-advanced-payouts' ); ?></p>
        </td>
    </tr>

    <!-- Address -->
    <tr class="form-row form-required">
        <th scope="row">
            <label for="affwp_advpay_address"><?php _e( 'BTC Wallet', 'affiliatewp-advanced-payouts' ); ?></label>
        </th>

        <td>
            <input class="regular-text" type="text" name="affwp_advpay_address" id="affwp_advpay_address" value="<?php echo esc_attr( $address ); ?>"/></input>
            <p class="description"><?php _e( 'BTC Wallet for Affiliator', 'affiliatewp-advanced-payouts' ); ?></p>
        </td>
    </tr>

    <!-- Tax number -->
    <tr class="form-row">
        <th scope="row">
            <label for="affwp_advpay_tax_number"><?php _e( 'ETH Wallet', 'affiliatewp-advanced-payouts' ); ?></label>
        </th>

        <td>
            <input class="regular-text" type="text" name="affwp_advpay_tax_number" id="affwp_advpay_tax_number" value="<?php echo esc_attr( $tax_number ); ?>"/>
            <p class="description"><?php _e( 'ETH Wallet for Affiliator' ); ?></p>
        </td>
    </tr>
    <?php
}

add_action( 'affwp_edit_affiliate_end', 'affwp_advpay_edit_affiliate_fields', 10, 1 );

/*
 * Save edit affiliate fields
 */
function affwp_advpay_save_edit_affiliate_fields( $data ) {

    // Silence

}
add_action( 'affwp_post_update_affiliate', 'affwp_advpay_save_edit_affiliate_fields', 10, 1 );