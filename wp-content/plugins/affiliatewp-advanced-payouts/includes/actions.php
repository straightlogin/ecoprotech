<?php
/**
 * Process payout request
 *
 * @param $request
 */
function affwp_advpay_action_process_payout_request( $request ) {

    if ( ! isset( $_GET['affwp_advpay_nonce'] ) || ! wp_verify_nonce( $_GET['affwp_advpay_nonce'], 'advpay_process' ) )
        return;

    $affiliate_id = affwp_get_affiliate_id();
    $user_id = affwp_get_affiliate_user_id( $affiliate_id );

    // Prevent multiple requests
    if ( affwp_advpay_get_affiliate_payout_status( $user_id ) === 'pending' )
        return;

    $affiliate_payouts_available = affwp_advpay_is_affiliate_payout_available( $affiliate_id );

    if ( $affiliate_payouts_available ) {
        affwp_advpay_update_affiliate_payout_status( $user_id , 'pending' );

        do_action( 'affwp_advpay_create_payout_request', $affiliate_id );
    }
}
add_action( 'affwp_advpay_process', 'affwp_advpay_action_process_payout_request' );

/**
 * Mark requested payout as solved
 *
 * @param $referral_id
 * @param $new_status
 * @param $old_status
 */
function affwp_advpay_action_referral_status_update_solves_pending_payout_request( $referral_id, $new_status, $old_status ) {

    if ( $old_status === 'unpaid' && $new_status === 'paid' ) {
        $referral = affwp_get_referral( $referral_id );
        $affiliate_id = $referral->affiliate_id;
        $affiliate = affwp_get_affiliate( $affiliate_id );
        $user_id = $affiliate->user_id;

        if ( affwp_advpay_get_affiliate_payout_status( $user_id ) === 'pending' ) {
            affwp_advpay_update_affiliate_payout_status( $user_id, 'delete');

            affwp_advpay_clear_pending_payouts_amount();
        }
    }
}
add_action( 'affwp_set_referral_status', 'affwp_advpay_action_referral_status_update_solves_pending_payout_request', 10, 3 );