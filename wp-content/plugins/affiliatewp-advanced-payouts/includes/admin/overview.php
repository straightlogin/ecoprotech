<?php
/*
 * Overview table
 */
function affwp_advpay_admin_overview() {

    ?>

    <div id="affwp-advpay-dashboard-widgets-wrap">
        <div id="affwp-advpay-dashboard-widgets" class="metabox-holder">

            <div id="affwp-advpay-postbox-container-1" class="postbox-container" style="margin: 0 16px;">

                <div class="postbox">
                    <h3><?php _e( 'Payouts Pending', 'affiliatewp-advanced-payouts' ); ?></h3>
                    <div class="inside">
                        <?php $affiliates = affiliate_wp()->affiliates->get_affiliates( array( 'number' => -1 ) ); ?>
                        <table class="affwp_table">

                            <thead>
                                <tr>
                                    <th><?php _e( 'Affiliate', 'affiliatewp-advanced-payouts' ); ?></th>
                                    <th><?php _e( 'Unpaid Earnings', 'affiliatewp-advanced-payouts' ); ?></th>
                                    <th><?php _e( 'Actions', 'affiliatewp-advanced-payouts' ); ?></th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php if ( $affiliates ) : ?>

                                <?php $i = 0; ?>

                                <?php foreach( $affiliates as $affiliate  ) : ?>
                                    <?php if ( affwp_advpay_get_affiliate_payout_status( $affiliate->user_id ) != 'pending' ) continue; ?>
                                    <tr>
                                        <td><?php echo affiliate_wp()->affiliates->get_affiliate_name( $affiliate->affiliate_id ); ?></td>
                                        <td><?php echo affwp_currency_filter( affwp_get_affiliate_unpaid_earnings( $affiliate->affiliate_id ) ); ?></td>
                                        <?php
                                        $affiliate_user_info = get_userdata($affiliate->user_id );
                                        $affiliate_username = $affiliate_user_info->user_login;
                                        $affiliate_payout_now_url = admin_url( 'admin.php?page=affiliate-wp-referrals&status=unpaid&affiliate_id=' . $affiliate_username );
                                        ?>
                                        <td><?php echo '<a href="' . esc_url( $affiliate_payout_now_url ) . '">' . __( 'Payout now!', 'affiliatewp-advanced-payouts' ) . '</a>'; ?></td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>

                                <?php if ( $i === 0 ) { ?>
                                    <tr>
                                        <td colspan="3"><?php _e( 'No pending affiliate payouts.', 'affiliatewp-advanced-payouts' ); ?></td>
                                    </tr>
                                <?php } ?>

                            <?php else : ?>
                                <tr>
                                    <td colspan="3"><?php _e( 'No affiliate registrations yet', 'affiliatewp-advanced-payouts' ); ?></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>

                        </table>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <?php
}
add_action( 'affwp_overview_bottom', 'affwp_advpay_admin_overview' );

/**
 * Menu item badge for pending payouts
 */
function affwp_advpay_admin_overview_notification_badge() {
    global $menu;

    $pending_payouts = affwp_advpay_get_pending_payouts_amount();

    if ( empty( $pending_payouts ) )
        return;

    foreach ( $menu as $key => $value ) {

        if ( $menu[$key][2] == 'affiliate-wp' ) {
            $menu[$key][0] .= ' <span class="awaiting-mod"><span class="update-count">' . $pending_payouts . '</span></span>';
            return;
        }
    }
}
add_action('admin_menu', 'affwp_advpay_admin_overview_notification_badge');