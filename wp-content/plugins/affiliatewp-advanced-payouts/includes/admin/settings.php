<?php
/**
 * Settings
 *
 * @since       1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Register settings
 *
 * @param $settings
 * @return mixed
 */
function affwp_advpay_add_settings( $settings ) {

    $settings['affwp_advpay_settings'] = array(
        'name' => '<strong>' . __( 'Advanced Payouts', 'affiliatewp-advanced-payouts' ) . '</strong>',
        'desc' => __( 'Configure the Advanced Payouts options', 'affiliatewp-advanced-payouts' ),
        'type' => 'header'
    );

    $settings['affwp_advpay_license_key'] = array(
        'name' => __( 'License Key', 'affiliatewp-advanced-payouts' ),
        'type' => 'text',
        'desc' => __( 'In order to receive updates and new features a valid license is required.', 'affiliatewp-advanced-payouts' )
    );

    $settings['affwp_advpay_enabled'] = array(
        'name' => __( 'Payout Requests', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Check in order to allow payout requests. Anyway you can use the min amount and payout status functionality.', 'affiliatewp-advanced-payouts' ),
        'type' => 'checkbox',
        'std' => '1'
    );

    $settings['affwp_advpay_min_amount'] = array(
        'name' => __( 'Minimum Amount', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Default amount of unpaid earnings required in order to request a payout. Leave empty or insert 0 if you want to disable this barrier.', 'affiliatewp-advanced-payouts' ),
        'type' => 'number',
        'size' => 'small',
        'step' => '1.00',
        'std' => '20'
    );

    /*
    $settings['affwp_advpay_waiting_time'] = array(
        'name' => __( 'Waiting Time', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'The amount of days which must be passed before a referral will be counted as "valid". This makes sense if you are offering something like a Money-Back-Guarantee. Your input will not be shown inside the affiliate dashboard. Please note that this setting only takes affect when checking whether an affiliate should be able to request a payout or not. Other parts of AffiliateWP do not make use of this setting.', 'affiliatewp-advanced-payouts' ),
        'type' => 'number',
        'size' => 'small',
        'step' => '1',
        'std' => '0'
    );
    */

    $settings['affwp_advpay_show_billing_data'] = array(
        'name' => __( 'Billing data fields', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Check in order to show the billing data fields on the affiliate dashboard settings tab.', 'affiliatewp-advanced-payouts' ),
        'type' => 'checkbox',
        'std' => '1'
    );

    $settings['affwp_advpay_require_billing_data'] = array(
        'name' => __( 'Billing data required', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Check and affiliates will be forced to fillout the billing data fields before payouts can be requested.', 'affiliatewp-advanced-payouts' ),
        'type' => 'checkbox',
        'std' => '1'
    );

    if ( function_exists( 'affwp_ps_profile_settings_fields_validated' ) ) {

        $settings['affwp_advpay_require_ps_billing_data'] = array(
            'name' => __( 'Billing data required', 'affiliatewp-advanced-payouts' ) . '<br>' . __( '(Payout Statements Addon)', 'affiliatewp-advanced-payouts' ),
            'desc' => __( 'Check and affiliates will be forced to fillout the billing data fields of our "Payout Statements" addon before payouts can be requested. Please make sure that you require billing data inside the settings of "Payout Statements" as well.', 'affiliatewp-advanced-payouts' ),
            'type' => 'checkbox',
            'std' => '0'
        );
    }

    $settings['affwp_advpay_custom_page'] = array(
        'name' => __( 'Custom Payout Page', 'affiliatewp-advanced-payouts' ) . '<br>' . __( '(optional)', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Select the page the affiliate will be redirected to when requesting a payout. Here you can use your own workflow with e.g. GravityForms.<br /><strong>Note:</strong> Using this method will <u>deactivate</u> the email notifications below.', 'affiliatewp-advanced-payouts' ),
        'type' => 'select',
        'options' => affwp_get_pages(),
        'sanitize_callback' => 'absint'
    );

    $settings['affwp_advpay_payout_pending_notice'] = array(
        'name' => __( 'Payout Pending: Dashboard Notice', 'affiliatewp-advanced-payouts' ),
        'type' => 'text',
        'std' => __( 'Your payout request was received and will be processed soon.', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'This message will be shown inside the affiliate dashboard after an affiliate requested a payout which is still pending.', 'affiliatewp-advanced-payouts' )
    );

    $settings['affwp_advpay_admin_notification'] = array(
        'name' => __( 'Admin Notification', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Check in order to notify admins when a new affiliate payout was requested.', 'affiliatewp-advanced-payouts' ),
        'type' => 'checkbox',
        'std' => '1'
    );

    $settings['affwp_advpay_admin_notification_email'] = array(
        'name' => __( 'Admin Notification: E-Mail', 'affiliatewp-advanced-payouts' ),
        'type' => 'text',
        'std' => get_bloginfo( 'admin_email' )
    );

    $settings['affwp_advpay_admin_notification_subject'] = array(
        'name' => __( 'Admin Notification: Subject', 'affiliatewp-advanced-payouts' ),
        'type' => 'text',
        'std' => __( 'New affiliate payout requested!', 'affiliatewp-advanced-payouts' )
    );

    $settings['affwp_advpay_admin_notification_message'] = array(
        'name' => __( 'Admin Notification: Message', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Enter the email to send when a new affiliate payout was requested. HTML is accepted. Available template tags:', 'affiliatewp-advanced-payouts' ) . '<br />' . affwp_advpay_get_extend_email_tags_list(),
        'type' => 'rich_editor',
        'std' => sprintf( __( 'A new affiliate payout was requested on your site: %s', 'affiliatewp-advanced-payouts' ), home_url() ) . "\n\n" . __( 'Name: ', 'affiliatewp-advanced-payouts' ) . "{name}\n\n" . __( 'Unpaid Earnings: ', 'affiliatewp-advanced-payouts' ) . "{advpay_unpaid_earnings}"
    );

    $settings['affwp_advpay_affiliate_notification'] = array(
        'name' => __( 'Affiliate Notification', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Check in order to notify affiliates after they requested a payout.', 'affiliatewp-advanced-payouts' ),
        'type' => 'checkbox',
        'std' => '1'
    );

    $settings['affwp_advpay_affiliate_notification_subject'] = array(
        'name' => __( 'Affiliate Notification: Subject', 'affiliatewp-advanced-payouts' ),
        'type' => 'text',
        'std' => __( 'Your affiliate payout request', 'affiliatewp-advanced-payouts' )
    );

    $settings['affwp_advpay_affiliate_notification_message'] = array(
        'name' => __( 'Affiliate Notification: Message', 'affiliatewp-advanced-payouts' ),
        'desc' => __( 'Enter the email to send when an affiliate requested a payout. HTML is accepted. Available template tags:', 'affiliatewp-advanced-payouts' ) . '<br />' . affwp_advpay_get_extend_email_tags_list(),
        'type' => 'rich_editor',
        'std' => sprintf( __( 'Thanks for requesting an affiliate payout on %s.', 'affiliatewp-advanced-payouts' ), home_url() ) . "\n\n" . __( 'Unpaid Earnings: ', 'affiliatewp-advanced-payouts' ) . "{advpay_unpaid_earnings}\n\n" . __( 'Payment Email: ', 'affiliatewp-advanced-payouts' ) . "{advpay_payment_email}\n\n" . __( 'We will review your request as soon as possible.', 'affiliatewp-advanced-payouts' )
    );

    return $settings;
}
add_filter( 'affwp_settings_integrations', 'affwp_advpay_add_settings' );

/**
 * Handle update of license key setting
 *
 * @param $input
 * @return mixed
 */
function affwp_advpay_settings_update_license_key( $input ) {

    $affwp_settings = get_option( 'affwp_settings' );

    $kryptonitewp_license_handler = affwp_advpay_license_handler();

    $new_license_key = ( ! empty( $input['affwp_advpay_license_key'] ) ) ? $input['affwp_advpay_license_key'] : null;
    $old_license_key = ( ! empty ( $affwp_settings['affwp_advpay_license_key'] ) ) ? $affwp_settings['affwp_advpay_license_key'] : null;

    // Validate license key update
    $kryptonitewp_license_handler->handle_update( $new_license_key, $old_license_key );

    return $input;
}
add_filter( 'affwp_settings_integrations_sanitize', 'affwp_advpay_settings_update_license_key' );

/**
 * License handler
 */
if( ! class_exists( 'KryptoniteWP_License_Handler' ) ) {
    include( AFFWP_ADVPAY_DIR . 'vendor/kryptonitewp-license-handler.php' );
}

function affwp_advpay_license_handler() {

    $kryptonitewp_license_handler = new KryptoniteWP_License_Handler( array(
        'license_server' => AFFWP_ADVPAY_LICENSE_SERVER,
        'item_id' => 1249,
        'url' => home_url()
    ));

    return $kryptonitewp_license_handler;
}