<?php
/**
 * Affiliates Table: Show notice when an affiliate has open payout request
 *
 * @param $value
 * @param $affiliate
 * @return string
 */
function affwp_advpay_admin_affiliates_table_unpaid_earnings( $value, $affiliate ) {

    $user_id = $affiliate->user_id;
    $affiliate_payout_status = affwp_advpay_get_affiliate_payout_status( $user_id );

    if ( 'pending' === $affiliate_payout_status ) {
        $value .= '<br><span style="color: orange;">' . __( 'Payout requested' ,'affiliatewp-advanced-payouts' ) . '</span>';
    }

    return $value;
}
add_filter( 'affwp_affiliate_table_unpaid_earnings', 'affwp_advpay_admin_affiliates_table_unpaid_earnings', 10, 2);