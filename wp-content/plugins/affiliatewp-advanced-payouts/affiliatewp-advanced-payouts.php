<?php
/**
 * Plugin Name:     AffiliateWP - Advanced Payouts
 * Plugin URI:      https://kryptonitewp.com/downloads/affiliatewp-advanced-payouts/
 * Description:     Extending AffiliateWP with payout requests, minimum amount for payouts and more!
 * Version:         1.1.2
 * Author:          KryptoniteWP
 * Author URI:      https://kryptonitewp.com
 * Text Domain:     affiliatewp-advanced-payouts
 * Domain Path:     /languages
 *
 * @author          KryptoniteWP <support@kryptonitewp.com>
 * @copyright       Copyright (c) KryptoniteWP
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
}

if( !class_exists( 'AFFILIATEWP_ADVANCED_PAYOUTS' ) ) {

    /**
     * Main AFFILIATEWP_ADVANCED_PAYOUTS class
     *
     * @since       1.0.0
     */
    class AFFILIATEWP_ADVANCED_PAYOUTS {

        /**
         * @var         AFFILIATEWP_ADVANCED_PAYOUTS $instance The one true AFFILIATEWP_ADVANCED_PAYOUTS
         * @since       1.0.0
         */
        private static $instance;


        /**
         * Get active instance
         *
         * @access      public
         * @since       1.0.0
         * @return      object self::$instance The one true AFFILIATEWP_ADVANCED_PAYOUTS
         */
        public static function instance() {
            if( !self::$instance ) {
                self::$instance = new AFFILIATEWP_ADVANCED_PAYOUTS();
                self::$instance->setup_constants();
                self::$instance->includes();
                self::$instance->load_textdomain();
            }

            return self::$instance;
        }


        /**
         * Setup plugin constants
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         */
        private function setup_constants() {

            // Plugin name
            define( 'AFFWP_ADVPAY_NAME', 'AffiliateWP - Advanced Payouts' );

            // Plugin version
            define( 'AFFWP_ADVPAY_VER', '1.1.2' );

            // Plugin path
            define( 'AFFWP_ADVPAY_DIR', plugin_dir_path( __FILE__ ) );

            // Plugin URL
            define( 'AFFWP_ADVPAY_URL', plugin_dir_url( __FILE__ ) );

            // Licensing & Update Server
            define( 'AFFWP_ADVPAY_LICENSE_SERVER', 'https://kryptonitewp.com' );
        }


        /**
         * Include necessary files
         *
         * @access      private
         * @since       1.0.0
         * @return      void
         */
        private function includes() {

            // Get out if Affiliate WP is not active
            if( ! class_exists( 'Affiliate_WP' ) ) {
                return;
            }

            // Include files and scripts
            require_once AFFWP_ADVPAY_DIR . 'includes/helper.php';

            if ( is_admin() ) {
                require_once AFFWP_ADVPAY_DIR . 'includes/admin/overview.php';
                require_once AFFWP_ADVPAY_DIR . 'includes/admin/affiliates.php';
                require_once AFFWP_ADVPAY_DIR . 'includes/admin/settings.php';
                //require_once AFFWP_ADVPAY_DIR . 'includes/admin/updater.php';
            }

            require_once AFFWP_ADVPAY_DIR . 'includes/functions.php';
            require_once AFFWP_ADVPAY_DIR . 'includes/actions.php';
            require_once AFFWP_ADVPAY_DIR . 'includes/custom-fields.php';
            require_once AFFWP_ADVPAY_DIR . 'includes/dashboard.php';
            require_once AFFWP_ADVPAY_DIR . 'includes/emails.php';
        }

        /**
         * Internationalization
         *
         * @access      public
         * @since       1.0.0
         * @return      void
         */
        public function load_textdomain() {
            
            // Set filter for plugin's languages directory
            $lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
            $lang_dir = apply_filters( 'affiliatewp_advanced_payouts_languages_directory', $lang_dir );

            // Traditional WordPress plugin locale filter
            $locale   = apply_filters( 'plugin_locale',  get_locale(), 'affiliatewp-advanced-payouts' );
            $mofile   = sprintf( '%1$s-%2$s.mo', 'affiliatewp-advanced-payouts', $locale );

            // Setup paths to current locale file
            $mofile_local  = $lang_dir . $mofile;
            $mofile_global = WP_LANG_DIR . '/affiliatewp-advanced-payouts/' . $mofile;

            if ( file_exists( $mofile_global ) ) {
                // Look in global /wp-content/languages/affiliatewp-advanced-payouts/ folder
                load_textdomain( 'affiliatewp-advanced-payouts', $mofile_global );
            } elseif ( file_exists( $mofile_local ) ) {
                // Look in local /wp-content/plugins/affiliatewp-advanced-payouts/languages/ folder
                load_textdomain( 'affiliatewp-advanced-payouts', $mofile_local );
            } else {
                // Load the default language files
                load_plugin_textdomain( 'affiliatewp-advanced-payouts', false, $lang_dir );
            }
        }


        /*
         * Activation function fires when the plugin is activated.
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
        public static function activation() {
            // nothing
        }

        /*
         * Uninstall function fires when the plugin is being uninstalled.
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
        public static function uninstall() {
            // nothing
        }
    }

    /**
     * The main function responsible for returning the one true AFFILIATEWP_ADVANCED_PAYOUTS
     * instance to functions everywhere
     *
     * @since       1.0.0
     * @return      \AFFILIATEWP_ADVANCED_PAYOUTS The one true AFFILIATEWP_ADVANCED_PAYOUTS
     */
    function AFFWP_ADVPAY_load() {

        return AFFILIATEWP_ADVANCED_PAYOUTS::instance();
    }

    /**
     * The activation & uninstall hooks are called outside of the singleton because WordPress doesn't
     * register the call from within the class hence, needs to be called outside and the
     * function also needs to be static.
     */
    register_activation_hook( __FILE__, array( 'AFFILIATEWP_ADVANCED_PAYOUTS', 'activation' ) );
    register_uninstall_hook( __FILE__, array( 'AFFILIATEWP_ADVANCED_PAYOUTS', 'uninstall') );

    add_action( 'plugins_loaded', 'AFFWP_ADVPAY_load' );

} // End if class_exists check

/*
 * Plugin action links
 */
function affwp_advpay_plugin_action_links( $links, $file ) {

    // Get out if Affiliate WP is not active
    if( ! class_exists( 'Affiliate_WP' ) ) {
        return $links;
    }

    $settings_link = '<a href="' . admin_url( 'admin.php?page=affiliate-wp-settings&tab=integrations' ) . '">' . esc_html__( 'Settings', 'affiliatewp-advanced-payouts' ) . '</a>';
    if ( $file == 'affiliatewp-advanced-payouts/affiliatewp-advanced-payouts.php' )
        array_unshift( $links, $settings_link );
    return $links;
}
add_filter( 'plugin_action_links', 'affwp_advpay_plugin_action_links', 10, 2 );

/*
 * Plugin Updater
 */
if( !class_exists( 'AFFWP_ADVPAY_EDD_SL_Plugin_Updater' ) ) {
    include(dirname(__FILE__) . '/vendor/EDD_SL_Plugin_Updater.php');
}

function affwp_advpay_plugin_updater() {

    $affwp_settings = get_option( 'affwp_settings' );

    if ( !empty( $affwp_settings['affwp_advpay_license_key'] ) ) {

        $affwp_advpay_updater = new AFFWP_ADVPAY_EDD_SL_Plugin_Updater( AFFWP_ADVPAY_LICENSE_SERVER, __FILE__, array(
                'version' 	=> AFFWP_ADVPAY_VER,
                'license' 	=> $affwp_settings['affwp_advpay_license_key'],
                'item_id' => 1249,
                'author' 	=> 'KryptoniteWP'
            )
        );
    }
}
add_action( 'admin_init', 'affwp_advpay_plugin_updater', 0 );