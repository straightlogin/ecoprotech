=== AffiliateWP - Advanced Payouts ===
Contributors: flowdee, kryptonitewp
Donate link: https://donate.flowdee.de
Tags: affiliate, affiliatewp, payouts, advanced, advanced payouts, flowdee, kryptonitewp
Requires at least: 3.5.1
Tested up to: 4.9.4

== Changelog ==

= Version 1.1.2 (10th March 2018) =
* Improvement: Added filter "affwp_advpay_request_payout_process_tab" for defining the process payout request target tab

= Version 1.1.1 (17th February 2018) =
* Improvement: The "Payout Now!" link on the AffiliateWP overview page now leads to a pre-filtered list of unpaid referrals of the related affiliate
* WordPress 4.9.4 compatibility

= Version 1.1.0 (4th February 2018) =
* New: Added support for "Affiliate Payouts" addon in order to relate on its "require billing fields" setting
* New: Added "Payout requested" notice into the "Unpaid Earnings" column on the affiliates overview table
* New: Added setting for updating the newly added "Payout Pending" notice, shown on the affiliate dashboard
* Improvement: Rebuild affiliate dashboard notices
* Improvement: Optimized info shown on affiliate dashboard tab "statistics"
* Fix: Pending payouts didn't show up anymore on AffiliateWP's admin overview page
* Minor improvements and fixes
* Updated plugin updater
* Compatibility to latest WordPress and AffiliateWP releases

= Version 1.0.1 (3rd March 2016) =
* Hide plugin settings link when AffiliateWP is not active

= Version 1.0.0 (20th February 2016) =
* Initial release