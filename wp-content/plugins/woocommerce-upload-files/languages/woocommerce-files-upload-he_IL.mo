��    ]           �      �     �  0   �  6   .  :   e     �  M   �     �     �     	     	     6	     I	     ^	     d	  	   q	     {	     �	  E   �	     �	     �	     
     -
     9
  	   G
     Q
  
   e
     p
     �
     �
     �
  
   �
     �
     �
     �
     �
                ;     R     o     �     �  	   �     �  "   �     �  Q   �     P     X     a     z     �     �  
   �     �  	   �     �     �          "     2     K     f  ,   k     �  	   �      �     �     �     �  .   �  	        )     2     9     A     F     U  1   m  3   �  E   �  @     <   Z     �  
   �     �  
   �     �     �     �     �     �  �  �  !   �  6   �  C     M   E     �  Q   �     �     �  !     $   7  #   \  #   �     �     �     �     �  "   �  k   	  %   u     �     �     �     �     �     �               /     6     H     \     p  *   �     �     �     �          4  "   R  )   u     �     �     �  7   �  5        H  [   V     �  
   �  %   �     �       %   (     N  7   ^     �     �  #   �  7   �       ,   4  8   a     �  "   �  
   �     �     �  
   �             '   0  	   X     b     o  
   v  
   �     �     �  .   �  2   �  @   #  >   d  9   �  
   �     �               #  
   >     I     P     Y     F          ?                 V       W   [      4      5   D   =          2   -           3   (   O       H   +   Y       0       G       >   L       	       U      C   6   :   '      ]   K                X   ;       M          1      /   "      8   
   E              J   Z          7              &   $      9   ,       <   I      R          N   A          \       .   #   B   T   @   %                            )      Q                      *         P       S   !         Max allowed size:   size is incorrect or its type is not allowed.   %s field group duplicated. %s field groups duplicated. %s field group synchronised. %s field groups synchronised. Active Active <span class="count">(%s)</span> Active <span class="count">(%s)</span> Add New Add New Field Add New Field Group Advanced Custom Fields Allowed file types Allowed file types:  Apply Bulk Actions Changelog Close Window Custom Fields Customise WordPress with powerful, professional and intuitive fields. Deleting file, please wait... Deleting, pelase wait...  Deleting, please wait... Description Documentation Duplicate Duplicate this item Edit Field Edit Field Group Field Field Group Field Groups Field Keys Field group deleted. Field group draft updated. Field group duplicated. %s Field group published. Field group saved. Field group scheduled for. Field group submitted. Field group synchronised. %s Field group title is required Field group updated. Fields File size File size must be at least %s. File size must must not exceed %s. Inactive Inactive <span class="count">(%s)</span> Inactive <span class="count">(%s)</span> Loading Location Max file size (MB) limit Max size: %s MB Move Complete. Move Custom Field Move Field Move to trash. Are you sure? New Field New Field Group No Field Groups found No Field Groups found in Trash No Fields found No Fields found in Trash No toggle fields available Null Please select the destination for this field Pro Resources Save in progress, please wait... Search Search Field Groups Search Fields See what's new in <a href="%s">version %s</a>. Select %s Settings Status Support Sync Sync available Synchronise field group Thank you for creating with <a href="%s">ACF</a>. The %s field can now be found in the %s field group The changes you made will be lost if you navigate away from this page The string "field_" may not be used at the start of a field name This field cannot be moved until its changes have been saved Title To upload: Upgrade Database View Field View Field Group Warning Website copy or Project-Id-Version: WooCommerce Upload Files
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-19 19:15+0200
PO-Revision-Date: 2018-07-19 19:15+0200
Last-Translator: Eitan Shaked <eitan.shak@gmail.com>
Language-Team: Hebrew
Language: he_IL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.9
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 גודל קובץ מקסימלי: גודל או סוג הקובץ אינם נתמכים. %s קבוצת שדה כפולה %s קבוצות שדה כפולות %s קבוצת שדה מסונכרנת %s קבוצת שדה מסונכרנות פעיל פעיל <span class="count">(%s)</span> פעיל <span class="count">(%s)</span> הוסף חדש הוסף שדה חדש הוסף שדה קבוצה חדש שדות מתאמים מתקדמים סוגי קבצים מאושרים: סוגי קבצים מאושרים: החל פעולות מרובות שינויים סגור חלון שדות מותאמים אישית התאם אישית את WordPress בעזרת שדות חזקים, מתקדמים ואינטאיטיבים. מוחק קובץ, אנא המתן... מוחק, אנא המתן... מוחק, אנא המתן... תאור מסמכים שכפל שכפל זאת ערוך שדה ערוך קבוצת שדה שדה קבוצת שדה קבוצות שדה מפתחות שדה קבוצת שדה נמחקה. טיוטת קבוצת שדה עודכנה. שדה כפול נמצא. %s קבוצת שדה פורסמה. קבוצת שדה נשמרה. קבוצת שדה תוזמנה. קבוצת שדה נשלחה. שדה קבוצה סונכרן. %s כותרת לקבוצת שדה דרושה קבוצת שדה עודכנה. שדות גודל קובץ גודל הקובץ חייב להיות גדול מ %s. גודל הקובץ חייב להיות קטן מ %s. לא פעיל לא פעיל <span class="count">(%s)</span> לא פעיל <span class="count">(%s)</span> טוען מיקום גדול קובץ מקסימלי (MB) גודל מקסימלי: %s MB ההעברה הסתיימה. העבר שדה מותאם אישית העבר שדה העברה לפח המחזור. האם אתה בטוח? שדה חדש קבוצת שדה חדשה לא נמצאו קבוצות שדה לא נמצאו קבוצות שדה בפח המחזור לא נמצאו שדות לא נמצאו שדות בפח המחזור לא נמצאו שדות שינוי מצב פנויים. ריק אנא בחר יעד לשדה זה מתקדם מקורות שומר, אנא המתן.... חיפוש חשב קבוצות שדה חפש שדות %s   בדוק מה חדש בגרסה %s בחר %s הגדרות מצב תמיכה סנכרן סינכרון זמין סנכרן קבוצת שדה תודה שיצרת עם <a href="%s">ACF</a>. השדה %s נמצא כעת בשדה קבוצה %s השיוניין שבצעת ייאבדו אם תצא מדף זה "field_" לא ניתן לשימוש בתחילת שם השדה השדה אינו ניתן להעברה ללא שמירה כותרת ממתין לשליחה: עדכן Database צפה בשדה ערוך קבוצת שדה אזהרה אתר העתק או 