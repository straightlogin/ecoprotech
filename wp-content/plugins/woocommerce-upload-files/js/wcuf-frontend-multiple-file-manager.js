var wcuf_multiple_files_queues = new Array();
var wcuf_images_to_preview = new Array();
var wcuf_canvas;
var wcuf_canvas_contex;
jQuery(document).on('click', '.wcuf_delete_single_file_in_multiple_list', wcuf_delete_single_file_in_multiple_list);
jQuery(document).on('change', '.wcuf_quantity_per_file_input', wcuf_set_quantity_per_file);

function wcuf_manage_multiple_file_browse(evt)
{
	var id =  jQuery(evt.currentTarget).data('id'); 
	var disable_image_preview =  jQuery(evt.currentTarget).data('images-preview-disabled'); 
	var detect_pdf =  jQuery(evt.currentTarget).data('detect-pdf'); 
	var options = {'disable_image_preview':disable_image_preview, 'detect_pdf':detect_pdf};
	var files = evt.target.files;
	
	if(typeof wcuf_multiple_files_queues[id] === 'undefined')
		wcuf_multiple_files_queues[id] = new Array();
	
	//jQuery('button.button#wcuf_upload_multiple_files_button_'+id).fadeIn();
	jQuery('button.button#wcuf_upload_multiple_files_button_'+id).css('display', 'inline-block');
	
	wcuf_canvas = document.createElement("canvas");
	wcuf_canvas_contex = wcuf_canvas.getContext("2d");
	
	for( var i = 0; i < files.length; i++)
	{
		files[i].quantity = 1;
		wcuf_multiple_files_queues[id].push(files[i]);
		wcuf_append_new_file_ui(id,files[i], options);
	}
	wcuf_process_next_element_to_preview();//starts the process: ALTERNATIVE METHOD NOT USED
	jQuery( document.body ).trigger( 'wcuf_added_multiple_files' );
	//console.log(wcuf_multiple_files_queues);
}
//the id is not relative to the file but to the upload field unique id
function wcuf_append_new_file_ui(id, file, options)
{
	var manage_pdf = options.detect_pdf && wcuf_is_pdf_file(file);
	var is_quantity_per_file_box_visible = !wcuf_enable_select_quantity_per_file || (manage_pdf) ? 'style="display:none"' : '';
	var template = '<div class="wcuf_single_file_in_multiple_list" >';
		//template +=  '<h4>'+wcuf_multiple_file_list_tile+'</h4>';
		template +=  '<div class="wcuf_single_file_name_container" >';
		template +=    '<span class="wcuf_single_file_name_in_multiple_list">'+file.name+'</span>';
		template +=    '<i data-id="'+id+'" class="wcuf_delete_single_file_in_multiple_list wcuf_delete_file_icon"></i>';
		template +=   '</div>';
		template +=   '<div class="wcuf_quantity_per_file_container" >';
		template +=     '<div class="wcuf_media_preview_container"><img width="50" class="wcuf_image_quantity_preview"></img></div>';
		template +=     '<span class="wcuf_quantity_per_file_label" '+is_quantity_per_file_box_visible+' >'+wcuf_quantity_per_file_label+'</span>';
		template +=     '<input type="number" min="1" data-id="'+id+'" class="wcuf_quantity_per_file_input" value="1" '+is_quantity_per_file_box_visible+'></input>';
		template +=   '</div>';
	template += '</div>';
	
	var elem = jQuery('#wcuf_file_name_'+id).append(template);
	jQuery('#wcuf_file_name_'+id).show();
	if(options.disable_image_preview == false)
		wcuf_readURL(file, jQuery('.wcuf_media_preview_container').last());
}
function wcuf_is_pdf_file(file) 
{
	var allowed_fileTypes = ['pdf']; 
	var extension = file.name.split('.').pop().toLowerCase();
	return allowed_fileTypes.indexOf(extension) > -1;
}
function wcuf_readURL(file, container) 
{
	var reader = new FileReader();
	var allowed_fileTypes = ['jpg', 'jpeg', 'png'/* , 'bmp' */];  
	
	 var extension = file.name.split('.').pop().toLowerCase(), 
         isSuccess = allowed_fileTypes.indexOf(extension) > -1 /* || file.type.match('audio.*') */;
		
	//var is_audio = file.type.match('audio.*');	
	if(!isSuccess)
	{
		//container.remove();
		//container.html('<img class="wcuf_image_quantity_preview" src="'+wcuf_options.icon_path+'image.png" />');
		container.html(wcuf_get_placehonder_according_file_type(extension));
		return;
	}
    wcuf_setImage(file,container);
	/* reader.onload = function (e) 
	{
		if(!is_audio)
			container.find('.wcuf_image_quantity_preview').attr('src', e.target.result);
		 //else 
		 //	container.html('<audio class="wcuf_audio_control" controls><source src="', e.target.result,'   "type="audio/ogg"><source src="', e.target.result,' "type="audio/mpeg"></audio>');
		
	}
	reader.readAsDataURL(file); */
}
function wcuf_setImage(file, container) 
{
    //var file = this.files[0];
    var URL = window.URL || window.webkitURL;
    if (URL.createObjectURL && (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif" /* || file.type == "image/bmp" */ )) 
	{
        //document.getElementById('uploadingImg').src = URL.createObjectURL(file);
		container.find('.wcuf_image_quantity_preview').attr('src',   URL.createObjectURL(file) );
		//wcuf_downscaleImageAndSetPreview(container.find('.wcuf_image_quantity_preview'), /* file,  */ URL.createObjectURL(file), 50, file.type, 0.7)
    } else {
        container.find('.wcuf_image_quantity_preview').remove();
    }
}
function wcuf_downscaleImageAndSetPreview(previewContainer, file_data, newWidth, imageType, imageArguments) 
{
    
	wcuf_images_to_preview.push({'file_data': file_data, 'image': "", 'meta': "", 'imageType': imageType, 'imageArguments': imageArguments, 'previewContainer':previewContainer, 'newWidth': newWidth});
}
//started after all files have been processed (see the init method)
function wcuf_process_next_element_to_preview()
{
	if(wcuf_images_to_preview.length == 0)
		return;
	
	var elem = wcuf_images_to_preview.shift();
	
	//var image = new Image();
	/* var image = elem.previewContainer;
	image.load(wcuf_process_next_element_to_preview);
	image.attr('src',   elem.file_data); */
	
	  loadImage(
			elem.file_data,
			function(img, meta)
			{
				elem.image = img;
				elem.meta = meta;
				wcuf_process_elements_to_preview(elem);				
			},
			{
				meta:true,
				orientation: true/*,
				 canvas: true,
				downsamplingRatio: 0.5 */
			}
		);  
}
function wcuf_process_elements_to_preview(elem)
{
	var image = elem.image;
	var imageType = elem.imageType || "image/jpeg";
    var imageArguments = elem.imageArguments || 0.7;
	 
	var oldWidth, oldHeight, newHeight, ctx, newDataUrl;
	oldWidth = image.width;
	oldHeight = image.height;
	newHeight = Math.floor(oldHeight / oldWidth *  elem.newWidth);
	if(image.height > image.width)
	{
		//console.log("here");
		newHeight = elem.newWidth; 
		elem.newWidth = Math.floor(oldHeight / oldWidth *  elem.newWidth);
	}
	
	wcuf_canvas = document.createElement("canvas");
	wcuf_canvas_contex = wcuf_canvas.getContext("2d"); 
	
	// Create a temporary canvas to draw the downscaled image on.
    wcuf_canvas.width = elem.newWidth;
	wcuf_canvas.height = newHeight;

	// Draw the downscaled image on the canvas and return the new data URL.
	
	wcuf_canvas_contex.clearRect(0, 0, elem.newWidth, newHeight);
	wcuf_canvas_contex.drawImage(image, 0, 0, elem.newWidth, newHeight);
	newDataUrl = wcuf_canvas.toDataURL(imageType, imageArguments);
	
	elem.previewContainer.attr('src',  newDataUrl) ;
	setTimeout(wcuf_process_next_element_to_preview, 500);
	//wcuf_process_next_element_to_preview(); 
	
	//elem.previewContainer.attr('src', elem.image.toDataURL(imageType, imageArguments));
	
}
function wcuf_get_field_index(elem)
{
	return elem.parent().parent().index(); 
}
function wcuf_delete_single_file_in_multiple_list(evt)
{
	//Files have not an unique id. To remove the html list index is found and then is used to splice the array
	var id =  jQuery(evt.currentTarget).data('id'); 
	//var index =  jQuery(evt.currentTarget).parent().parent().index(); 
	var index =  wcuf_get_field_index(jQuery(evt.currentTarget)); 
	jQuery('.wcuf_single_file_in_multiple_list:nth-child('+(index+1)+')').remove();
	wcuf_multiple_files_queues[id].splice(index, 1);
	
	if(wcuf_multiple_files_queues[id].length == 0)
	{
		jQuery('button.button#wcuf_upload_multiple_files_button_'+id).fadeOut();
		jQuery('#wcuf_file_name_'+id).hide();
	}
	
	jQuery( document.body ).trigger( 'wcuf_deleted_file_in_multiple_selection' );
}
function wcuf_set_quantity_per_file(evt)
{
	var index =  wcuf_get_field_index(jQuery(evt.currentTarget)); 
	var value = jQuery(evt.currentTarget).val();
	var id = jQuery(evt.currentTarget).data('id'); 
	
	value = value < 1 ? 1 : value;
	jQuery(evt.currentTarget).val(value);
	wcuf_multiple_files_queues[id][index].quantity = value;
}
function wcuf_get_placehonder_according_file_type(extension)
{
	var preview_name = "generic.png";
	switch(extension)
	{
		case "avi":
		case "mpeg":
		case "mpg":
		case "divx":
		case "xvid":
		case "mp4":
		case "mov":
		case "webm":
		case "mka": preview_name = "video.png"; break;
		case "flac":
		case "mp3":
		case "wav":
		case "m4a": preview_name = "audio.png"; break;
		case "bmp":
		case "tiff":
		case "exif":
		case "jpeg":
		case "gif": preview_name = "image.png"; break;
		case "doc":
		case "docx": preview_name = "doc.png"; break;
		case "xls":
		case "sxls": preview_name = "excel.png"; break;
		case "zip":
		case "rar":
		case "tar":
		case "gz":
		case "zip": preview_name = "zip.png"; break;
		case "pdf": preview_name = "pdf.png"; break;
		
	}
	return '<img class="wcuf_image_quantity_preview" src="'+wcuf_options.icon_path+preview_name+'" />';
}